<?php include("../inc/config.php"); ?>
    <div ui-yield-to="modals"></div>
    <div ui-content-for="modals">
        <div class="modal" ui-if="modals" ui-state='modals'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog" style='height:100%;'>
                <div class="modal-content" style='height:90%;'>
                    
                    <div class="modal-header">
                        <button class="close" ui-turn-off="modals" ng-click='vm.clearState(vm.appState)'>&times;</button>
                        <h4 class="modal-title">Appointment Infomation</h4>
                    </div>

                    <div class="modal-body" style='height:85%; '>
                        <div class="container" style='height:100%; overflow:auto;'>
                            <div class='panel panel-info'>

                                <div class='panel-heading'>Appointment Date : <strong>{{vm.pState.date}}</strong></div>

                                <div class='section'>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-3 col-md-offset-1">
                                            <label>Doctor</label>
                                            <select class='form-control' ng-model='vm.choosenDoctor' ng-change='vm.pickFromDoctor(vm.choosenDepartment,vm.choosenDoctor)' ng-disabled='vm.pState.date==vm.today'>
                                                <option value='{{doctor.doctorId}}' ng-repeat='doctor in vm.allDoctor'>{{doctor.firstName}} {{doctor.lastName}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-md-offset-1">
                                            <label>Department</label>
                                            <select class='form-control' ng-model='vm.choosenDepartment' ng-change='vm.changeDepartment(vm.choosenDepartment);vm.pickFromDepartment(vm.choosenDepartment)' ng-disabled='vm.pState.date==vm.today'>
                                                <option value='{{department.departmentNumber}}' ng-repeat='department in vm.allDepartment'>{{department.departmentName}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1">
                                            <label>Time</label>
                                            <p>{{vm.timeByPeriod(vm.pState.period,0)}} - {{vm.timeByPeriod(vm.pState.period,1)}}</p>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <label>Cause</label>
                                            <textarea class='form-control' ng-model='vm.pState.cause' rows='3'></textarea>
                                        </div>
                                    </div>

                                    <br>
                                    <br>
                                    <div class="row panel">
                                        <div class='col-md-2 col-md-offset-9'>
                                            <div class='btn btn-info  pull-right' ng-click='vm.pState.date==vm.today||vm.toggleChangeDate();vm.generateEvent(vm.appState);' ng-show='!vm.showChangeDate' ng-disabled='vm.pState.date==vm.today'><i class="fa fa-calendar-plus-o "></i> Choose Date </div>
                                            <div class='btn btn-info  pull-right' ng-click='vm.toggleChangeDate();' ng-show='vm.showChangeDate'><i class="fa fa-calendar-plus-o "></i> Hide Calendar </div>
                                        </div>
                                        <div class="col-md-8 col-md-offset-2" ng-show="vm.showChangeDate">
                                            <h3 class="text-center">{{ vm.calendarTitle }}</h3>
                                            <div class="row">
                                                <div class="col-md-6 text-center">
                                                    <div class="btn-group">
                                                        <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" decrement="vm.calendarView" ng-click='vm.toPreviousMonth()'>
                                                            Previous
                                                        </button>
                                                        <button class="btn btn-default" mwl-date-modifier date="vm.calendarDay" set-to-today ng-click='vm.toThisMonth()'>
                                                            Today
                                                        </button>
                                                        <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" increment="vm.calendarView" ng-click='vm.toNextMonth()'>
                                                            Next
                                                        </button>
                                                    </div>
                                                </div>
                                                
                                                <br class="visible-xs visible-sm">
                                                <div class="col-md-6 text-center">
                                                    <div class="btn-group">
                                                        <!--                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'year'">Year</label>-->
                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'month'">Month</label>
                                                        <!--                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'week'">Week</label>-->
                                                        <!--                                                    <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'day'">Day</label>-->
                                                    </div>
                                                </div>
                                            </div>

                                            <br>
                                            <mwl-calendar events="vm.events" view="vm.calendarView" view-title="vm.calendarTitle" current-day="vm.calendarDay" on-event-click="vm.changeDate(date,type,num_current_event)" on-event-times-changed="vm.eventTimesChanged(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd" edit-event-html="'<i class=\'fa fa-pencil white\'></i>'" delete-event-html="'<i class=\'fa fa-remove white\'></i>'" on-edit-event-click="vm.eventEdited(calendarEvent)" on-delete-event-click="vm.eventDeleted(calendarEvent)" auto-open="false" day-view-start="06:00" day-view-end="22:00" day-view-split="30" cell-modifier="vm.modifyCell(calendarCell)">
                                            </mwl-calendar>

                                            <br>
                                            <p class='text-right'><small>*The Number in Calendar is Number of Available Appointment in That Day</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer lg">
                    <button ui-turn-off="modals" class="btn btn-default" ng-click='vm.clearState()'>Close</button>
                    <button ui-turn-on="modalsDelete" class="btn btn-danger" ng-disabled='vm.pState.date==vm.today'>Delete</button>
                    <button ui-turn-off="modals" class="btn btn-primary" ng-click='vm.saveState()' ng-disabled='vm.pState.date==vm.today'>Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div ui-yield-to="modalsDelete"></div>
    <div ui-content-for="modalsDelete">
        <div class="modal" ui-if="modalsDelete" ui-state='modalsDelete'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title" style="color:red">Warning</h4>
                    </div>

                    <div class="modal-body">
                        You can undo this action.Are you sure to continute?
                    </div>

                    <div class="modal-footer">
                        <button ui-turn-off="modalsDelete" class="btn btn-default">Cancel</button>
                        <button ui-turn-off="modalsDelete" class="btn btn-danger" ng-click="vm.deleteAssignment();vm.delState()">Continute</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="scrollable-header section" style="z-index:900">
        <div class="container section">
            <div class="row">
                <div class="col-md-12">
                    <h2 style="display:inline">My Dashboard</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="scrollable-content" style="position: relative; overflow: hidden;">
        <div ui-state="activeTab" default="1" style="height:inherit">
            <ul class="nav nav-tabs container">
                
                <li ui-class="{'active': activeTab == 1}" ng-click='vm.setNewAppointment()'>
                    <a ui-set="{'activeTab': 1}">Appointment</a>
                </li>

                <li ui-class="{'active': activeTab == 2}">
                    <a ui-set="{'activeTab': 2}" ng-click='vm.setNewAppointment()'>Make an Appointment</a>
                </li>
            </ul>

            <div style="position:relative;height:80%">
                <div ui-if="activeTab == 1" class="section scrollable">
                    <div class="container" style="height:inherit">
                        <div class="row" style="height:inherit">
                            <div class="col-md-12" style="height:inherit">
                                <h3 class="page-header">Welcome <small> {{vm.aStaff.firstName}} {{vm.aStaff.lastName}}</small></h3>
                                <br>
                                <div>

                                    <div class='row'>
                                        <div class="col-md-6 col-md-offset-3">
                                            <div class='row panel panel-info'>
                                                <div class='panel-heading'>Please Enter SSID To search Patient</div>
                                                <div class='row'>
                                                    <div class='col-md-8 col-md-offset-2'>
                                                        <div class="section">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" ng-model='vm.searchArg' placeholder="" aria-describedby="sizing-addon1">
                                                                <span class="input-group-btn" id="sizing-addon1"><button class="btn btn-default" ng-click='vm.findPatient()'><i class="fa fa-search"></i></button></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class='row section'>
                                        <h4 ng-show='vm.showContain&&vm.allAppointment.length==0'>Does not Have Appointment</h4>
                                        <table class="table table-striped" ng-show='vm.showContain&&vm.allAppointment.length!=0'>
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Department</th>
                                                    <th>Doctor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="content-row" ui-turn-on='modals' ng-repeat='appointment in vm.allAppointment' ng-click="vm.setState(appointment)">
                                                    <td>{{$index+1}}</td>
                                                    <td>{{appointment.date}}</td>
                                                    <td>{{vm.timeByPeriod(appointment.period,0)}} - {{vm.timeByPeriod(appointment.period,1)}}</td>
                                                    <td>{{appointment.departmentName}}</td>
                                                    <td>{{appointment.doctorFirstName}} {{appointment.doctorLastName}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div ui-if="activeTab == 2" class="section scrollable">
                    <div class="scrollable-content">
                        <div class="container">
                            <h3 class="page-header">Welcome <small> {{vm.aStaff.firstName}} {{vm.aStaff.lastName}}</small></h3>

                            <br>
                            <div class='row'>
                                <div class="col-md-6 col-md-offset-3">
                                    <div class='row panel panel-info'>
                                        <div class='panel-heading'>Please Enter SSID To search Patient</div>
                                        <div class='row'>
                                            <div class='col-md-8 col-md-offset-1'>
                                                <div class="section">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" ng-model='vm.searchArg' placeholder="" aria-describedby="sizing-addon1">
                                                        <span class="input-group-btn" id="sizing-addon1"><button class="btn btn-default" ng-click='vm.findPatient()'><i class="fa fa-search"></i></button></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-md-2'>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" ng-change='vm.changeMode()' ng-model='vm.walkInMode'>Walk In</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class='panel panel-info' ng-show='vm.showContain&&!vm.walkInMode'>
                                <div class='panel-heading'><strong>{{vm.aPatient.firstName}} {{vm.aPatient.lastName}}</strong></div>
                                <div class="section" ng-hide='false'>

                                    <div class='row'>
                                        <div class='col-md-3 col-md-offset-1'>
                                            <label>Doctor</label>
                                            <select class='form-control' ng-model='vm.choosenDoctor' ng-change='vm.pickFromDoctor(vm.choosenDepartment,vm.choosenDoctor)'>
                                                <option value='{{doctor.doctorId}}' ng-repeat='doctor in vm.allDoctor'>{{doctor.firstName}} {{doctor.lastName}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 ">
                                            <label>Department</label>
                                            <select class='form-control' ng-model='vm.choosenDepartment' ng-change='vm.changeDepartment(vm.choosenDepartment);vm.pickFromDepartment(vm.choosenDepartment)'>
                                                <option value='{{department.departmentNumber}}' ng-repeat='department in vm.allDepartment'>{{department.departmentName}}</option>
                                            </select>
                                        </div>
                                        <div class='col-md-2'>
                                            <label>Date</label>
                                            <p>{{vm.pState.date}}</p>
                                        </div>
                                        <div class='col-md-2 '>
                                            <label>Time</label>
                                            <p>{{vm.timeByPeriod(vm.pState.period,0)}} - {{vm.timeByPeriod(vm.pState.period,1)}}</p>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <label>Cause</label>
                                            <textarea class="form-control" rows="3" ng-model='vm.pState.cause'></textarea>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class='col-md-2 col-md-offset-7'>
                                            <div class='btn btn-info  pull-right' ng-click='vm.toggleChangeDate();vm.generateEvent(vm.appState);' ng-show='!vm.showChangeDate'><i class="fa fa-calendar-plus-o "></i> Choose Date </div>
                                            <div class='btn btn-info  pull-right' ng-click='vm.toggleChangeDate();' ng-show='vm.showChangeDate'><i class="fa fa-calendar-plus-o "></i> Hide Calendar </div>
                                        </div>
                                        <div class='col-md-1'>
                                            <div class='btn btn-primary' ng-click='vm.saveState()'>Save Appointment</div>
                                        </div>
                                    </div>

                                    <br>
                                    <div class='row'>
                                        <div class="col-md-8 col-md-offset-2" ng-show='vm.showChangeDate'>
                                            <h3 class="text-center">{{ vm.calendarTitle }}</h3>
                                            <div class="row">
                                                <div class="col-md-6 text-center">
                                                    <div class="btn-group">
                                                        <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" decrement="vm.calendarView" ng-click='vm.toPreviousMonth()'>
                                                            Previous
                                                        </button>
                                                        <button class="btn btn-default" mwl-date-modifier date="vm.calendarDay" set-to-today ng-click='vm.toThisMonth()'>
                                                            Today
                                                        </button>
                                                        <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" increment="vm.calendarView" ng-click='vm.toNextMonth()'>
                                                            Next
                                                        </button>
                                                    </div>
                                                </div>

                                                <br class="visible-xs visible-sm">
                                                <div class="col-md-6 text-center">
                                                    <div class="btn-group">
                                                        <!--                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'year'">Year</label>-->
                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'month'">Month</label>
                                                        <!--                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'week'">Week</label>-->
                                                        <!--                                                    <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'day'">Day</label>-->
                                                    </div>
                                                </div>
                                            </div>

                                            <br>
                                            <mwl-calendar events="vm.events" view="vm.calendarView" view-title="vm.calendarTitle" current-day="vm.calendarDay" on-event-click="vm.setCurrentCeilOperation(date,type,num_current_event)" on-event-times-changed="vm.eventTimesChanged(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd" edit-event-html="'<i class=\'fa fa-pencil white\'></i>'" delete-event-html="'<i class=\'fa fa-remove white\'></i>'" on-edit-event-click="vm.eventEdited(calendarEvent)" on-delete-event-click="vm.eventDeleted(calendarEvent)" auto-open="false" day-view-start="06:00" day-view-end="22:00" day-view-split="30" cell-modifier="vm.modifyCell(calendarCell)">
                                            </mwl-calendar>

                                            <br>
                                            <p class='text-right'><small>*The Number in Calendar is Number of Available Appointment in That Day</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class='panel panel-info' ng-show='vm.showContain&&vm.walkInMode'>
                                <div class='panel-heading'><strong>{{vm.aPatient.firstName}} {{vm.aPatient.lastName}}</strong></div>
                                <div class="section" ng-hide='false'>
                                    <div class='row'>
                                        <div class='col-md-3 col-md-offset-1'>
                                            <label>Doctor</label>
                                            <select class='form-control' ng-model='vm.choosenDoctor' ng-change='vm.pickFromDoctor(vm.choosenDepartment,vm.choosenDoctor)'>
                                                <option value='{{doctor.doctorId}}' ng-repeat='doctor in vm.allDoctor'>{{doctor.firstName}} {{doctor.lastName}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 ">
                                            <label>Department</label>
                                            <select class='form-control' ng-model='vm.choosenDepartment' ng-change='vm.changeDepartment(vm.choosenDepartment);'>
                                                <option value='{{department.departmentNumber}}' ng-repeat='department in vm.allDepartment'>{{department.departmentName}}</option>
                                            </select>
                                        </div>
                                        <div class='col-md-2'>
                                            <label>Date</label>
                                            <p>{{vm.pState.date}}</p>
                                        </div>
                                        <div class='col-md-2 '>
                                            <label>Time</label>
                                            <p>{{vm.timeByPeriod(vm.pState.period,0)}} - {{vm.timeByPeriod(vm.pState.period,1)}}</p>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <label>Cause</label>
                                            <textarea class="form-control" rows="3" ng-model='vm.pState.cause'></textarea>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="row">
                                        <div class='col-md-2 col-md-offset-7'>
                                            <div>
                                                <p>Available Seat: <strong>{{vm.availableSeat}}</strong></p>
                                            </div>
                                        </div>
                                        <div class='col-md-1'>
                                            <div class='btn btn-primary' ng-click='vm.saveState()' ng-disabled='vm.availableSeat==0'>Save Appointment</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
