<?php include("../inc/config.php"); ?>
    <div ui-yield-to="modals"></div>
    <div ui-content-for="modals">
        <div class="modal" ui-if="modals" ui-state='modals'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog" style='height:100%;'>
                <div class="modal-content" style='height:90%;'>

                    <div class="modal-header">
                        <button class="close" ui-turn-off="modals" ng-click='vm.clearState(vm.appState)'>&times;</button>
                        <h4 class="modal-title">Appointment Infomation</h4>
                    </div>

                    <div class="modal-body" style='height:85%; '>
                        <div class="container" style='height:100%; overflow:auto;'>
                            <div class='panel panel-info'>
                                <div class='panel-heading'>Appointment Date : <strong>{{vm.pState.date}}</strong></div>
                                <div class='section'>

                                    <br>
                                    <div class="row">
                                        <div class="col-md-3 col-md-offset-1">
                                            <label>Doctor</label>
                                            <select class='form-control' ng-model='vm.choosenDoctor' ng-change='vm.pickFromDoctor(vm.choosenDepartment,vm.choosenDoctor)' ng-disabled='vm.pState.date==vm.today'>
                                                <option value='{{doctor.doctorId}}' ng-repeat='doctor in vm.allDoctor'>{{doctor.firstName}} {{doctor.lastName}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-md-offset-1">
                                            <label>Department</label>
                                            <select class='form-control' ng-model='vm.choosenDepartment' ng-change='vm.changeDepartment(vm.choosenDepartment);vm.pickFromDepartment(vm.choosenDepartment)' ng-disabled='vm.pState.date==vm.today'>
                                                <option value='{{department.departmentNumber}}' ng-repeat='department in vm.allDepartment'>{{department.departmentName}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1">
                                            <label>Time</label>
                                            <p>{{vm.timeByPeriod(vm.pState.period,0)}} - {{vm.timeByPeriod(vm.pState.period,1)}}</p>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <label>Cause</label>
                                            <textarea class='form-control' ng-model='vm.pState.cause' rows='3'></textarea>
                                        </div>
                                    </div>

                                    <br>
                                    <br>
                                    <div class="row panel">
                                        <div class='col-md-2 col-md-offset-9'>
                                            <div class='btn btn-info  pull-right' ng-click='vm.pState.date==vm.today||vm.toggleChangeDate();vm.generateEvent(vm.appState);' ng-show='!vm.showChangeDate' ng-disabled="vm.pState.date==vm.today"><i class="fa fa-calendar-plus-o "></i> Choose Date </div>
                                            <div class='btn btn-info  pull-right' ng-click='vm.toggleChangeDate();' ng-show='vm.showChangeDate'><i class="fa fa-calendar-plus-o "></i> Hide Calendar </div>
                                        </div>

                                        <div class="col-md-8 col-md-offset-2" ng-show="vm.showChangeDate">
                                            <h3 class="text-center">{{ vm.calendarTitle }}</h3>
                                            <div class="row">
                                                <div class="col-md-6 text-center">
                                                    <div class="btn-group">
                                                        <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" decrement="vm.calendarView" ng-click='vm.toPreviousMonth()'>
                                                            Previous
                                                        </button>
                                                        <button class="btn btn-default" mwl-date-modifier date="vm.calendarDay" set-to-today ng-click='vm.toThisMonth()'>
                                                            Today
                                                        </button>
                                                        <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" increment="vm.calendarView" ng-click='vm.toNextMonth()'>
                                                            Next
                                                        </button>
                                                    </div>
                                                </div>

                                                <br class="visible-xs visible-sm">
                                                <div class="col-md-6 text-center">
                                                    <div class="btn-group">
                                                        <!--                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'year'">Year</label>-->
                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'month'">Month</label>
                                                        <!--                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'week'">Week</label>-->
                                                        <!--                                                    <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'day'">Day</label>-->
                                                    </div>
                                                </div>
                                            </div>

                                            <br>
                                            <mwl-calendar events="vm.events" view="vm.calendarView" view-title="vm.calendarTitle" current-day="vm.calendarDay" on-event-click="vm.changeDate(date,type,num_current_event)" on-event-times-changed="vm.eventTimesChanged(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd" edit-event-html="'<i class=\'fa fa-pencil white\'></i>'" delete-event-html="'<i class=\'fa fa-remove white\'></i>'" on-edit-event-click="vm.eventEdited(calendarEvent)" on-delete-event-click="vm.eventDeleted(calendarEvent)" auto-open="false" day-view-start="06:00" day-view-end="22:00" day-view-split="30" cell-modifier="vm.modifyCell(calendarCell)">
                                            </mwl-calendar>

                                            <br>
                                            <p class='text-right'><small>*The Number in Calendar is Number of Available Appointment in That Day</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer lg">
                    <button ui-turn-off="modals" class="btn btn-default" ng-click='vm.clearState()'>Close</button>
                    <button ui-turn-on="modalsDelete" class="btn btn-danger" ng-click='' ng-disabled="vm.pState.date==vm.today">Delete</button>
                    <button ui-turn-off="modals" class="btn btn-primary" ng-click='vm.saveState()' ng-disabled="vm.pState.date==vm.today">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div ui-yield-to="modalsAddAppointment"></div>
    <div ui-content-for="modalsAddAppointment">
        <div class="modal" ui-if="modalsAddAppointment" ui-state='modalsAddAppointment'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog">
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <button class="close" ui-turn-off="modalsAddAppointment" ng-click='vm.clearState(vm.nState)'>&times;</button>
                        <h4 class="modal-title">Appointment Information</h4>
                    </div>

                    <div class="modal-body">
                        <div class="container" style='height:100%; overflow:auto;'>
                            <div class="col-md-12" style='height:90%; overflow:auto;'>
                                <br>
                                <div class='panel panel-info'>
                                    
                                    <div class='panel-heading'>New Appointment</div>
                                    
                                    <br>
                                    <div class='section'>
                                        
                                        <br>
                                        <div class="row">
                                            <div class='col-md-10 col-md-offset-1'>
                                                <h4>Your Appointment Date is <strong>{{vm.nState.date.toLocaleDateString()}}</strong></h4>
                                            </div>
                                        </div>
                                        
                                        <br>
                                        <div class="row">
                                            <div class="col-md-3 col-md-offset-1">
                                                <label>Doctor</label>
                                                <p>{{vm.nState.doctor}}</p>
                                            </div>
                                            <div class="col-md-3 col-md-offset-1">
                                                <label>Department</label>
                                                <p>{{vm.nState.department}}</p>
                                            </div>
                                            <div class='col-md-2 col-md-1'>
                                                <label>Time</label>
                                                <p>{{vm.nState.timeByPeriod(vm.nState.period,0)}} - {{vm.nState.timeByPeriod(vm.nState.period,1)}}</p>
                                            </div>
                                        </div>
                                        
                                        <br>
                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-1">
                                                <label>Cause</label>
                                                <textarea class="form-control" rows="3" ng-model='vm.nState.cause'></textarea>
                                            </div>
                                        </div>
                                        
                                        <br>
                                        <div class='row'>
                                            <div class="col-md-10 col-md-offset-1">
                                                <label>Abnormality</label>
                                                <textarea class="form-control" rows="3" ng-model='vm.nState.abnormality'></textarea>
                                            </div>
                                        </div>
                                        
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button ui-turn-off="modalsAddAppointment" class="btn btn-default" ng-click='vm.clearState(vm.nState)'>Close</button>
                        <button ui-turn-off="modalsAddAppointment" class="btn btn-primary" ng-click='vm.saveNState()'>Save</button>
                    </div>
                    
                </div>
            </div>
        </div>

        <div ui-yield-to="modalsDelete"></div>
        <div ui-content-for="modalsDelete">
            <div class="modal" ui-if="modalsDelete" ui-state='modalsDelete'>
                <div class="modal-backdrop in"></div>
                <div class="modal-dialog">
                    <div class="modal-content">
                        
                        <div class="modal-header">
                            <h4 class="modal-title" style="color:red">Warning</h4>

                        </div>
                        
                        <div class="modal-body">
                            You can undo this action.Are you sure to continute?
                        </div>
                        
                        <div class="modal-footer">
                            <button ui-turn-off="modalsDelete" class="btn btn-default">Cancel</button>
                            <button ui-turn-off="modalsDelete" class="btn btn-danger" ng-click="vm.deleteAssignment();vm.delState()">Continute</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="scrollable-header section" style="z-index:900">
        <div class="container section">
            <div class="row">
                <div class="col-md-12">
                    <h2>My Dashboard</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="scrollable-content" style="position: relative; overflow: hidden;">
        <div ui-state="activeTab" default="1" style="height:inherit">
            <ul class="nav nav-tabs container">
                
                <li ui-class="{'active': activeTab == 1}" ng-click='vm.clearState()'>
                    <a ui-set="{'activeTab': 1}">Appointment</a>
                </li>
                
                <li ui-class="{'active': activeTab == 2}" ng-click='vm.setNewAppointment()'>
                    <a ui-set="{'activeTab': 2}">Make an Appointment</a>
                </li>
            </ul>

            <div ui-if="activeTab == 1">
                <div class="container" style="height:inherit">
                    <div class="row" style="height:inherit">
                        <div class="col-md-12" style="height:inherit">
                            
                            <h3 class="page-header">Welcome <small> {{vm.aPatient.firstName}} {{vm.aPatient.lastName}}</small></h3>
                            <br>
                            
                            <div ng-show='!vm.hasApp'>
                                
                                <div class='panel'>
                                    <h4 ng-show='vm.allAppointment.length!=0'>Your Next Appointment</h4>
                                    <h4 ng-show='vm.allAppointment.length==0'>Does not Have Appointment</h4>
                                </div>
                                
                                <div class='row'>
                                    <table class="table table-striped" ng-show='vm.allAppointment.length!=0'>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Department</th>
                                                <th>Doctor</th>
                                            </tr>
                                        </thead>
                                        <tbody ng-repeat='appointment in vm.allAppointment'>
                                            <tr class="content-row" ui-turn-on='modals' ng-click="vm.setState(appointment)">
                                                <th scope="row">{{$index+1}}</th>
                                                <td>{{appointment.date}}</td>
                                                <td>{{vm.timeByPeriod(appointment.period,0)}} - {{vm.timeByPeriod(appointment.period,1)}}</td>
                                                <td>{{appointment.departmentName}}</td>
                                                <td>{{appointment.doctorFirstName}} {{appointment.doctorLastName}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div style="position:relative;height:80%">
                <div ui-if="activeTab == 2" class="section scrollable">
                    <div class="scrollable-content">
                        <br>
                        <div class='container'>
                            <div class='panel panel-info'>
                                
                                <div class='panel-heading'>Appointment Date: <strong>{{vm.pState.date}}</strong> </div>
                                
                                <div class="section">
                                    
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3 col-md-offset-1">
                                            <label>Doctor</label>
                                            <select class='form-control' ng-model='vm.choosenDoctor' ng-change='vm.pickFromDoctor(vm.choosenDepartment,vm.choosenDoctor)'>
                                                <option value='{{doctor.doctorId}}' ng-repeat='doctor in vm.allDoctor'>{{doctor.firstName}} {{doctor.lastName}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-md-offset-1">
                                            <label>Department</label>
                                            <select class='form-control' ng-model='vm.choosenDepartment' ng-change='vm.changeDepartment(vm.choosenDepartment);vm.pickFromDepartment(vm.choosenDepartment)'>
                                                <option value='{{department.departmentNumber}}' ng-repeat='department in vm.allDepartment'>{{department.departmentName}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1">
                                            <label>Time</label>
                                            <p>{{vm.timeByPeriod(vm.pState.period,0)}} - {{vm.timeByPeriod(vm.pState.period,1)}}</p>
                                        </div>
                                    </div>
                                    
                                    <br>
                                    <div class="row ">
                                        <div class="col-md-10 col-md-offset-1">
                                            <label>Cause</label>
                                            <textarea class='form-control' ng-model='vm.pState.cause' rows='3'></textarea>
                                        </div>
                                    </div>
                                    
                                    <br>
                                    <div class="row">
                                        <div class='col-md-2 col-md-offset-7'>
                                            <div class='btn btn-info  pull-right' ng-click='vm.toggleChangeDate();' ng-show='!vm.showChangeDate'><i class="fa fa-calendar-plus-o "></i> Choose Date </div>
                                            <div class='btn btn-info  pull-right' ng-click='vm.toggleChangeDate();' ng-show='vm.showChangeDate'><i class="fa fa-calendar-plus-o "></i> Hide Calendar </div>
                                        </div>
                                        <div class='col-md-1'>
                                            <div class='btn btn-primary' ng-click='vm.saveState()'>Save Appointment</div>
                                        </div>
                                    </div>
                                    
                                    <br>
                                    <div class='row'>
                                        <div class="col-md-8 col-md-offset-2" ng-show='vm.showChangeDate'>
                                            <h3 class="text-center">{{ vm.calendarTitle }}</h3>
                                            <div class="row">
                                                <div class="col-md-6 text-center">
                                                    <div class="btn-group">

                                                        <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" decrement="vm.calendarView" ng-click='vm.toPreviousMonth()'>
                                                            Previous
                                                        </button>
                                                        <button class="btn btn-default" mwl-date-modifier date="vm.calendarDay" set-to-today ng-click='vm.toThisMonth()'>
                                                            Today
                                                        </button>
                                                        <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" increment="vm.calendarView" ng-click='vm.toNextMonth()'>
                                                            Next
                                                        </button>
                                                    </div>
                                                </div>
                                                
                                                <br class="visible-xs visible-sm">
                                                <div class="col-md-6 text-center">
                                                    <div class="btn-group">
                                                        <!--                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'year'">Year</label>-->
                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'month'">Month</label>
                                                        <!--                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'week'">Week</label>-->
                                                        <!--                                                    <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'day'">Day</label>-->
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <br>
                                            <mwl-calendar events="vm.events" view="vm.calendarView" view-title="vm.calendarTitle" current-day="vm.calendarDay" on-event-click="vm.setCurrentCeilOperation(date,type,num_current_event)" on-event-times-changed="vm.eventTimesChanged(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd" edit-event-html="'<i class=\'fa fa-pencil white\'></i>'" delete-event-html="'<i class=\'fa fa-remove white\'></i>'" on-edit-event-click="vm.eventEdited(calendarEvent)" on-delete-event-click="vm.eventDeleted(calendarEvent)" auto-open="false" day-view-start="06:00" day-view-end="22:00" day-view-split="30" cell-modifier="vm.modifyCell(calendarCell)">
                                            </mwl-calendar>
                                            
                                            <br>
                                            <p class='text-right'><small>*The Number in Calendar is Number of Available Appointment in That Day</small></p>
                                            
                                            <div class="section">
                                            </div>
                                            
                                            <div class="section">
                                            </div>
                                            
                                            <div class="section">
                                            </div>
                            
                                            <script type="text/ng-template" id="modalContent.html">
                                                <div class="modal-header">
                                                    <button class="close" ng-click='$close()'>&times;</button>
                                                    <h4 class="modal-title">Patient Infomation</h4>
                                                </div>
                                                <div class="modal-body" style='height:82%;'>
                                                    <div class="container" style='height:100%; overflow:auto;'>

                                                        <div class="col-md-3">
                                                            <div class='row'>
                                                                <h3>Basic Info</h3>
                                                                <img src='../img/jack.jpg' class='img-rounded img-responsive' />
                                                                <p><span ng-bind='vm.pInfo.firstName'></span> <span ng-bind='vm.pInfo.lastName'></span></p>
                                                            </div>
                                                            <div ui-state="patientTab" default="1" class='row'>
                                                                <ul class="nav nav-pills nav-stacked">
                                                                    <li ui-class="{'active': patientTab == 1}">
                                                                        <a ui-set="{'patientTab': 1}">History</a>
                                                                    </li>
                                                                    <li ui-class="{'active': patientTab == 2}">
                                                                        <a ui-set="{'patientTab': 2}">Measurement</a>
                                                                    </li>
                                                                    <li ui-class="{'active': patientTab == 3}">
                                                                        <a ui-set="{'patientTab': 3}">Result</a>
                                                                    </li>
                                                                    <li ui-class="{'active': patientTab == 4}">
                                                                        <a ui-set="{'patientTab': 4}">Appointment</a>
                                                                    </li>
                                                                </ul>


                                                            </div>
                                                        </div>

                                                        <div class="col-md-8" style='height:100%; overflow:auto;'>
                                                            <div ui-if="patientTab == 1" class='col-md-12'>
                                                                <h3 class="page-header">History</h3>
                                                                <div class='row'>
                                                                    <div class='col-md-12'>
                                                                        <div class='panel panel-default'>
                                                                            <div class='panel-heading'>General Information</div>
                                                                            <div class='panel-body'>
                                                                                <p>{{vm.pInfo.ssid}} {{vm.pInfo.id}} {{vm.pInfo.sex}} {{vm.pInfo.address}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class='col-md-12'>
                                                                        <div class='panel panel-default'>
                                                                            <div class='panel-heading'>Medical History</div>
                                                                            <ul class='list-group'>
                                                                                <li class='list-group-item' ng-repeat='history in vm.pHistory'>
                                                                                    <p>{{history.date}}</p>
                                                                                    <p>{{history.symptom}}</p>
                                                                                    <p>{{history.date}}</p>
                                                                                    <p>{{history.symptom}}</p>
                                                                                    <p>{{history.date}}</p>
                                                                                    <p>{{history.symptom}}</p>
                                                                                    <p>{{history.date}}</p>
                                                                                    <p>{{history.symptom}}</p>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div ui-if="patientTab == 2" class='col-md-12'>
                                                                <h3 class="page-header">Measurement</h3>
                                                                <div class="row">
                                                                    <div class='col-md-12'>
                                                                        <small>Measure by <strong>{{vm.pMeasure.nurse}}</strong></small>
                                                                        <br>
                                                                        <br>
                                                                        <dl class="dl-horizontal">
                                                                            <dt><h3>Pressure</h3></dt>
                                                                            <dd>
                                                                                <p>{{vm.pMeasure.pressure}}</p>
                                                                            </dd>
                                                                            <br>
                                                                            <dt><h3>Heart Rate</h3></dt>
                                                                            <dd>
                                                                                <p>{{vm.pMeasure.hrRate}}</p>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div ui-if="patientTab == 3" class='col-md-12'>
                                                                <h3 class="page-header">Result</h3>
                                                                <p>
                                                                    <!-- ... -->
                                                                </p>
                                                            </div>
                                                            <div ui-if="patientTab == 4" class='col-md-12'>
                                                                <h3 class="page-header">Appointment</h3>
                                                                <p>
                                                                    <!-- ... -->
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button class="btn btn-default" ng-click='vm.clearState();$close()'>Close</button>
                                                        <button class="btn btn-primary">Save changes</button>
                                                    </div>

                                                </div>
                                            </script>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
