<?php include("../inc/config.php"); ?>
    <div ui-yield-to="modals"></div>
    <div ui-content-for="modals">
        <div class="modal" ui-if="modals" ui-state='modals'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog" style='height:100%;'>
                <div class="modal-content" style='height:90%;'>

                    <div class="modal-header">
                        <button class="close" ui-turn-off="modals">&times;</button>
                        <h4 class="modal-title">Patient Infomation</h4>
                    </div>

                    <div class="modal-body" style='height:85%; '>
                        <div class="container" style='height:100%; overflow:auto;'>

                            <div class="col-md-12">
                                <div ui-state="patientTab" default="1" class='row'>
                                    <ul class="nav nav-pills nav-tabs">
                                        <li ui-class="{'active': patientTab == 1}">
                                            <a ui-set="{'patientTab': 1}">Information</a>
                                        </li>
                                        <li ui-class="{'active': patientTab == 2}" ng-click='vm.getTodayInfo()'>
                                            <a ui-set="{'patientTab': 2}">Measurement</a>
                                        </li>
                                        <li ui-class="{'active': patientTab == 3}" ng-click='vm.setNewMedicalHistory()'>
                                            <a ui-set="{'patientTab': 3}">Disagnostic Result</a>
                                        </li>
                                        <li ui-class="{'active': patientTab == 4}" ng-click='vm.setNewAppointment()'>
                                            <a ui-set="{'patientTab': 4}">Appointment</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                            <div class="col-md-12" style='height:89%; overflow:auto;'>
                                <div ui-if="patientTab == 1">
                                    <h3 class="page-header">Information</h3>
                                    <div class='row'>
                                        <div class='col-md-12 section'>
                                            <div class='panel panel-info'>

                                                <div class='panel-heading'>
                                                    General Information
                                                </div>

                                                <div class='section'>
                                                    <br>
                                                    <div class='row'>
                                                        <div class="col-md-3 col-md-offset-1">
                                                            <img ng-src="../img/jack.jpg" class='img-rounded img-responsive' height="250" width="150" />
                                                        </div>
                                                        <div class='col-md-8'>

                                                            <div class='row'>
                                                                <div class='col-md-4'>
                                                                    <label>Name</label>
                                                                    <p>{{vm.aPatient.firstName}} {{vm.aPatient.lastName}}</p>
                                                                </div>
                                                                <div class='col-md-3'>
                                                                    <label>SSID</label>
                                                                    <p>{{vm.aPatient.ssid}}</p>
                                                                </div>
                                                                <div class='col-md-2'>
                                                                    <label>Sex</label>
                                                                    <p>{{vm.tranSex(vm.aPatient.sex)}}</p>
                                                                </div>
                                                            </div>

                                                            <br>
                                                            <div class='row'>
                                                                <div class='col-md-4'>
                                                                    <label>Email</label>
                                                                    <p>{{vm.aPatient.email}}</p>
                                                                </div>
                                                                <div class='col-md-4'>
                                                                    <label>Telephone</label>
                                                                    <p>{{vm.aPatient.telephone}}</p>
                                                                </div>
                                                            </div>

                                                            <br>
                                                            <div class='row'>
                                                                <div class='col-md-8'>
                                                                    <label>Address</label>
                                                                    <p>{{vm.aPatient.address}}</p>
                                                                </div>
                                                            </div>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class='row'>
                                        <div class='col-md-12 section'>
                                            <div class='panel panel-info'>

                                                <div class='panel-heading'>
                                                    Drug Allergy <a class='pull-right' ng-click='vm.addAllergySlot()'><i class="fa fa-plus" style="color:"></i></a>
                                                </div>
                                                <br>

                                                <div class='row'>
                                                    <div class='col-md-10 col-md-offset-1'>
                                                        <table class='table'>
                                                            <thead>
                                                                <th>#</th>
                                                                <th>Drug Name</th>
                                                                <th>Drug ID</th>
                                                            </thead>
                                                            <tbody>
                                                                <tr ng-repeat='drug in vm.drugAllergy'>
                                                                    <td>{{$index+1}}</td>
                                                                    <td>{{drug.drugName}}</td>
                                                                    <td>{{drug.drugId}}</td>
                                                                </tr>

                                                                <tr ng-repeat='newAllergy in vm.newDrugAllergy'>
                                                                    <td ng-click='vm.delAllergySlot($index)'>
                                                                        <a>Delete</a>
                                                                    </td>

                                                                    <td>
                                                                        <select class="form-control" ng-model='newAllergy.drugId' ng-change='vm.findDrug(newAllergy,newAllergy.drugId)'>
                                                                            <option ng-repeat='tDrug in vm.drugList' value={{tDrug.drugId}}>{{tDrug.drugName}}</option>
                                                                        </select>
                                                                    </td>

                                                                    <td>
                                                                        <p>{{newAllergy.drugId}}</p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <div class='btn btn-primary pull-right' ng-hide='vm.allergySlotCount==0' ng-click='vm.saveNewAllergy()'>Save</div>

                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>

                                    <div class='row'>
                                        <div class='col-md-12'>
                                            <div class='panel panel-info'>

                                                <div class='panel-heading'>Medical History</div>
                                                <br>

                                                <ul class='list-group'>
                                                    <li class='list-group-item'>
                                                        <div align='center'>
                                                            <ul class="pagination" style="margin: 0 0;">

                                                                <li ng-click='vm.previousUse()'>
                                                                    <a href="#" aria-label="Previous">
                                                                        <span aria-hidden="true">&laquo;</span>
                                                                    </a>
                                                                </li>

                                                                <li ng-repeat='history in vm.allMedicalHistory' ng-click='vm.setHistory(history)'><a>{{history.date}}</a></li>

                                                                <li ng-click='vm.nextUse()'>
                                                                    <a href="#" aria-label="Next">
                                                                        <span aria-hidden="true">&raquo;</span>
                                                                    </a>
                                                                </li>

                                                            </ul>
                                                        </div>

                                                        <div class='section'>
                                                            <br>
                                                            <div class='row'>
                                                                <div class='col-md-11'>
                                                                    <h4 class='pull-right'>Date : {{vm.focusHistory.date}}</h4>
                                                                </div>
                                                            </div>

                                                            <div class='row panel'>
                                                                <div class='col-md-10 col-md-offset-1'>
                                                                    <h4>>> Measurement</h4>
                                                                </div>
                                                            </div>

                                                            <div class='row section'>
                                                                <div class='col-md-10 col-md-offset-1'>
                                                                    <div class='row'>
                                                                        <div class='col-md-3'>
                                                                            <label>Measured by</label>
                                                                            <p>{{vm.aPatientInfo.nurseFirstName}} {{vm.aPatientInfo.nurseLastName}}</p>
                                                                        </div>
                                                                        <div class='col-md-9'>
                                                                            <div class='row'>
                                                                                <div class='col-md-4'>
                                                                                    <label>Weight</label>
                                                                                    <p>{{vm.aPatientInfo.weight}} kg</p>
                                                                                </div>
                                                                                <div class='col-md-4'>
                                                                                    <label>Height</label>
                                                                                    <p>{{vm.aPatientInfo.height}} cm</p>
                                                                                </div>
                                                                                <div class='col-md-4'>
                                                                                    <label>Temperature</label>
                                                                                    <p>{{vm.aPatientInfo.temperature}} c</p>
                                                                                </div>
                                                                            </div>
                                                                            <br>
                                                                            <div class='row'>
                                                                                <div class='col-md-4'>
                                                                                    <label>Heart Rate</label>
                                                                                    <p>{{vm.aPatientInfo.heartRate}} BPM</p>
                                                                                </div>
                                                                                <div class='col-md-4'>
                                                                                    <label>Blood Pressure</label>
                                                                                    <p>{{vm.aPatientInfo.systolic}}/{{vm.aPatientInfo.diastolic}}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <br>
                                                            <div class='row panel'>
                                                                <div class='col-md-10 col-md-offset-1'>
                                                                    <h4>>> Diagnosis</h4>
                                                                </div>
                                                            </div>

                                                            <div class='row section'>
                                                                <div class='col-md-10 col-md-offset-1'>
                                                                    <div class='row'>
                                                                        <div class='col-md-3'>
                                                                            <label>Diagnosed by</label>
                                                                            <p>Doctor {{vm.focusHistory.doctorFirstName}} {{vm.focusHistory.doctorLastName}}</p>
                                                                        </div>
                                                                        <div class='col-md-9'>
                                                                            <div class='row'>
                                                                                <div class='col-md-4'>
                                                                                    <label>Disease</label>
                                                                                    <p>{{vm.focusHistory.diseaseName}}</p>
                                                                                </div>
                                                                                <div class='col-md-8'>
                                                                                    <label>Note</label>
                                                                                    <p>{{vm.checkCause(vm.focusHistory.doctorNote)}}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class='row panel'>
                                                                <div class='col-md-10 col-md-offset-1'>
                                                                    <h4>>> Dispensation</h4>
                                                                </div>
                                                            </div>

                                                            <div class='row section'>
                                                                <div class='col-md-10 col-md-offset-1'>
                                                                    <div class='row'>

                                                                        <div class='col-md-3'>
                                                                            <label>Dispensed by</label>
                                                                            <p>{{vm.drugUse.pharmacistFirstName}} {{vm.drugUse.pharmacistLastName}}</p>
                                                                        </div>

                                                                        <div class='col-md-9'>
                                                                            <table class='table'>
                                                                                <thead>
                                                                                    <th>#</th>
                                                                                    <th>Drug Name</th>
                                                                                    <th>Drug ID</th>
                                                                                    <th>Quantity</th>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr ng-repeat='drug in vm.drugUse.drugList'>
                                                                                        <td>{{$index+1}}</td>
                                                                                        <td>{{drug.drugName}}</td>
                                                                                        <td>{{drug.drugId}}</td>
                                                                                        <td>{{drug.drugQuantity}}</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                <div ui-if="patientTab == 2">
                                    
                                    <h3 class="page-header">Measurement</h3>
                                    <br>
                                    
                                    <div class='panel panel-info'>
                                        <div class='panel-heading'>Measured by <strong>Nurse {{vm.aPatientInfo.nurseFirstName}} {{vm.aPatientInfo.nurseLastName}}</strong></div>
                                        <br>
                                        
                                        <div class='section'>
                                            <div class='row'>
                                                <div class='col-md-10 col-md-offset-1'>
                                                    
                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>Weight</label>
                                                            <p>{{vm.aPatientInfo.weight}} kg</p>
                                                        </div>
                                                        <div class='col-md-4'>
                                                            <label>Height</label>
                                                            <p>{{vm.aPatientInfo.height}} cm</p>
                                                        </div>
                                                        <div class='col-md-4'>
                                                            <label>Temperature</label>
                                                            <p>{{vm.aPatientInfo.temperature}} c</p>
                                                        </div>
                                                    </div>
                                                    
                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>Heart Rate</label>
                                                            <p>{{vm.aPatientInfo.heartRate}} BPM</p>
                                                        </div>
                                                        <div class='col-md-4'>
                                                            <label>Blood Pressure</label>
                                                            <p>{{vm.aPatientInfo.systolic}}/{{vm.aPatientInfo.diastolic}}</p>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                <div ui-if="patientTab == 3">
                                    <h3 class="page-header">Diagnostic Result</h3>
                                    <br>
                                    
                                    <div class='panel panel-info'>
                                        <div class='panel-heading'>Patient Problem</div>
                                        <br>
                                        <div class='row section'>
                                            <div class='col-md-1 col-md-offset-1'>
                                                <p><strong>Cause : </strong></p>
                                            </div>
                                            <div class='col-md-8'>
                                                <p>{{vm.checkCause(vm.focusAppointment.cause)}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <br>
                                    <div class='panel panel-info'>
                                        <div class='panel-heading'>Diagnosis</div>
                                        
                                        <br>
                                        <div class='row'>
                                            <div class='col-md-10 col-md-offset-1'>
                                                <form name="EditForm">
                                                    
                                                    <div class="row form-group">
                                                        <div class="col-md-12">
                                                            <form-group>
                                                                <label>Disease</label>
                                                                <select class="form-control" ng-model='vm.pState.diseaseId' ng-change='vm.findDisease(vm.pState,vm.pState.diseaseId)'>
                                                                    <option ng-repeat='tDisease in vm.diseaseList' value={{tDisease.diseaseId}}>{{tDisease.diseaseName}}</option>
                                                                </select>
                                                            </form-group>
                                                        </div>
                                                    </div>
                                                    
                                                    <br>
                                                    <div class="row form-group">
                                                        <div class="col-md-12">
                                                            <form-group>
                                                                <label>Description</label>
                                                                <textarea class="form-control" ng-model='vm.pState.doctorNote' rows="6"></textarea>
                                                            </form-group>
                                                        </div>
                                                    </div>
                                                    
                                                    <br>
                                                    <div class="row form-group">
                                                        <div class='col-md-12'>
                                                            <table class='table'>
                                                                <thead>
                                                                    <th>Drug Name</th>
                                                                    <th>Drug ID</th>
                                                                    <th>Quantity</th>
                                                                    <th>#</th>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat='drug in vm.drugOrder.drugList'>
                                                                        <td>
                                                                            <select class="form-control" ng-model='drug.drugId' ng-change='vm.findDrug(drug,drug.drugId)'>
                                                                                <option ng-repeat='tDrug in vm.drugList' value={{tDrug.drugId}}>{{tDrug.drugName}}</option>
                                                                            </select>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <p>{{drug.drugId}}</p>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <input type="text" class="form-control" placeholder="quantity" ng-model='drug.drugQuantity'>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <a ng-click='vm.removeDrug($index)'>Remove</a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            
                                                            <div class='btn btn-primary pull-right' ng-click='vm.addDrug()'><i class="fa fa-plus" style="color:white"></i> Drug</div>

                                                        </div>
                                                    </div>
                                                    <br>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                <div ui-if="patientTab == 4" class='col-md-12'>
                                    <h3 class="page-header">Appointment</h3>
                                    
                                    <div class='panel panel-info'>
                                        <div class='panel-heading'>Appointment Date : <strong>{{vm.newAppointment.date}}</strong> At Time :<strong>{{vm.timeByPeriod(vm.newAppointment.period,0)}} - {{vm.timeByPeriod(vm.newAppointment.period,1)}}</strong></div>
                                        
                                        <div class='section'>
                                            <div class="row">
                                                <div class="col-md-10 col-md-offset-1">
                                                    <label>Cause</label>
                                                    <textarea class='form-control' ng-model='vm.newAppointment.cause' rows='3'></textarea>
                                                </div>
                                            </div>
                                            <br>
                                            
                                            <br>
                                            <div class="row panel">
                                                <div class='col-md-2 col-md-offset-9'>
                                                    <div class='btn btn-primary  pull-right' ng-click='vm.saveNewAppointment()'><i class="fa fa-calendar-plus-o "></i> Save Appointment </div>

                                                </div>

                                                <div class="col-md-8 col-md-offset-2">
                                                    <h3 class="text-center">{{ vm.calendarTitle }}</h3>
                                                    <div class="row">
                                                        <div class="col-md-6 text-center">
                                                            <div class="btn-group">

                                                                <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" decrement="vm.calendarView" ng-click='vm.toPreviousMonth()'>
                                                                    Previous
                                                                </button>
                                                                <button class="btn btn-default" mwl-date-modifier date="vm.calendarDay" set-to-today ng-click='vm.toThisMonth()'>
                                                                    Today
                                                                </button>
                                                                <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" increment="vm.calendarView" ng-click='vm.toNextMonth()'>
                                                                    Next
                                                                </button>
                                                            </div>
                                                        </div>

                                                        <br class="visible-xs visible-sm">

                                                        <div class="col-md-6 text-center">
                                                            <div class="btn-group">
                                                                <!--                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'year'">Year</label>-->
                                                                <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'month'">Month</label>
                                                                <!--                                                        <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'week'">Week</label>-->
                                                                <!--                                                    <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'day'">Day</label>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <mwl-calendar events="vm.events" view="vm.calendarView" view-title="vm.calendarTitle" current-day="vm.calendarDay" on-event-click="vm.changeDate(date,type,num_current_event)" on-event-times-changed="vm.eventTimesChanged(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd" edit-event-html="'<i class=\'fa fa-pencil white\'></i>'" delete-event-html="'<i class=\'fa fa-remove white\'></i>'" on-edit-event-click="vm.eventEdited(calendarEvent)" on-delete-event-click="vm.eventDeleted(calendarEvent)" auto-open="false" day-view-start="06:00" day-view-end="22:00" day-view-split="30" cell-modifier="vm.modifyCell(calendarCell)">
                                                    </mwl-calendar>
                                                    <br>
                                                    <p class='text-right'><small>*The Number in Calendar is Number of Available Appointment in That Day</small></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
                <div class="modal-footer lg">
                    <button ui-turn-off="modals" class="btn btn-default" ng-click='vm.clearState()'>Close</button>
                    <button ui-turn-off="{{vm.modalControl}}" class="btn btn-primary" ng-click='vm.saveState()'>Save changes</button>
                </div>
            </div>
        </div>

        <div ui-yield-to="modalsDelete"></div>
        <div ui-content-for="modalsDelete">
            <div class="modal" ui-if="modalsDelete" ui-state='modalsDelete'>
                <div class="modal-backdrop in"></div>
                <div class="modal-dialog">
                    <div class="modal-content">
                        
                        <div class="modal-header">
                            <h4 class="modal-title" style="color:red">Warning</h4>
                        </div>
                        
                        <div class="modal-body">
                            You can undo this action.Are you sure to continute?
                        </div>
                        
                        <div class="modal-footer">
                            <button ui-turn-off="modalsDelete" class="btn btn-default">Cancel</button>
                            <button ui-turn-off="modalsDelete" class="btn btn-danger" ng-click="vm.deleteWorkDay()">Continute</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="scrollable-header section" style="z-index:900">
        <div class="container section">
            <div class="row">
                <div class="col-md-12">
                    <h2>My Dashboard</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="scrollable-content" style="position: relative;overflow: hidden;">
        <div ui-state="activeTab" default="1" style="height:inherit">
            
            <ul class="nav nav-tabs container">
                
                <li ui-class="{'active': activeTab == 1}">
                    <a ui-set="{'activeTab': 1}">Activity Today</a>
                </li>
                
                <li ui-class="{'active': activeTab == 2}" ng-click='vm.toThisMonthD()'>
                    <a ui-set="{'activeTab': 2}">Appointment</a>
                </li>
            </ul>

            <div ui-if="activeTab == 1">
                <div class="container" style="height:inherit">
                    <div class="row" style="height:inherit">
                        <div class="col-md-12" style="height:inherit">
                            <h3 class="page-header">Welcome <small>Doctor {{vm.aDoctor.firstName}} {{vm.aDoctor.lastName}}</small></h3>
                            <div style='padding-top:20px;'>
                                <div class='panel'>
                                    <h4 ng-show='vm.allAppointment.length!=0'>Today Appointment</h4>
                                    <h4 ng-show='vm.allAppointment.length==0'>Does not Have Appointment Today</h4>
                                    <table class="table table-striped" ng-show='vm.allAppointment.length!=0'>
                                        <thead>
                                            <tr>

                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>SSID</th>
                                                <th>Cause</th>
                                            </tr>
                                        </thead>
                                        <tbody ng-repeat='appointment in vm.allAppointment'>
                                            <tr class="content-row" ui-turn-on='modals' ng-show='vm.checkAppointment(appointment)' ng-click="vm.setState(appointment)">
                                                <td>{{appointment.patientFirstName}}</td>
                                                <td>{{appointment.patientLastName}}</td>
                                                <td>{{appointment.patientSsid}}</td>
                                                <td>{{vm.checkCause(appointment.cause)}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div style="position:relative;height:80%">
                <div ui-if="activeTab == 2" class="section scrollable">
                    
                    <div class="container">
                        <h3 class="page-header" style="inine"><div class="btn btn-floating btn-large " ng-click="vm.toggleAdd()" ><i class="fa fa-calendar-plus-o fa-lg"></i> </div> Appointment </h3>
                    </div>
                    
                    <div class="scrollable-content">
                        <div class="container">
                            <div class="row" id="demo">
                                <div class="col-md-8 col-md-offset-2">
                                    <h2 class="text-center">{{ vm.calendarTitle }}</h2>
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <div class="btn-group">
                                                <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" decrement="vm.calendarView" ng-click='vm.toPreviousMonthD()'>
                                                    Previous
                                                </button>
                                                <button class="btn btn-default" mwl-date-modifier date="vm.calendarDay" set-to-today ng-click='vm.toThisMonthD()'>
                                                    Today
                                                </button>
                                                <button class="btn btn-primary" mwl-date-modifier date="vm.calendarDay" increment="vm.calendarView" ng-click='vm.toNextMonthD()'>
                                                    Next
                                                </button>
                                            </div>
                                        </div>
                                        
                                        <br class="visible-xs visible-sm">
                                        <div class="col-md-6 text-center">
                                            <div class="btn-group">
                                                <label class="btn btn-primary" ng-model="vm.calendarView" btn-radio="'month'">Month</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <br>
                                    <mwl-calendar events="vm.eventsD" view="vm.calendarView" view-title="vm.calendarTitle" current-day="vm.calendarDay" on-event-click="vm.setCurrentCeilOperation(date,type,num_current_event)" on-event-times-changed="vm.eventTimesChanged(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd" edit-event-html="'<i class=\'fa fa-pencil white\'></i>'" delete-event-html="'<i class=\'fa fa-remove white\'></i>'" on-edit-event-click="vm.eventEdited(calendarEvent)" on-delete-event-click="vm.eventDeleted(calendarEvent)" auto-open="false" day-view-start="06:00" day-view-end="22:00" day-view-split="30" cell-modifier="vm.modifyCell(calendarCell)">
                                    </mwl-calendar>
                                    
                                    <br>
                                    <p class='text-right'><small>*The Number in Calendar is Number of Appointment in That Day</small></p>
                                    <div class="section">
                                    </div>
                                    
                                    <div class="section">
                                    </div>
                                    
                                    <div class="section">
                                    </div>

                                    <!--
                                        <br>
                                        <br> {{vm.calendarView}}
                                        <br>
-->

                                    <!--
                                        <script type="text/ng-template" id="modalContent.html">
                                            <div class="modal-header">
                                                <h3 class="modal-title">Event action occurred!</h3>
                                            </div>
                                            <div class="modal-body">
                                                <p>Action: <pre>{{ vm.action }}</pre></p>
                                                <p>Event: <pre>{{ vm.event | json }}</pre></p>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-primary" ng-click="$close()">OK</button>
                                            </div>
                                        </script> 
-->
                                    <script type="text/ng-template" id="modalContent.html">
                                        <div class="modal-header">
                                            <button class="close" ng-click='$close()'>&times;</button>
                                            <h4 class="modal-title">Patient Infomation</h4>
                                        </div>
                                        <div class="modal-body" style='height:82%;'>
                                            <div class="container" style='height:100%; overflow:auto;'>

                                                <div class="col-md-3">
                                                    <div class='row'>
                                                        <h3>Basic Info</h3>
                                                        <img src='../img/jack.jpg' class='img-rounded img-responsive' />
                                                        <p><span ng-bind='vm.pInfo.firstName'></span> <span ng-bind='vm.pInfo.lastName'></span></p>
                                                    </div>
                                                    <div ui-state="patientTab" default="1" class='row'>
                                                        <ul class="nav nav-pills nav-stacked">
                                                            <li ui-class="{'active': patientTab == 1}">
                                                                <a ui-set="{'patientTab': 1}">History</a>
                                                            </li>
                                                            <li ui-class="{'active': patientTab == 2}">
                                                                <a ui-set="{'patientTab': 2}">Measurement</a>
                                                            </li>
                                                            <li ui-class="{'active': patientTab == 3}">
                                                                <a ui-set="{'patientTab': 3}">Diagnostic Result</a>
                                                            </li>
                                                            <li ui-class="{'active': patientTab == 4}">
                                                                <a ui-set="{'patientTab': 4}">Appointment</a>
                                                            </li>
                                                        </ul>


                                                    </div>
                                                </div>

                                                <div class="col-md-8" style='height:100%; overflow:auto;'>
                                                    <div ui-if="patientTab == 1" class='col-md-12'>
                                                        <h3 class="page-header">History</h3>
                                                        <div class='row'>
                                                            <div class='col-md-12'>
                                                                <div class='panel panel-default'>
                                                                    <div class='panel-heading'>General Information</div>
                                                                    <div class='panel-body'>
                                                                        <p>{{vm.pInfo.ssid}} {{vm.pInfo.id}} {{vm.pInfo.sex}} {{vm.pInfo.address}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class='col-md-12'>
                                                                <div class='panel panel-default'>
                                                                    <div class='panel-heading'>Medical History</div>
                                                                    <ul class='list-group'>
                                                                        <li class='list-group-item' ng-repeat='history in vm.pHistory'>
                                                                            <p>{{history.date}}</p>
                                                                            <p>{{history.symptom}}</p>
                                                                            <p>{{history.date}}</p>
                                                                            <p>{{history.symptom}}</p>
                                                                            <p>{{history.date}}</p>
                                                                            <p>{{history.symptom}}</p>
                                                                            <p>{{history.date}}</p>
                                                                            <p>{{history.symptom}}</p>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div ui-if="patientTab == 2" class='col-md-12'>
                                                        <h3 class="page-header">Measurement</h3>
                                                        <div class="row">
                                                            <div class='col-md-12'>
                                                                <small>Measure by <strong>{{vm.pMeasure.nurse}}</strong></small>
                                                                <br>
                                                                <br>
                                                                <dl class="dl-horizontal">
                                                                    <dt><h3>Pressure</h3></dt>
                                                                    <dd>
                                                                        <p>{{vm.pMeasure.pressure}}</p>
                                                                    </dd>
                                                                    <br>
                                                                    <dt><h3>Heart Rate</h3></dt>
                                                                    <dd>
                                                                        <p>{{vm.pMeasure.hrRate}}</p>
                                                                    </dd>
                                                                </dl>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div ui-if="patientTab == 3" class='col-md-12'>
                                                        <h3 class="page-header">Result</h3>
                                                        <p>
                                                            <!-- ... -->
                                                        </p>
                                                    </div>
                                                    <div ui-if="patientTab == 4" class='col-md-12'>
                                                        <h3 class="page-header">Appointment</h3>
                                                        <p>
                                                            <!-- ... -->
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-default" ng-click='vm.clearState();$close()'>Close</button>
                                                <button class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
