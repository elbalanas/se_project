<?php include("../inc/config.php"); ?>
    <div ui-yield-to="modals"></div>
    <div ui-content-for="modals">
        <div class="modal" ui-if="modals" ui-state='modals'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog" style='height:100%;'>
                <div class="modal-content" style='height:90%;'>
                    <div class="modal-header">
                        <button class="close" ui-turn-off="modals">&times;</button>
                        <h4 class="modal-title">Patient Infomation</h4>
                    </div>

                    <div class="modal-body" style='height:85%; '>

                        <div class="container" style='height:100%; overflow:auto;'>
                            <div class="col-md-12">

                                <div ui-state="patientTab" default="2" class='row'>
                                    <ul class="nav nav-pills nav-tabs">
                                        <li ui-class="{'active': patientTab == 1}">
                                            <a ui-set="{'patientTab': 1}">Patient Information</a>
                                        </li>
                                        <li ui-class="{'active': patientTab == 2}">
                                            <a ui-set="{'patientTab': 2}">Measurement</a>
                                        </li>
                                        <!--
                                        <li ui-class="{'active': patientTab == 3}">
                                            <a ui-set="{'patientTab': 3}">Result</a>
                                        </li>
                                        <li ui-class="{'active': patientTab == 4}">
                                            <a ui-set="{'patientTab': 4}">Appointment</a>
                                        </li>
-->
                                    </ul>


                                </div>
                            </div>


                            <div class="col-md-12" style='height:80%; overflow:auto;'>
                                <div ui-if="patientTab == 1">
                                    <h3 class="page-header">Patient Information</h3>
                                    <br>
                                    <div>
                                        <div class='panel panel-info'>
                                            <div class='panel-heading'>General Information</div>
                                            <br>
                                            <div class='row'>
                                                <div class='col-md-3 col-md-offset-1'>
                                                    <img src='../img/jack.jpg' class='img-rounded img-responsive' height="250" width="150">
                                                </div>
                                                <div class='col-md-8'>
                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>Name</label>
                                                            <p>{{vm.aPatient.firstName}} {{vm.aPatient.lastName}}</p>
                                                        </div>
                                                        <div class='col-md-3'>
                                                            <label>SSID</label>
                                                            <p>{{vm.aPatient.ssid}}</p>
                                                        </div>
                                                        <div class='col-md-2'>
                                                            <label>Sex</label>
                                                            <p>{{vm.tranSex(vm.aPatient.sex)}}</p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>Email</label>
                                                            <p>{{vm.aPatient.email}}</p>
                                                        </div>
                                                        <div class='col-md-4'>
                                                            <label>Telephone</label>
                                                            <p>{{vm.aPatient.telephone}}</p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-8'>
                                                            <label>Address</label>
                                                            <p>{{vm.aPatient.address}}</p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div>
                                        <div class='panel panel-info'>
                                            <div class='panel-heading'>Today Activity</div>
                                            <div class='row section'>
                                                <br>
                                                <div class='col-md-12'>
                                                    <div class='row'>
                                                        <div class='col-md-5 col-md-offset-1'>
                                                            <label>Appointment with</label>
                                                            <p>Doctor {{vm.focusPerson.doctorFirstName}} {{vm.focusPerson.doctorLastName}}</p>
                                                        </div>
                                                        <div class='col-md-5'>
                                                            <label>Department</label>
                                                            <p>{{vm.focusPerson.departmentName}}</p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-10 col-md-offset-1'>
                                                            <label>Cause</label>
                                                            <p>{{vm.checkCause(vm.focusPerson.cause)}}</p>
                                                        </div>
                                                    </div>
                                                    
                                                
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <!--
                                    <div class='row section'>
                                        <div class='col-md-3'>
                                        
                                        </div>
                                        <div class='col-md-9 panel panel-info'>
                                            <div class='panel-heading'>General Information</div>
                                            <div class='row'>
                                                <div class='col-md-3'>
                                                    <label>FirstName</label>
                                                    <p>{{vm.pState.firstName}}</p>
                                                </div>
                                                <div class='col-md-3'>
                                                    <label>LastName</label>
                                                    <p>{{vm.pState.lastName}}</p>
                                                </div>
                                                <div class='col-md-2'>
                                                    <label>SSID</label>
                                                    <p>{{vm.pState.ssid}}</p>
                                                </div>
                                                <div class='col-md-1 col-md-offset-1'>
                                                    <label>Status</label>
                                                    <i class='fa fa-check fa-2x' style='color:#00ed00;' ng-show='vm.checkStatus(vm.pState)'></i>
                                                    <i class='fa fa-check fa-2x' style='color:#ddd;' ng-hide='vm.checkStatus(vm.pState)'></i>
                                                </div>
                                            </div>
                                            <div class='row'>
                                                <div class='col-md-4'>
                                                    <label>Doctor</label>
                                                    <p>{{vm.pState.doctor}}</p>
                                                </div>
                                                <div class='col-md-4'>
                                                    <label>Department</label>
                                                    <p>{{vm.pState.department}}</p>
                                                </div>
                                            </div>
                                            <br>
                                            <div class='row'>
                                                <div class='col-md-12'>
                                                    <label>cause</label>
                                                    <p>{{vm.pState.cause}}</p>
                                                </div>
                                            </div>
                                            <br>
                                            <div class='row'>
                                                <div class='col-md-12'>
                                                    <label>Abnormality</label>
                                                    <p>{{vm.pState.abnormality}}</p>
                                                </div>
                                            </div>
                                            </div>
                                        
                                    </div>
-->
                                    <!--
                                        <div class='col-md-12'>
                                            <div class='panel panel-default'>
                                                <div class='panel-heading'>General Information</div>
                                                <div class='panel-body'>
                                                    <div class="col-md-3">
                                                        <img ng-src="../img/{{vm.ddata.patient.info.img}}" class='img-rounded img-responsive' height="250" width="150" />
                                                    </div>
                                                    <div class='col-md-9'>
                                                        <p>{{vm.ddata.patient.info.ssid}}</p>
                                                        <p>{{vm.ddata.patient.info.email}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class='col-md-12'>
                                            <div class='panel panel-default'>
                                                <div class='panel-heading'>Medical History</div>
                                                <ul class='list-group'>
                                                    <li class='list-group-item' ng-repeat='history in vm.ddata.patient.history'>
                                                        <p>{{history.date.toLocaleDateString()}}</p>
                                                        <p>{{history.result.disease}}</p>
                                                        <p>{{history.doctor}}</p>
                                                        <p>{{history.result.note}}</p>
                                                        <ul>
                                                            <li ng-repeat='drug in history.drug.use'>
                                                                <p>{{drug.name}} - <small>{{drug.value}}</small></p>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
-->


                                </div>
                                <div ui-if="patientTab == 2">
                                    <h3 class="page-header">Measurement</h3>
                                    <br>
                                    <div class="panel panel-info">
                                        <div class='panel-heading'>Measurement Result</div>
                                        <br>
                                        <div class='section'>
                                            <div class="row">
                                                <div class='col-md-3 col-md-offset-1'>
                                                    <label>Weight</label>
                                                    <div class="input-group">

                                                        <input type="text" ng-model="vm.pState.weight" class="form-control" aria-describedby="weight-unit">
                                                        <span class="input-group-addon" id="weight-unit">kg</span>
                                                    </div>
                                                </div>
                                                <div class='col-md-3'>
                                                    <label>Height</label>
                                                    <div class="input-group">

                                                        <input type="text" ng-model="vm.pState.height" class="form-control" aria-describedby="height-unit">
                                                        <span class="input-group-addon" id="height-unit">cm</span>
                                                    </div>
                                                </div>
                                                <div class='col-md-3'>
                                                    <label>Temperature</label>
                                                    <div class="input-group">

                                                        <input type="text" ng-model="vm.pState.temperature" class="form-control" aria-describedby="temp-unit">
                                                        <span class="input-group-addon" id="temp-unit">C</span>
                                                    </div>
                                                </div>
<!--
                                                <div class='col-md-1'>
                                                    <label>Status</label>
                                                    <i class='fa fa-check fa-2x' style='color:#00ed00;' ng-show='vm.checkStatus(vm.pState)'></i>
                                                    <i class='fa fa-check fa-2x' style='color:#ddd;' ng-hide='vm.checkStatus(vm.pState)'></i>
                                                </div>
-->
                                            </div>

                                            <br>
                                            <div class="row">
                                                <div class='col-md-3 col-md-offset-1'>
                                                    <label>Blood Pressure</label>
                                                    <div class="form-group row">
                                                        <div class='col-md-5'>
                                                            <input type="text" ng-model="vm.pState.systolic" class="form-control" aria-describedby="bpressure-unit">
                                                        </div>
                                                        <div class='col-md-1'>/</div>
                                                        <div class='col-md-5'>
                                                            <input type="text" ng-model="vm.pState.diastolic" class="form-control" aria-describedby="bpressure-unit">
                                                        </div>
                                                        <!--                                                                <span class="input-group-addon" id="bpressure-unit"></span>-->
                                                    </div>
                                                </div>
                                                <div class='col-md-3'>
                                                    <label>Heart Rate</label>
                                                    <div class="form-group">

                                                        <input type="text" ng-model="vm.pState.heartRate" class="form-control" aria-describedby="heartRate-unit">
                                                        <!--                                                                <span class="input-group-addon" id="heartRate-unit"></span>-->
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <!--
                                <div ui-if="patientTab == 3" class='col-md-12'>
                                    <h3 class="page-header">Result</h3>

                                    <form name="EditForm">
                                        <div class="row form-group">
                                            <div class="col-md-11">
                                                <form-group>
                                                    <label>Disease</label>
                                                    <input type="text" ng-model="vm.pResult.result.disease" class="form-control">
                                                </form-group>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-11">
                                                <form-group>
                                                    <label>Description</label>
                                                    <textarea class="form-control" ng-model='vm.pResult.result.note' rows="8"></textarea>
                                                </form-group>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-11">
                                                <form class="form-inline">
                                                    <label>Drug Order</label>
                                                    <div class='form-group' style='display:block' ng-repeat='order in vm.pResult.drug.order'>
                                                        <select class="form-control" style='display:inline; width:55%; margin-right:2%' ng-model='order.name'>
                                                            <option ng-repeat='drug in vm.drugList' value={{drug}}>{{drug}}</option>
                                                        </select>
                                                        <input type="text" class="form-control" placeholder="amount" style='display:inline; width:15%; margin-right:2%' ng-model='order.quantity'>
                                                        <div class='btn btn-danger' style='display:inline' ng-click='vm.removeDrug($index)'>remove</div>
                                                    </div>

                                                </form>
                                                <div class='btn btn-primary pull-right' ng-click='vm.addDrug()'>Add Drug</div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label>Completed</label>
                                                <ui-switch ng-model="vm.pResult.status" class="ng-pristine ng-untouched ng-valid ng-isolate-scope switch switch-transition-enabled active">
                                                    <div class="switch-handle"></div>
                                                </ui-switch>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <div ui-if="patientTab == 4" class='col-md-12'>
                                    <h3 class="page-header">Appointment</h3>
                                    <p>
                                         ... 
                                    </p>
                                </div>
                            </div>
-->

                            </div>
                        </div>



                    </div>

                </div>
                <div class="modal-footer lg">

                    <button ui-turn-off="modals" class="btn btn-default" ng-click='vm.clearState()'>Close</button>
                    <button ui-turn-off="{{vm.modalControl}}" class="btn btn-primary" ng-click='vm.saveState()'>Save changes</button>
                </div>
            </div>

        </div>
    </div>
    <!--    </div>-->
    <!--
        <div ui-yield-to="modalsDelete"></div>
        <div ui-content-for="modalsDelete">
            <div class="modal" ui-if="modalsDelete" ui-state='modalsDelete'>
                <div class="modal-backdrop in"></div>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">


                            <h4 class="modal-title" style="color:red">Warning</h4>

                        </div>
                        <div class="modal-body">
                            You can undo this action.Are you sure to continute?

                        </div>
                        <div class="modal-footer">

                            <button ui-turn-off="modalsDelete" class="btn btn-default">Cancel</button>
                            <button ui-turn-off="modalsDelete" class="btn btn-danger" ng-click="vm.deleteWorkDay()">Continute</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
-->

    <div class="scrollable-header section" style="z-index:900">
        <div class="container section">
            <div class="row">
                <div class="col-md-12">
                    <h2>My Dashboard</h2>
                </div>
            </div>
        </div>
    </div>


    <div class="scrollable-content" style="position: relative;overflow: hidden;">


        <div ui-state="activeTab" default="1" style="height:inherit">

            <ul class="nav nav-tabs container">



                <li ui-class="{'active': activeTab == 1}">
                    <a ui-set="{'activeTab': 1}">Activity</a>
                </li>
                <!--
                <li ui-class="{'active': activeTab == 2}">
                    <a ui-set="{'activeTab': 2}">Appointment</a>
                </li>
-->



            </ul>


            <div style="position:relative;height:90%">
                <div class="section scrollable" ui-if="activeTab == 1">
                    <div class="scrollable-content">
                        <div class="container" style="height:inherit">

                            <div class="row" style="height:inherit">
                                <div class="col-md-12" style="height:inherit">



                                    <h3 class="page-header">Welcome <small>Nurse {{vm.aNurse.firstName}}  {{vm.aNurse.lastName}}</small></h3>
                                    <!--
                            <h3 ng-show='vm.hasApp'>You have Appointment Today at {{vm.startTime}} - {{vm.finTime}}.</h3>
                            <h4 ng-hide='vm.hasApp'>You don't have Appointment Today. Your next Appointment is <span style='font-weight:bold;'>{{vm.Fdata.date}}</span> at {{vm.startTime}} - {{vm.finTime}}.</h4>
-->
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>
                                                <input type="text" class="form-control" ng-model='vm.searchArg'  placeholder="Search for..." aria-describedby="sizing-addon1">

                                            </div>

                                            <!-- /input-group -->
                                        </div>
                                    </div>
                                    <div ng-show="true">
                                        <br>
                                        <h4 class="page-header" style="display:inline" ng-show='vm.allAppointment.length!=0'>Patient List</h4>
                                        <h4 ng-show='vm.allAppointment.length==0'>Does not Have Appointment Today</h4>
<!--                                        <h5 class='text-center' ng-show='vm.isEmpty()'><strong >Search Not Found</strong></h5>-->
                                        <table class="table table-striped" ng-show='vm.allAppointment.length!=0'>
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Doctor</th>
                                                    <th>Department</th>
                                                    <th>SSID</th>
<!--                                                    <th>Status</th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="content-row" ui-turn-on='modals' ng-click="vm.setState(person)" ng-repeat='person in vm.allAppointment' ng-show='vm.pSearch(person)'>

                                                    <th scope="row">{{$index+1}}</th>
                                                    <td>{{person.patientFirstName}} {{person.patientLastName}}</td>
                                                    <td>{{person.doctorFirstName}} {{person.doctorLastName}} </td>
                                                    <td>{{person.departmentName}}</td>
                                                    <td>{{person.patientSsid}}</td>
<!--
                                                    <td>
                                                        <i class='fa fa-check fa-lg' style='color:#00fa00;' ng-show='vm.checkStatus(person)'></i>
                                                        <i class='fa fa-check fa-lg' style='color:#ddd;' ng-hide='vm.checkStatus(person)'></i>

                                                    </td>
-->
                                                </tr>
                                            </tbody>
                                            
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
