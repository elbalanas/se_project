<!DOCTYPE html>
<html>


<?php
        
	include("inc/config.php");
	include(root."inc/head.php");
	//echo '<script src="'.base_url.'js/core/AppHome.js"></script>';
    echo '<link rel="stylesheet" type="text/css" href="'.base_url.'lib/sweet-alert/dist/sweetalert.css">';
    echo '<script src="'.base_url.'lib/sweet-alert/dist/sweetalert.min.js"></script>';
    echo '<script src="'.base_url.'js/core/first_page.js"></script>';

	?>

    <body>
        <div ng-app="myApp" style="height:inherit">

            <div class="col-md-12 col-xs-12 text-center bg-primary">
                <p>
                    <h2>Hospital</h2>
                    <h2>OPD-SYSTEM</h2>
                    <small>make Appointment to be <strong>easy to use.</strong></small>
                </p>

            </div>
        <div class="navbar navbar-app navbar-absolute-bottom" style="padding-top:15px;">
            <p>You can integrate with my service by read <a href="api/API.html" style="color:#222222;">API(click here)</a> .</p>
        </div>
            <div class="container" style="height:inherit" ng-controller="firstPageCtrl">
                <div class="row" style="height:inherit">
                    <div class="col-md-6" style="height:inherit">
                        <div ui-state="activeTab" default="1" style="height:inherit">
                            <ul class="nav nav-tabs">
                                <li ui-class="{'active': activeTab == 1}">
                                    <a ui-set="{'activeTab': 1}">Sign in</a>
                                </li>
                                <li ui-class="{'active': activeTab == 2}">
                                    <a ui-set="{'activeTab': 2}">Sign up</a>
                                </li>

                            </ul>

                            <div ui-if="activeTab == 1">
                                <h3 class="page-header">Sign in</h3>
                                <?php if(isset($_GET['error'])){
									$error=$_GET['error'];
									echo '<h5><p class="text-danger">'.$error.'</p></h5>';
								} ?>
                                <form role="form" action="<?php echo base_url."backend/sign_in.php ";?>" method="post">
                                    <div class="form-group text-left">
                                        <label for="InputEmail1">
                                            <h4>Username</h4></label>
                                        <input type="text" class="form-control" id="InputEmail1" placeholder="Username" name="username" required>
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="InputPassword1">
                                            <h4>Password</h4></label>
                                        <input type="password" class="form-control" id="InputPassword1" placeholder="Password" name="password" required>
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                                    <!--
                                    <a href=<?php echo base_url."doctor/index.php" ?> >Go to Doctor page</a>
                                    <a href=<?php echo base_url."patient/index.php" ?> >Go to Patient page</a>
                                    <a href=<?php echo base_url."nurse/index.php" ?> >Go to Nurse page</a>
                                    <a href=<?php echo base_url."pharmacy/index.php" ?> >Go to Pharmacy page</a>
-->
                                </form>
                            </div>
                            <div style="position:relative;height:100%" >
                            <div ui-if="activeTab == 2"  class="scrollable" style="padding-bottom: 45%;">
                                <h3 class="page-header">Sign up</h3>
                                <form class="scrollable-content" role="form" >
                                    <div class="form-group text-left">
                                        <label for="exampleInputEmail1">
                                            <h4>Username(ssid)</h4></label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Username" name="username" ng-model="pState.ssid">
                                    </div>
                                    <div class="form-group text-left bg-warning">
<!--                                        <label for="exampleInputPassword1">-->
                                        <h5> <b>Note:</b>Password will be equal to ssid and you can change after log in.</h5>
<!--                                        </label>-->
<!--                                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" ng-model="pState.password">-->
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="exampleInputPassword1">
                                            <h4>First Name</h4></label>
                                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" ng-model="pState.firstName">
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="exampleInputPassword1">
                                            <h4>Last Name</h4></label>
                                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" ng-model="pState.lastName">
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="exampleInputPassword1">
                                            <h4>Sex</h4></label>
                                        <select class="form-control" ng-model="pState.sex">
                                          <option value="0">Male</option>
                                          <option value="1">Female</option>

                                        </select>
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="exampleInputPassword1">
                                            <h4>Address</h4></label>
                                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" ng-model="pState.address">
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="exampleInputPassword1">
                                            <h4>Email</h4></label>
                                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" ng-model="pState.email">
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="exampleInputPassword1">
                                            <h4>Telephone</h4></label>
                                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" ng-model="pState.telephone">
                                    </div>


                                    <button type="submit" class="btn btn-primary btn-lg" ng-click="signUp()" >Submit</button>
                                </form>
                            </div>
                            </div>

                        </div>
                    </div>

                    <div class='col-md-6'>
                        <img src='img/hospital_banner.jpg' class='img-responsive' />
                    </div>
                </div>
            </div>

    </body>
<?php echo '<script src="'.base_url.'js/factory/patientClass.js"></script>' ?>
</html>
