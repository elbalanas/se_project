<?php

function connectToDatabase() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$conn = new mysqli($servername, $username, $password);
		if($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
        //$db->exec("SET NAMES 'utf8' ");

        $conn->query("SET NAMES 'utf8' ");
		return $conn;
	}

?>
