<?php
	session_start();
	$sign_in=false;
	if(isset($_SESSION['username'])&&isset($_SESSION['recently'])){
		include(root."inc/connect_database.php");
		$results=$db->prepare("SELECT recently FROM members WHERE username=?");
		$results->bindParam(1,$_SESSION['username']);
		$results->execute();
		$true_recently=$results->fetch(PDO::FETCH_ASSOC);
		if($_SESSION['recently']==$true_recently){
			$sign_in=true;
		}
	}
	if(!$sign_in&&!isset($home)){
		$go="Location: ".base_url;
		header($go);
		exit;
	}
?>