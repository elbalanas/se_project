<?php echo
	'<head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>OPD-SYSTEM</title>
		<link rel="stylesheet" href="'.base_url.'lib/mobile-angular-ui/css/mobile-angular-ui-base.min.css">
		<link rel="stylesheet" href="'.base_url.'lib/mobile-angular-ui/css/mobile-angular-ui-hover.min.css">
		<link rel="stylesheet" href="'.base_url.'lib/mobile-angular-ui/css/mobile-angular-ui-desktop.min.css">

        <link rel="stylesheet" href="'.base_url.'css/main.css">
		

        
        <script src="'.base_url.'lib/angular-1.4.6/angular.min.js"></script>
        <script src="'.base_url.'lib/angular-1.4.6/angular-route.min.js"></script>
        
        <script src="'.base_url.'lib/mobile-angular-ui/js/mobile-angular-ui.js"></script>

        
        
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>';
?>

