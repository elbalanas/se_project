app.factory('drugUseClass', function ($http, $q) {
    this.drugUse = [
        {
            drugUseId: '',
            prescriptionId: '',
            patientId: '',
            doctorId: '',
            doctorFirstName: '',
            doctorLastName: '',
            pharmacistId: '',
            pharmacistFirstName: '',
            pharmacistLastName: '',
            medicalId: '',
            date: '',
            drugList: [
                {
                    drugUseOrderId: '',
                    drugId: '',
                    drugName: '',
                    drugQuantity: ''
                }
            ]
        }
    ]
    return {
        drugUse: this.drugUse,
        //แก้ไขเพิ่มให้รับ parameter ได้แบบนี้ทำงานให้งานได้ดีขึ้น
        //ดึงข้อมูลการจ่ายยาให้คนไข้คนนี้ในวันนั้นๆออกมา
        getDrugUseById: function (patientId, date) {
            var data = this.drugUse;
            var promise = $http({
                    method: 'POST',
                    url: '../backend/drugUseClass.php',
                    data: {
                        action: "getDrugUseById",
                        patientId: patientId,
                        date: date
                    }

                }).success(function (res) {

                    console.log(res);
                    angular.copy(res, data);
                    //                    console.log(data);
                    //data=res;

                }).error(function (error) {
                    console.log(error);
                }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },
        //ดึงข้อมูลการใช้ยาของคนป่วยคนนี้ทั้งหมดมา
        //ดึงมาที่ละ 10 อัน โดย parameter range เป็นตัวกำหนด
        //เริ่มจาก 1 เช่น range=1 ให้เอาข้อมูลลำดับที่ 1-10 มา
        //โดยข้อมูลที่ส่งมาขอให้เรียงมาแล้ว จากวันล่าสุดไปวันเก่าสุด
        getAllDrugUseById: function (patientId, range) {
            var data = this.drugUse;
            var promise = $http({
                    method: 'POST',
                    url: '../backend/drugUseClass.php',
                    data: {
                        action: "getAllDrugUseById",
                        patientId: patientId,
                        range: range
                    }

                }).success(function (res) {

                    console.log(res);
                    if (res != "   No data") {
                        angular.copy(res, data);
                        console.log(data);
                    }

                }).error(function (error) {
                    console.log(error);
                }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;

        },
        //ถ้ามีแล้วให้updateเข้าไป ถ้ายังไม่มีให้เข้าระบบ
        //ระวัง อันนี้อาจจะมีปัญหานิดหน่อย เนื่องจากต้องเปลี่ยนแปลงหลายตาราง
        updateDrugUse: function (aDrugUse) {
            var promise = $http({
                method: 'POST',
                url: '../backend/drugUseClass.php',
                data: {
                    action: "updateDrugUse",
                    aDrugUse: aDrugUse
                }

            }).success(function (res) {

                console.log(res);
                //                    angular.copy(res, data);
                //                    console.log(data);

            }).error(function (error) {
                console.log(error);
            }), deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        }
    };
});
