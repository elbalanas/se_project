app.factory('pharmacistClass', function ($http,$q) {
    this.pharmacist = [
        {
            pharmacistId: '',
            ssid: '',
            firstName: '',
            lastName: '',
            address: '',
            telephone: ''
        }
    ]

    return {
        pharmacist: this.pharmacist,
        //เอาข้อมูลของเเภสัชคนนี้มา
        getAPharmacist: function (pharmacistId) {
            var data=this.pharmacist;
            var promise = $http({
					method : 'POST',
					url : '../backend/pharmacistClass.php',
					data: {action:"getAPharmacist",pharmacistId:pharmacistId}

				}).success(function(res){

					console.log(res);
                    angular.copy(res, data);
                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },
        //เอาข้อมูลของเภสัชทั้งหมดที่มีในระบบออกมา
        getPharmacistList:function(){
            var data=this.pharmacist;
            $http({
					method : 'POST',
					url : '../backend/pharmacistClass.php',
					data: {action:"getPharmacistList"}

				}).success(function(res){

					console.log(res);
                    angular.copy(res, data);
                    console.log(data);

				}).error(function(error){
					console.log(error);
				});
        },

        //อัพเดทข้อมูลถ้ามีเภสัชอยู่แล้ว ถ้ายังให้เพิ่มเข้าระบบ
        updatePharmacist:function(aPharmacist){
            var promise = $http({
					method : 'POST',
					url : '../backend/pharmacistClass.php',
					data: {action:"updatePharmacist",aPharmacist:aPharmacist}

				}).success(function(res){

					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },

        //ลบเภสัชคนนี้ออกจากระบบ
        deletePharmacistById:function(pharmacistId){
            var promise = $http({
					method : 'POST',
					url : '../backend/pharmacistClass.php',
					data: {action:"deletePharmacistById",pharmacistId:pharmacistId}

				}).success(function(res){

					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        }
    };
});
