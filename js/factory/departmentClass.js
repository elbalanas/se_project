app.factory('departmentClass', function ($http,$q) {
    
    //โครงสร้างถูกแก้ไขให้ง่ายขึ้นไม่ต้องเอา doctor มาเกี่ยวแล้ว
    //ใช้แยกกันเลยจะได้ดูเป็น class ชัดเจน
    this.department = [
        {
            departmentId: '',
            departmentName: ''
        }
    ]

    return {
        department: this.department,
        //แก้ไขจากเดิมรับข้อมูลหมอในแผนกมาด้วย
        //เป็นแค่รับรายชื่อแผนกทั้งหมดในระบบมาแทน
        getDepartmentList: function () {
            var data=this.department;
            $http({
					method : 'POST',
					url : '../backend/departmentClass.php',
					data: {action:"getDepartmentList"}

				}).success(function(res){

					console.log(res);
                    angular.copy(res, data);
                    console.log(data);

				}).error(function(error){
					console.log(error);
				});

        },
        //ถ้ามีอยู่แล้วให้อัพเดททับ แต่ถ้ายังไม่มีให้เพิม่เข้าระบบ
        updateDepartment:function(aDepartment){
            var promise = $http({
					method : 'POST',
					url : '../backend/departmentClass.php',
					data: {action:"updateDepartment",aDepartment:aDepartment}

				}).success(function(res){

					console.log(res);


				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },
        //ลบแผนกนนี้ออกจากระบบ
        //ระวังต้องคิดห้รอบคอบว่า ถ้าลบแล้วต้องลบอะไรต่ออีกไหม
        deleteDepartmentById:function(departmentNumber){
            var promise = $http({
					method : 'POST',
					url : '../backend/departmentClass.php',
					data: {action:"deleteDepartmentById",departmentId:departmentNumber}

				}).success(function(res){

					console.log(res);


				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        }
    };
});
