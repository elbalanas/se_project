app.factory('medicalHistoryClass', function ($http, $q) {
    this.history = [
//        {
//            medicalId: '',
//            diseaseId: '',
//            diseaseName: '',
//            doctorId: '',
//            doctorFirstName: '',
//            doctorLastName: '',
//            patientId: '',
//            doctorNote: '',
//            date: ''
//        }
    ]

    return {
        history: this.history,
        //แก้ไขชื่อเอา คำว่า today ออกแล้วเพิ่มparameter date เข้าไป
        //แบบนี้จะทำให้ทำงานได้ดีกว่า
        //ทำหน้าที่ เอาประวัติการรักษาของคนไข้คนนี้ ในวันนั้นๆออกมา
        getMedicalHistoryById: function (patientId, date) {
            console.log("get diagnose");
            console.log(patientId);
            console.log(date);
            var data = this.history;
            var promise = $http({
                method: 'POST',
                url: '../backend/medicalHistoryClass.php',
                data: {
                    action: "getMedicalHistoryById",
                    patientId: patientId,
                    date: date
                }

            }).success(function (res) {

                console.log(res);
                angular.copy(res, data);
                console.log(data);

            }).error(function (error) {
                console.log(error);
            }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;


        },
        //อันนี้จะดึงข้อมูลการรักษาทั้งหมดของคนไข้ออกมา
        //โดยจะเอาออกมาทีละ10อัน
        //โดยparameter range จะใช้บอกว่าจะเอาข้อมูลชุดที่เท่าไร
        //เริ่มจาก 1 เช่น range=1 หมายถึงเอา ตัวที่ 1-10
        //โดยhistoryที่เอาออกมา ขอให้เรียงจากวันล่าสุดไปหาวันเก่าสุด
        getAllMedicalHistoryById: function (patientId, range) {
            var data = this.history;
            var promise = $http({
                    method: 'POST',
                    url: '../backend/medicalHistoryClass.php',
                    data: {
                        action: "getAllMedicalHistoryById",
                        patientId: patientId,
                        range: range
                    }

                }).success(function (res) {

                    console.log(res + "2");
                    angular.copy(res, data);
                    console.log(data);

                }).error(function (error) {
                    console.log(error);
                }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },
        //ถ้ามีอยู่แล้วให้อัพเดททับ แต่ถ้ายังไม่มีให้พิ่มเข้าระบบ
        updateMedicalHistory: function (medicalHistory) {

            var promise=$http({
                method: 'POST',
                url: '../backend/medicalHistoryClass.php',
                data: {
                    action: "updateMedicalHistory",
                    medicalHistory: medicalHistory
                }

            }).success(function (res) {

                //console.log(res);
//                angular.copy(res, data);
//                console.log(data);

            }).error(function (error) {
                console.log(error);
            }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;

        }
    };
});
