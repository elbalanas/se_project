app.factory('appointmentClass', function ($http,$q) {
    this.appointment = [
//        {
//            appointmentId: '',
//            patientId: '',
//            patientFirstName: '',
//            patientLastName: '',
//            patientSsid: '',
//            cause: '',
//            staffId: '',
//            staffFirstName: '',
//            staffLastName: '',
//            status: '',
//            departmantNumber: '',
//            departmantName: '',
//            doctorId: '',
//            doctorFirstName: '',
//            doctorLastName: '',
//            type: '',
//            date: '',
//            period: ''
//        }
    ]

    return {
        appointment: this.appointment,
        //แก้ไขชื่อจาก getAllappointmentByDate ไม่รับ parameter แล้ว
        //ดึงข้อมูลการนัดหมายทั้งหมดในระบบของวันนี้ออกมา
        getAllTodayAppointment: function () {
            var data=this.appointment;
            $http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"getAllTodayAppointment"}

				}).success(function(res){

					if(res!="   No data"){
                        console.log(res);
                        angular.copy(res, data);
                        console.log(data);
                    }
                    else{
                        var temp = {}
                        angular.copy(temp,data)
                        console.log(data)
                    }

				}).error(function(error){
					console.log(error);
				})
        },

        //ดึงข้อมูลการนัดหมายของคนไข้คนนี้ทั้งหมดออกมา
        getPatientAppointById: function (patientId) {
            console.log(patientId);
            var data=this.appointment;
            $http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"getPatientAppointById",patientId:patientId}

				}).success(function(res){

					if(res!="   No data"){
                        console.log(res);
                        angular.copy(res, data);
                        console.log(data);
                    }
                    else{
                        var temp = {}
                        angular.copy(temp,data)
                        console.log(data)
                    }

				}).error(function(error){
					console.log(error);
				});
        },

        //ใหม่ รับเลขแผนกไปหา หมอ และ วันเวลานัดหมายที่ดีที่สุดออก
        //โครงสร้างที่ส่งกลับมาเปลี่ยนได้ตามเหมาะสม แต่ถ้าเปลี่ยนบอกด้วย
        getBestAppointmentByDepartment: function (departmentNumber) {
            var data=this.appointment;
            var promise=$http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"getBestAppointmentByDepartment",departmentNumber:departmentNumber}

				}).success(function(res){

					console.log(res);
                    //angular.copy(res, data);
                    //console.log(data);


				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
                promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
                    //console.log(deferObject.promise)
            return deferObject.promise;

        },

        //ใหม่ รับเลขแผนกและชื่อหมอไป หาวันเวลานัดหมายที่ดีที่สุดมา
        //โครงสร้างที่ส่งกลับมาเปลี่ยนได้ตามเหมาะสม แต่ถ้าเปลี่ยนบอกด้วย
        getBestAppointmentByDoctor: function (departmentNumber, doctorId) {
            var data=this.appointment;
            console.log(departmentNumber)
            var promise=$http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"getBestAppointmentByDoctor",departmentNumber:departmentNumber,doctorId:doctorId}

				}).success(function(res){

					console.log(res);
                    //angular.copy(res, data);
                    //console.log(data);

				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();


                promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
            return deferObject.promise;
        },

        //ส่งเลขนัดหมายมา เอาไปหาและลบนัดหมายนี้ออกจากระบบ
        //ระวังข้างในจริงๆ อาจต้องไปแก้หลายที่
        //เปลี่ยนparameter ที่รับเป็นแค่เลขนัดหมายพอ
        deleteAppointmentById: function (appointmentId) {
            var promise = $http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"deleteAppointmentById",appointmentId:appointmentId}

				}).success(function(res){

					console.log("success");


				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
                promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
                    //console.log(deferObject.promise)
            return deferObject.promise;
        },

        //จะส่งนัดหมายมา ให้สังเกตุดูถ้านัดหมายมี AppointmentIdแล้ว
        //ให้เอานัดหมายนี้ไปแก้ไขนัดหมายที่มีIdตรงกัน แต่ถ้ายังให้add
        //นัดหมายนี้เข้าไป
        updateAppointment: function (aAppointment) {
            console.log(aAppointment);
            var promise = $http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"updateAppointment",aAppointment:aAppointment}

				}).success(function(res){

					console.log("success"+res);


				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
                promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
                    //console.log(deferObject.promise)
            return deferObject.promise;
        },

        //ส่งนัดหมายทั้งหมดของหมอคนนี้ในวันนี้มา
        //แก้ไขเพิ่มเติม parameter date เข้าไป
        //แบบนี้จะช่วยให้มันทำงานได้หลากหลายขึ้น
        getDoctorAppointmentById: function (doctorId, date) {
            var data=this.appointment;
            $http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"getDoctorAppointmentById",doctorId:doctorId,date:date}

				}).success(function(res){

					if(res!="   No data"){
                        console.log(res);
                        angular.copy(res, data);
                        console.log(data);
                    }
                    else{
                        var temp = {}
                        angular.copy(temp,data)
                        console.log(data)
                    }
				}).error(function(error){
					console.log(error);
				});


        },

        //ส่งparameterเข้าไป เอาไปหาจำนวนนัดหมายที่มีในวันนั้นๆ ของหมอคนนี้ในเดือนนั้น
        getAvailableAppointment: function (departmentId, doctorId, month, year) {
            var promise=$http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"getAvailableAppointment",departmentId:departmentId, doctorId:doctorId, month:month, year:year}

				}).success(function(res){

					console.log(res);
                   // angular.copy(res, data);
                    //console.log(data);
                //return true;

				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
                promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
                    //console.log(deferObject.promise)
            return deferObject.promise;
            //return
                //ส่งกลับมาตามที่คุยกันในห้อง
                //แต่อยากได้แบบนี้มากกว่า [{จำนวนนัดหมาย,สถานะ},{จำนวนนัดหมาย,สถานะ}]
                //สถานะไว้บอกว่ามันเป็นวันที่ต้องทำงานไหม
                //เนื่องจากอาจจะมีวันที่ไม่มีคนนัดเลยแต่ว่าหมอต้องมาทำงาน
                //ถ้าสงสัยถามได้
        },
        addAvailableDate:function(doctorId, date, period){
            var promise = $http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"addAvailableDate",doctorId:doctorId,date:date,period:period}

				}).success(function(res){

					console.log(res);


				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
                promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
                    //console.log(deferObject.promise)
            return deferObject.promise;
        },
        deleteAvailableDate:function(doctorId, date, period){
            var promise = $http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"deleteAvailableDate",doctorId:doctorId,date:date,period:period}

				}).success(function(res){

					console.log(res);


				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
                promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
                    //console.log(deferObject.promise)
            return deferObject.promise;
        },
        addUnavailableDate:function(doctorId, date, period){
            var promise = $http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"addUnavailableDate",doctorId:doctorId,date:date,period:period}

				}).success(function(res){

					console.log(res);


				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
                promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
                    //console.log(deferObject.promise)
            return deferObject.promise;
        },
        deleteUnavailableDate:function(doctorId, date, period){
            var promise = $http({
					method : 'POST',
					url : '../backend/appointmentClass.php',
					data: {action:"deleteUnavailableDate",doctorId:doctorId,date:date,period:period}

				}).success(function(res){

					console.log(res);


				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
                promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
                    //console.log(deferObject.promise)
            return deferObject.promise;
        }
    };
});
