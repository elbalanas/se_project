app.factory('doctorClass', function ($http, $q) {
    this.doctor = [
        {
            doctorId: '',
            ssid: '',
            firstName: '',
            lastName: '',
            departmentNumber: '',
            address: '',
            telephone: '',
            task: []
        }
    ]

    return {
        doctor: this.doctor,
        //ส่งข้อมูลของหมอคนนี้ออกมา
        getADoctor: function (doctorId) {
            var data = this.doctor;
            var promise = $http({
                method: 'POST',
                url: '../backend/doctorClass.php',
                data: {
                    action: "getADoctor",
                    doctorId: doctorId
                }

            }).success(function (res) {

                console.log(res);
                angular.copy(res, data);
                console.log(data);

            }).error(function (error) {
                console.log(error);
            }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;

        },
        //เอารายชื่อหมอทั้งหมดในระบบออกมา
        getDoctorList: function () {
            var data = this.doctor;
            $http({
                method: 'POST',
                url: '../backend/doctorClass.php',
                data: {
                    action: "getDoctorList"
                }

            }).success(function (res) {

                console.log(res);
                angular.copy(res, data);
                console.log(data);

            }).error(function (error) {
                console.log(error);
            });


        },

        //รับเลขแผนกไป ไปเอาข้อมูลของหมอที่อยู่ในแผนกนั้นทั่งหมดออกมา
        getDoctorByDepartment: function (departmentNumber) {
            var data = this.doctor;
            var promise = $http({
                    method: 'POST',
                    url: '../backend/doctorClass.php',
                    data: {
                        action: "getDoctorByDepartment",
                        departmentNumber: departmentNumber
                    }

                }).success(function (res) {

                    console.log(res);
                    angular.copy(res, data);
                    console.log(data);

                }).error(function (error) {
                    console.log(error);
                }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;

        },

        //รับข้อมูลมา เอาไปแก้ไขข้อมูลในระบบ ถ้ายังไม่มีให้ใส่เข้าไป
        //ถ้ามีแล้วให้เอาอันนี้ไปupdateอันเก่า
        updateDoctor: function (aDoctor) {
            //var data=this.doctor;
            var promise = $http({
                method: 'POST',
                url: '../backend/doctorClass.php',
                data: {
                    action: "updateDoctor",
                    aDoctor: aDoctor
                }

            }).success(function (res) {

                console.log(res);
                //angular.copy(res, data);
                //console.log(data);

            }).error(function (error) {
                console.log(error);
            }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },

        //เอาเลขไอดีไปลบหมอไอดีออกจากระบบ
        deleteDoctorById: function (doctorId) {
            var promise = $http({
                method: 'POST',
                url: '../backend/doctorClass.php',
                data: {
                    action: "deleteDoctorById",
                    doctorId: doctorId
                }

            }).success(function (res) {

                console.log(res);
                //angular.copy(res, data);
                //console.log(data);

            }).error(function (error) {
                console.log(error);
            }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;

        }
    };
});
