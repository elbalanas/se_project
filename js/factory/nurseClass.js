app.factory('nurseClass', function ($http,$q) {

    this.nurse = [
        {
            nurseId: '',
            ssid:'',
            firstName:'',
            lastName:'',
            address:'',
            telephone:''
        }
    ]

    return {
        nurse: this.nurse,
        //เอาข้อมูลของนางพยาบาลคนนี้ออกมา
        getANurse: function (nurseId) {
            var data=this.nurse;
            var promise = $http({
					method : 'POST',
					url : '../backend/nurseClass.php',
					data: {action:"getANurse",nurseId:nurseId}

				}).success(function(res){

					console.log(res);
                    if(res!="   No data"){
                        angular.copy(res, data);

                        //data=res;
                        console.log(data);
                    }

				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;

        },
        //เอาข้อมูลของนางพยาบาลทั้งหมดออกมา
        getNurseList:function(){
            var data=this.nurse;
            $http({
					method : 'POST',
					url : '../backend/nurseClass.php',
					data: {action:"getNurseList"}

				}).success(function(res){

					console.log(res);
                    angular.copy(res, data);
                    console.log(data);
                return true;

				}).error(function(error){
					console.log(error);
				});
        },
        //เอาข้อมูลไปถ้ามีอยู่แล้วให้อัพเดทของนายพยาบาลคนนี้
        //แต่ถ้ายังไม่มีให้ใส่เข้าระบบ
        updateNurse:function(aNurse){
            var promise = $http({
					method : 'POST',
					url : '../backend/nurseClass.php',
					data: {action:"updateNurse",aNurse:aNurse}

				}).success(function(res){

					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);


				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },
        //ลบนางพยาบาลคนนีี้ออกจากระบบ
        deleteNurseById:function(nurseId){
            var promise = $http({
					method : 'POST',
					url : '../backend/nurseClass.php',
					data: {action:"deleteNurseById",nurseId:nurseId}

				}).success(function(res){

					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        }
    };
});
