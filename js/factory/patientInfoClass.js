app.factory('patientInfoClass', function ($http,$q) {


//    this.info = [
//        {
//            infoId: '',
//            nurseId: '',
//            nurseFirstName: '',
//            nurseLastName:'',
//            patientId: '',
//            patientFirstName: '',
//            patientLastName:'',
//            weight: '',
//            height: '',
//            temperature: '',
//            heartRate: '',
//            systotic: '',
//            diastotic: '',
//            date: ''
//        }
//    ]
    this.info={};


    return {
        info: this.info,
        //แก้ไขให้รับวันและเปลี่ยนชื่อเพราะจะได้ใช้ได้มากกว่าและเข้าใจง่ายกว่า
        //date_format = YYYY-MM-DD
        //เอาข้อมูลผลการวัดในวันนั้นของคนป่วยคนนี้มา
        getPatientInfoById: function (patientId,date) {
            console.log(date);
            var data=this.info;
            var promise = $http({
					method : 'POST',
					url : '../backend/patientInfoClass.php',
					data: {action:"getPatientInfoById",patientId:patientId,date:date}

				}).success(function(res){


					console.log(res);
                    if(res!="   No data"){
                        angular.copy(res,data);
                        console.log(data);
                    }
                    else{
                        var temp = {}
                        angular.copy(temp,data)
                        console.log(data)
                    }

				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },

        //ถ้ามีอยู่แล้วให้update แต่ถ้ายังไม่มีให้ใส่เข้าไป
        updatePatientInfo: function (patientInfoForm) {
            $http({
					method : 'POST',
					url : '../backend/patientInfoClass.php',
					data: {action:"updatePatientInfo",patientInfoForm:patientInfoForm}

				}).success(function(res){
                    console.log(patientInfoForm);
					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);

				}).error(function(error){
					console.log(error);
				});
        }
    };
});
