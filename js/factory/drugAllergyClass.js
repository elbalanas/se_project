app.factory('drugAllergyClass', function ($http, $q) {
    this.allergy = [
        {
            drugAllergyId: '',
            drugId: '',
            patientId: '',
            drugName: ''
        }
    ]
    return {
        allergy: this.allergy,

        //แก้ไขชื่อมาจากอะไรไมม่รู้มาเป็นอันนี้
        //รับidคนไข้ไปดีงข้อมูลยาที่คนไข้แพ้ทั้งหมดมา
        getPatientAllergyById: function (patientId) {
            var data = this.allergy;
            $http({
                method: 'POST',
                url: '../backend/drugAllergyClass.php',
                data: {
                    action: "getPatientAllergyById",
                    patientId: patientId
                }

            }).success(function (res) {

                console.log(res);
                angular.copy(res, data);
                console.log(data);

            }).error(function (error) {
                console.log(error);
            });
        },

        //รับIdคนป่วยกับ รายชื่อยาที่แพ้อันใหม่ไป
        //เอารายชื่อยาที่ให้ใส่ลงdatabase ที่ละอัน
        addAllergyById: function (patientId, drugList) {
            //drugList ที่ให้มาจะเป็น array ของ object ของยาตาม
            //structure ข้างบน
            var promise = $http({
                    method: 'POST',
                    url: '../backend/drugAllergyClass.php',
                    data: {
                        action: "addAllergyById",
                        patientId: patientId,
                        drugList: drugList
                    }

                }).success(function (res) {

                    console.log(res);
                    //angular.copy(res, data);
                    //console.log(data);

                }).error(function (error) {
                    console.log(error);
                }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        }
    };
});
