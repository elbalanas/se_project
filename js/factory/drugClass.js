app.factory('drugClass', function ($http,$q) {
    this.drug = [
        {
            drugId: '',
            drugName: ''
        }
    ]
    return {
        drug: this.drug,
        getDrugList: function () {
            var data=this.drug;
            $http({
					method : 'POST',
					url : '../backend/drugClass.php',
					data: {action:"getDrugList"}

				}).success(function(res){

					console.log(res);
                    angular.copy(res, data);
                    console.log(data);

				}).error(function(error){
					console.log(error);
				});
        },
        //ใช้อัพเดทถ้ามีไอดีนี้แล้ว ถ้ายังไม่มีให้ใส่เข้าตาราง
        updateDrug:function(aDrug){
          //อันนี้ยังไม่ชัวร์ เนื่องจากยังไม่ชัวร์เรื่อง sturctureของclass นี้
            //var data=this.staff;
            var promise = $http({
					method : 'POST',
					url : '../backend/drugClass.php',
					data: {action:"updateDrug",aDrug:aDrug}

				}).success(function(res){

					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },
        //ลบยารหัสนี้ออกจากระบบ
        deleteDrugById:function(drugId){
            var promise = $http({
					method : 'POST',
					url : '../backend/drugClass.php',
					data: {action:"deleteDrugById",drugId:drugId}

				}).success(function(res){

					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        }
    };
});
