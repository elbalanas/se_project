app.factory('drugOrderClass', function ($http, $q) {

    this.order = {
        prescriptionId: '',
        patientId: '',
        doctorId: '',
        doctorFirstName: '',
        doctorLastName: '',
        medicalId: '',
        date: '',
        doctorNote: '',
        drugList: []
    };



    return {
        order: this.order,

        //แก้ไขให้รับ parameter date
        //เพื่อให้สามารถทำงาานได้หลากหลายมากขึ้น
        //เอาข้อมูลยาที่หมอสั่งให้คนไข้คนนี้ในวันนี้ออกมา
        getDrugOrderById: function (patientId, date) {
            var data = this.order;
            var promise = $http({
                    method: 'POST',
                    url: '../backend/drugOrderClass.php',
                    data: {
                        action: "getDrugOrderById",
                        patientId: patientId,
                        date: date
                    }

                }).success(function (res) {
                
                    console.log(res);
                    angular.copy(res, data);
                    //                    console.log(data);
                    //data=res;
                    console.log(data);
           


                }).error(function (error) {
                    console.log(error);
                }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },
        //เอาorderนี้ไปupdate อันเดิมถ้ามี ถ้าไม่มีให้ใส่orderนี้เข้าไปในระบบ
        //ระวังอันนี้ต้องไปupdate หลาย table
        updateDrugOrder: function (drugOrder) {
            $http({
                method: 'POST',
                url: '../backend/drugOrderClass.php',
                data: {
                    action: "updateDrugOrder",
                    drugOrder: drugOrder
                }

            }).success(function (res) {

                console.log(res);
                //angular.copy(res, data);
                console.log("update");

            }).error(function (error) {
                console.log(error);
            });

        }
    };
});
