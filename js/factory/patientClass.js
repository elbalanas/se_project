app.factory('patientClass', function ($http,$q) {
    this.patient = {
        patientId: '',
        ssid: '',
        fisrtName: '',
        lastName: '',
        sex: '',
        address: '',
        telephone: '',
        email: ''
    }

    return {
        patient: this.patient,
        //เอาข้อมูลของคนไข้คนนี้ออกมา
        getAPatient: function (patientId) {
            var data=this.patient;
            var promise = $http({
					method : 'POST',
					url : '../backend/patientClass.php',
					data: {action:"getAPatient",patientId:patientId}

				}).success(function(res){

					console.log(res);
                    angular.copy(res, data);
                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
            promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
            return deferObject.promise;
        },
        //ถ้ามีคนไข้อยู่แล้วให้อัพเทด ถ้ายังไม่มีให้เพิ่มเข้าไป
        addPatient:function(aPatient){
            //var data=this.patient;
            var  promise = $http({
					method : 'POST',
					url : 'backend/patientClass.php',
					data: {action:"updatePatient",aPatient:aPatient}

				}).success(function(res){

					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
            promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
            return deferObject.promise;
        },
        updatePatient:function(aPatient){
            //var data=this.patient;
            var  promise = $http({
					method : 'POST',
					url : '../backend/patientClass.php',
					data: {action:"updatePatient",aPatient:aPatient}

				}).success(function(res){

					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
            promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
            return deferObject.promise;
        },
        getPatientBySsid:function(ssid){
            var data=this.patient;
            var promise=$http({
					method : 'POST',
					url : '../backend/patientClass.php',
					data: {action:"getPatientBySsid",ssid:ssid}

				}).success(function(res){

					if (res != "   No data") {
                        angular.copy(res, data);
                        console.log(data);
                    }
                    console.log(data);


				}).error(function(error){
					console.log(error);
				}),deferObject =  deferObject || $q.defer();
            promise.then(
                  // OnSuccess function
                  function(answer){
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                  },
                  // OnFailure function
                  function(reason){
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                  });
            return deferObject.promise;


        },
        //ลบคนไข้คนนี้ออกจากระบบ
        deletePatientById:function(patientId){
            $http({
					method : 'POST',
					url : '../backend/patientClass.php',
					data: {action:"deletePatientById",patientId:patientId}

				}).success(function(res){

					console.log(res);


				}).error(function(error){
					console.log(error);
				});
        }
    }
});


