app.factory('diseaseClass', function ($http,$q) {
    this.disease = [{
        diseaseId: '',
        diseaseName: ''
    }]
    return {
        disease: this.disease,

        //เอารายชื่อโรคทั้งหมดในระบบส่งออกมา
        getDiseaseList: function () {
            var data=this.disease;
            $http({
					method : 'POST',
					url : '../backend/diseaseClass.php',
					data: {action:"getDiseaseList"}

				}).success(function(res){

					console.log(res);
                    angular.copy(res, data);
                    console.log(data);

				}).error(function(error){
					console.log(error);
				});


        },
        //ถ้ามีอยู่ในระบบให้เอาโรคนี้ไปupdate ถ้ายังไม่มีให้ใส่ข้อมูลลงไป
        updateDisease:function(aDisease){
            //var data=this.disease;
            var promise = $http({
					method : 'POST',
					url : '../backend/diseaseClass.php',
					data: {action:"updateDisease",aDisease:aDisease}

				}).success(function(res){

					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },

        //ระบบโรคนี้ออกจากระบบ
        deleteDiseaseById:function(diseaseId){
            var promise = $http({
					method : 'POST',
					url : '../backend/diseaseClass.php',
					data: {action:"deleteDiseaseById",diseaseId:diseaseId}

				}).success(function(res){

					console.log(res);
//                    angular.copy(res, data);
//                    console.log(data);

				}).error(function(error){
					console.log(error);
				}),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        }

    };
});
