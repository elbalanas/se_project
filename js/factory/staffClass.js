app.factory('staffClass', function ($http, $q) {
    this.staff = [
        {
            staffId: '',
            ssid: '',
            firstName: '',
            lastName: '',
            address: '',
            telephone: '',
            adminFlag: ''
        }
    ]
    return {
        staff: this.staff,
        //เอาข้อมูลของเจ้าหน้าที่คนนี้ออกมา
        getAStaff: function (staffId) {
            var data = this.staff;
            var promise = $http({
                    method: 'POST',
                    url: '../backend/staffClass.php',
                    data: {
                        action: "getAStaff",
                        staffId: staffId
                    }

                }).success(function (res) {
                    //console.log(staffId);
                    //console.log(res);
                    if (res != "   No data") {
                        angular.copy(res, data);
                        console.log(data);
                    }

                }).error(function (error) {
                    console.log(error);
                }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },
        //เอาข้อมูลของเข้าหน้าที่ทั้งหมดในระบบออกมา
        getStaffList: function () {
            var data = this.staff;
            var promise = $http({
                method: 'POST',
                url: '../backend/staffClass.php',
                data: {
                    action: "getStaffList"
                }

            }).success(function (res) {

                console.log(res);
                angular.copy(res, data);
                console.log(data);

            }).error(function (error) {
                console.log(error);
            }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;


        },
        changeAuthor: function (ssid, staffId, adminFlag) {
            var promise = $http({
                method: 'POST',
                url: '../backend/staffClass.php',
                data: {
                    action: "changeAuthor",
                    ssid: ssid,
                    staffId: staffId,
                    adminFlag: adminFlag
                }

            }).success(function (res) {
                console.log(res);
            }).error(function(error){
                console.log(error)
            }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },
        //ถ้ามีอยู่แล้วให้เอาข้อมูลนี้ไปupdate ให้ ถ้ายังไม่่มีให้ใส่ข้อมูลลงไปในระบบ
        updateStaff: function (aStaff) {
            var promise = $http({
                    method: 'POST',
                    url: '../backend/staffClass.php',
                    data: {
                        action: "updateStaff",
                        aStaff: aStaff
                    }

                }).success(function (res) {

                    console.log(res);
                    //angular.copy(res, data);
                    //console.log(data);

                }).error(function (error) {
                    console.log(error);
                }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;
        },
        //ลบเข้าหน้าที่คนนี้ออกจากระบบ
        deleteStaffById: function (staffId) {

            var promise = $http({
                    method: 'POST',
                    url: '../backend/staffClass.php',
                    data: {
                        action: "deleteStaffById",
                        staffId: staffId
                    }

                }).success(function (res) {

                    console.log(res);
                    //angular.copy(res, data);
                    //console.log(data);

                }).error(function (error) {
                    console.log(error);
                }),
                deferObject = deferObject || $q.defer();
            promise.then(
                // OnSuccess function
                function (answer) {
                    // This code will only run if we have a successful promise.
                    deferObject.resolve(answer);
                },
                // OnFailure function
                function (reason) {
                    // This code will only run if we have a failed promise.
                    deferObject.reject(reason);
                });
            return deferObject.promise;


        }
    };
});
