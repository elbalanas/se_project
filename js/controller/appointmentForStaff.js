app.controller('staffCtrl', function ($modal, moment, patientClass, appointmentClass, departmentClass, doctorClass, staffClass, $rootScope, $scope, sessionClass) {

    var vm = this;

    vm.showChangeDate = false;

    //toggle to show or off calendar
    vm.toggleChangeDate = function () {
        vm.showChangeDate = !vm.showChangeDate;
    }

    //close modals after confirm delete
    vm.deleteAssignment = function () {
        $rootScope.Ui.turnOff("modals");
    }

    vm.aPatient = patientClass.patient;
    vm.aStaff = staffClass.staff;
    vm.allAppointment = appointmentClass.appointment;
    vm.allDepartment = departmentClass.department;
    vm.allDoctor = doctorClass.doctor;
    vm.session = sessionClass.session;
    var aDay = new Date();

    vm.generatePeriod = function (aDay) {
        var hour = aDay.getHours();
        var min = aDay.getMinutes();
        min = (hour * 60) + min;
        if (min > 720) {
            return '1'
        }
        if (min <= 720) {
            return '0'
        }
    }
    
    vm.tranSex = function (sex) {
        if (sex == '0') return 'Male'
        if (sex == '1') return 'Female'
    }
    
    vm.timeByPeriod = function (period, set) {
        if (period == '0') {
            if (set == 0) return '8:00'
            else return '11:00'
        }
        if (period == '1') {
            if (set == 0) return '13:00'
            else return '16:00'
        } else return
    }

    //initialize this page
    vm.init = function () {
        vm.today = aDay.getFullYear() + '-' + (aDay.getMonth() + 1) + '-' + aDay.getDate();
        vm.period = vm.generatePeriod(aDay);

        staffClass.getAStaff(vm.session.id).then(function () {
            vm.initProfile();
        })
        departmentClass.getDepartmentList();
        vm.choosenDepartment = vm.allDepartment[0].departmentNumber;
        vm.pState = {}
        vm.searchArg = '';
        vm.showContain = false;
        vm.walkInMode = false;
    }

    sessionClass.getSession().then(function () {
        vm.init();
    });

    //set new Appointment state (new)
    vm.setNewAppointment = function () {
        vm.editing = 0;
        vm.showContain = false;

        vm.findPatient();

        vm.pState = {
            appointmentId: '',
            patientId: '',
            patientFirstName: '',
            patientLastName: '',
            patientSsid: '',
            cause: '',
            staffId: '',
            staffFirstName: '',
            staffLastName: '',
            status: 'false',
            departmentNumber: '',
            departmentName: '',
            doctorId: '',
            doctorFirstName: '',
            doctorLastName: '',
            type: '1',
            date: '',
            period: ''
        }
        
        vm.choosenDepartment = vm.allDepartment[0].departmentNumber;
        doctorClass.getDoctorByDepartment(vm.choosenDepartment).then(function () {
            vm.choosenDoctor = vm.allDoctor[0].doctorId;
            vm.pickFromDepartment(vm.choosenDepartment);
        })
        vm.showChangeDate = false;
        vm.currentMonth = aDay.getMonth() + 1
        vm.currentYear = aDay.getFullYear();
        vm.walkInMode = false;
    }

    //find and get patient appointment information list by his SSID
    vm.findPatient = function () {
        if (vm.searchArg == '') {
            return
        }
        if (vm.searchArg === 'undefined' || vm.searchArg.length != 13 || isNaN(vm.searchArg) || vm.searchArg == null) {
            sweetAlert("Sorry!!", "SSID is Incorrect", "error")
            return
        }
        
        patientClass.getPatientBySsid(vm.searchArg).then(function () {
                if (vm.aPatient.ssid != vm.searchArg) {
                    sweetAlert("Sorry!!", "SSID is Incorrect", "error")
                    vm.showContain = false;
                } else {
                    appointmentClass.getPatientAppointById(patientClass.patient['patientId']);
                    vm.showContain = true;
                }
            },function () {
                sweetAlert("Sorry!!", "SSID is Incorrect", "error")
            }
        );
    }

    //get doctor list for department according to departmentNumber and pick best available appointment for this department
    vm.changeDepartment = function (dNumber) {
        doctorClass.getDoctorByDepartment(dNumber).then(function () {
            vm.choosenDoctor = vm.allDoctor[0].doctorId;
            if (vm.editing == 0) {
                vm.pickFromDepartment(vm.choosenDepartment)
            }
        })


    }

    //pick best available appointment for this department (dont care doctor)
    vm.pickFromDepartment = function (choosenDepartment) {
        vm.editing = 0;
        
        if (vm.walkInMode) {
            vm.findAvaiableSeat();
            return
        }
        
        appointmentClass.getBestAppointmentByDepartment(choosenDepartment).then(function (res) {
            vm.choosenDoctor = res.data.doctorId;
            vm.pState.date = res.data.date;
            vm.pState.period = res.data.period
            
            appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth + "", vm.currentYear + '').then(function (res2) {
                vm.generateCalendar(res2.data, vm.currentMonth - 1, vm.currentYear);
            });
        });
    }
    
    //pick best available appointment for this department and this doctor
    vm.pickFromDoctor = function (choosenDepartment, choosenDoctor) {
        if (vm.walkInMode) {
            vm.findAvaiableSeat();
            return
        }
        
        appointmentClass.getBestAppointmentByDoctor(choosenDepartment, choosenDoctor).then(function (res) {
            vm.pState.date = res.data.date;
            vm.pState.period = res.data.period

            appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth + "",
                vm.currentYear + "").then(function (res) {
                vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
            });
        });

    }
    
    //change mode between walkin and reserve
    //walkin will have more available seat and can select only today
    vm.changeMode = function () {
        if (vm.walkInMode) {
            vm.pState.date = vm.today;
            vm.pState.period = vm.period
            
            vm.choosenDepartment = vm.allDepartment[0].departmentNumber;
            doctorClass.getDoctorByDepartment(vm.choosenDepartment).then(function () {
                vm.choosenDoctor = vm.allDoctor[0].doctorId;
                vm.findAvaiableSeat();
            })
        } else {
            vm.pickFromDepartment(vm.choosenDepartment)
        }
    }

    //find availableSeat for current time 0-15 -> 0 = no more seat  , 15 = not appointment today
    vm.findAvaiableSeat = function () {
        var temp = '';
        var date = aDay.getDate();
        var position = ((date - 1) * 2);
        
        appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth, vm.currentYear).then(function (res) {
            var temp = res.data;
            
            if (vm.period == '0' && temp[position].status == '1') {
                vm.availableSeat = 15 - temp[position].numberOfAppointment;
            }
            if (vm.period == '0' && temp[position].status == '0') {
                vm.availableSeat = 0;
            }
            if (vm.period == '1' && temp[position + 1].status == '1') {
                vm.availableSeat = 15 - temp[position + 1].numberOfAppointment;
            }
            if (vm.period == '1' && temp[position + 1].status == '0') {
                vm.availableSeat = 0
            }
        });
    }
    
    //set appointment state (edit)
    vm.setState = function (appointment) {
        var temp = appointment
        angular.copy(appointment, vm.pState);
        
        vm.currentMonth = aDay.getMonth() + 1;
        vm.currentYear = aDay.getFullYear();
        vm.editing = true;
        vm.showChangeDate = false;
        
        vm.choosenDepartment = vm.pState.departmentNumber;
        
        doctorClass.getDoctorByDepartment(vm.choosenDepartment);
        
        vm.changeDepartment(vm.choosenDepartment);
        vm.choosenDoctor = vm.pState.doctorId;
        vm.toThisMonth();
       
    }

    vm.clearState = function (state) {
        // just empty - -
    }
    
    //save appointment state to DB (edit,new)
    vm.saveState = function () {
        vm.pState.patientFirstName = vm.aPatient.firstName;
        vm.pState.patientLastName = vm.aPatient.lastName;
        vm.pState.patientId = vm.aPatient.patientId;
        vm.pState.patientSsid = vm.aPatient.ssid;
        vm.pState.staffFirstName = vm.aStaff.firstName;
        vm.pState.staffLastName = vm.aStaff.lastName;
        vm.pState.staffId = vm.aStaff.staffId;
        vm.pState.type = '2';
        vm.pState.departmentNumber = vm.choosenDepartment;
        
        for (department of vm.allDepartment) {
            if (department.departmentNumber == vm.choosenDepartment) {
                vm.pState.departmentName = department.departmentName;
            }
        }
        
        vm.pState.doctorId = vm.choosenDoctor;
        for (doctor of vm.allDoctor) {
            if (doctor.doctorId == vm.choosenDoctor) {
                vm.pState.doctorFirstName = doctor.firstName;
                vm.pState.doctorLastName = doctor.lastName;
            }
        }
        
        sweetAlert("Well Done!!", "Information have been Updated", "success");
        appointmentClass.updateAppointment(vm.pState).then(function () {
            vm.setNewAppointment();
        })

    }

    //delete this choosen appointment from DB
    vm.delState = function () {
        appointmentClass.deleteAppointmentById(vm.pState.appointmentId).then(function () {
            sweetAlert("Well Done!!", "Appointment have been Deleted", "success");
            appointmentClass.getPatientAppointById(patientClass.patient['patientId']);
        })
    }

    //connect to today button in calendar -> set and generate calendar to current month and year
    vm.toThisMonth = function () {
        vm.currentMonth = aDay.getMonth() + 1;
        vm.currentYear = aDay.getFullYear();

        appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth + "",vm.currentYear + '').then(function (res) {
            vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
        });
    }

    //connect to previous button in calendar -> set and generate calendar to 1 previous month
    vm.toPreviousMonth = function () {
        if (vm.currentMonth == 1) {
            vm.currentMonth = 12;
            vm.currentYear = vm.currentYear - 1;
        } else vm.currentMonth = vm.currentMonth - 1;
        
        appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth + "",vm.currentYear + '').then(function (res) {
            vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
        });
    }

    //connect to next button in calendar -> set and generate calendar to 1 next month
    vm.toNextMonth = function () {
        if (vm.currentMonth == 12) {
            vm.currentMonth = 1;
            vm.currentYear = vm.currentYear + 1;
        } else vm.currentMonth = vm.currentMonth + 1;

        appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth + "",vm.currentYear + '').then(function (res) {
            vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
        });
    }

    //geneeate data for calendar according to month and year
    //appointment data come from getAvailableAppointment  by appointmentClass
    vm.generateCalendar = function (appointment, month, year) {
        var cal = true;
        var begin = 0;
        
        if (aDay.getFullYear() > year) cal = false;
        else if (aDay.getFullYear() == year) {
            if (aDay.getMonth() > month) cal = false;
            else if (aDay.getMonth() == month) {
                begin = aDay.getDate() * 2;
                console.log(begin)
                cal = true;
            } else cal = true;
        } else cal = true;
        
        vm.events = [];
        if (cal) {
            for (var i = begin; i < appointment.length; i = i + 2) {
                if (appointment[i].status == 0 && appointment[i + 1].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                    }
                } else if (appointment[i].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingAfternoon: 10 - appointment[i + 1].numberOfAppointment
                    }
                } else if (appointment[i + 1].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingMorning: 10 - appointment[i].numberOfAppointment
                    }
                } else {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingMorning: 10 - appointment[i].numberOfAppointment,
                        remainingAfternoon: 10 - appointment[i + 1].numberOfAppointment
                    }
                }
                vm.events.push(ele);
            }
        }
    }
    
    //initialize current User profile page
    vm.initProfile = function () {
        vm.profile = {
            staffId: vm.aStaff.staffId,
            ssid: vm.aStaff.ssid,
            firstName: vm.aStaff.firstName,
            lastName: vm.aStaff.lastName,
            telephone: vm.aStaff.telephone,
            address: vm.aStaff.address,
            adminFlag: 0
        }
    }

    //check and save current profile page
    vm.saveProfile = function () {
        var valid = true;
        var sId = vm.profile.staffId
        
        if (vm.profile.firstName == '' || vm.profile.firstName === 'undefined' || vm.profile.firstName == null || vm.profile.firstName.length > 200) {
            valid = false;
            sweetAlert("Sorry!!", "First Name is Incorrect!", "error");
            return
        }
        if (vm.profile.lastName == '' || vm.profile.lastName === 'undefined' || vm.profile.lastName == null || vm.profile.lastName.length > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Last Name is Incorrect!", "error");
            return
        }
        if (vm.profile.telephone == '' || vm.profile.telephone === 'undefined' || isNaN(vm.profile.telephone) || vm.profile.telephone == null || vm.profile.telephone.length > 10) {
            valid = false;
            sweetAlert("Sorry!!", "Telephone is Incorrect!", "error");
            return
        }
        if (vm.profile.address == '' || vm.profile.address === 'undefined' || vm.profile.address == null || vm.profile.address > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Address is Incorrect!", "error");
            return
        }
        
        if (valid) {
            staffClass.updateStaff(vm.profile).then(function () {
                staffClass.getAStaff(sId).then(function () {
                    vm.initProfile();
                })
            })
            sweetAlert("Well Done!!", "Information have been Updated", "success");
        }
    }

    //check and save current user new password
    vm.changePassword = function () {
        var valid = true;
        
        if (vm.newPassword == '' || vm.newPassword === 'undefined' || vm.newPassword == null || vm.newPassword.length > 13) {
            valid = false;
            sweetAlert("Sorry", "New Password is Invalid. Please Try Again", "error");
            vm.newPassword = ''
        }
        
        if (valid) {
            sessionClass.changePassword(vm.newPassword);
            sweetAlert("Well Done!!", "Information have been Updated", "success");
            vm.newPassword = ''
        }
    }

    // *** this part is controller method for calendar in appointment view

    vm.addNew = false;

    vm.isAdd = function () {
        return vm.addNew;
    }

    vm.toggleAdd = function () {
        vm.addNew = !vm.addNew;
    }

    //These variables MUST be set as a minimum for the calendar to work
    vm.calendarView = 'month';
    vm.calendarDay = new Date();
    vm.events = [];

    function showModal1(action, event) {
        $modal.open({
            templateUrl: 'modalContent.html',
            controller: function () {
                var vm = this;
                vm.action = action;
                vm.event = event;
            },
            controllerAs: 'vm'
        });
    }

    function showModal2(action, event) {
        $modal.open({
            templateUrl: 'modalContent2.html',
            controller: function () {
                var vm = this;
                vm.action = action;
                vm.event = event;
            },
            controllerAs: 'vm'
        });
    }


    vm.eventClicked = function (event) {
        showModal1('Clicked', event);
    };

    vm.eventEdited = function (event) {
        showModal1('Edited', event);
    };

    vm.eventDeleted = function (event) {
        vm.delEvents(event.id);

    };

    vm.delEvents = function (eid) {
        var pos
        for (event in vm.events) {
            if (vm.events[event].id == eid) {
                pos = event;
            }
        }
        vm.events.splice(pos, 1);
    }

    vm.eventTimesChanged = function (event) {
        showModal1('Dropped or resized', event);
    };

    vm.toggle = function ($event, field, event) {
        $event.preventDefault();
        $event.stopPropagation();
        event[field] = !event[field];
    };
    
    vm.currentOperationDay = 0;
    vm.currentOperationType = "info";
    vm.currentOperation = "del";

    //calendar operation button -> set and alert choosen date and time appointment
    vm.setCurrentCeilOperation = function (date, type, num_current_event) {
        //        vm.currentOperationDay=date;//type data is date
        //        vm.currentOperationType=type;//type data is "info" or "warning"
        //        if(num_current_event>0){
        //            vm.currentOperation="del";
        //            $rootScope.Ui.turnOn("modalsAddAppointment");
        //            //console.log(date.toDate();
        //            //console.log(vm.nState.date);
        //            vm.nState.date = date.toDate();
        //            if(type=='info')vm.nState.period='morning'
        //            else vm.nState.period='afternoon'
        //        }else{
        //            vm.currentOperation="add";
        //            vm.addNewWorkDay(vm.currentOperationDay,vm.currentOperationType);
        //        }
        vm.pState.date = date.format("YYYY-MM-DD");
        
        var time = "8.00-11.00";
        if (type == 'info') {
            vm.pState.period = 0
        } else {
            time = "13.00-16.00"
            vm.pState.period = 1
        }
        sweetAlert("Selected!!", vm.pState.date + " " + time, "info");
    }
    
    vm.dateAppointment = "12-08-2015 8:00-11:00";
    
    //calendar operation button -> set and alert choosen date and time appointment
    vm.changeDate = function (date, type, num_current_event) {
        //        vm.appState.date=new Date(date.toDate().setHours(0,0,0,0));
        //        if(type=='info') vm.appState.period='morning';
        //        else vm.appState.period='afternoon';
        //        vm.calendarDay = vm.appState.date;
        //        if(type=="info") vm.dateAppointment=date.format("YYYY-M-D")+" 8:00-11:00";
        //        else vm.dateAppointment=date.format("YYYY-M-D")+" 13:00-16:00";
        vm.pState.date = date.format("YYYY-MM-DD");
        //        if (num_current_event > 0) {
        //            vm.currentOperation = "del";
        //            $rootScope.Ui.turnOn("modalsAddAppointment");
        //            //console.log(date.toDate();
        //            //console.log(vm.nState.date);
        //            //vm.nState.date = date.toDate();
        //
        var time = "8.00-11.00";
        if (type == 'info') {
            vm.pState.period = 0
        } else {
            time = "13.00-16.00"
            vm.pState.period = 1
        }
        sweetAlert("Selected!!", vm.pState.date + " " + time, "info");
    }

    vm.addNewWorkDay = function (currentOperationDay, currentOperationType) {
        if (vm.currentOperationType == "info") {
            var start = vm.currentOperationDay.format("YYYY-M-D") + " 8:00"
            var end = vm.currentOperationDay.format("YYYY-M-D") + " 11:00"
        } else {
            var start = vm.currentOperationDay.format("YYYY-M-D") + " 13:00"
            var end = vm.currentOperationDay.format("YYYY-M-D") + " 16:00"
        }
        
        var elem = {
            title: "<dummy>",
            type: currentOperationType,
            startsAt: moment(start).toDate(),
            endsAt: moment(end).toDate(),
            draggable: true,
            resizable: true,

        }
        vm.events.push(elem);
    }
    
    vm.deleteWorkDay = function () {
        for (var ceil = 0; ceil < vm.events.length; ceil++) {
            if (vm.events[ceil].startsAt.getDate() == vm.currentOperationDay.toDate().getDate() && vm.events[ceil].type === vm.currentOperationType) {
                vm.events.splice(ceil, 1);
                ceil--;
            }
        }
    }

    


});
