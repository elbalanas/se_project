app.controller('doctorCtrl', function ($modal, moment, patientClass, patientInfoClass, drugAllergyClass, medicalHistoryClass, drugUseClass, appointmentClass, doctorClass, drugOrderClass, drugClass, diseaseClass, $rootScope, sessionClass) {

    var vm = this;



    /*



        Space for Comment



    */

    vm.allAppointment = appointmentClass.appointment;
    vm.aPatient = patientClass.patient;
    vm.aPatientInfo = patientInfoClass.info;
    vm.drugAllergy = drugAllergyClass.allergy;
    vm.aDoctor = doctorClass.doctor;
    vm.allMedicalHistory = medicalHistoryClass.history;
    vm.diseaseList = diseaseClass.disease;
    vm.drugList = drugClass.drug;
    vm.drugUse = drugUseClass.drugUse;
    vm.drugOrder = drugOrderClass.order;
    vm.session = sessionClass.session;

    var aDay = new Date();

    vm.generatePeriod = function (aDay) {
        var hour = aDay.getHours();
        var min = aDay.getMinutes();
        min = (hour * 60) + min;
        if (min > 720) {
            return '1'
        }
        if (min <= 720) {
            return '0'
        }
    }

    vm.timeByPeriod = function (period, set) {
        if (period == 0) {
            if (set == 0) return '8:00'
            else return '11:00'
        }
        if (period == 1) {
            if (set == 0) return '13:00'
            else return '16:00'
        }
    }
    
    vm.tranSex = function (sex) {
        if (sex == 1) return 'Female'
        if (sex == 0) return 'Male'
        return '-'
    }
    
    vm.checkCause = function (cause) {
        if (cause === 'undefined' || cause == null || cause == '') return '-'
        else return cause
    }

    //initialize this page
    vm.init = function () {
        vm.today = aDay.getFullYear() + '-' + (aDay.getMonth() + 1) + '-' + aDay.getDate();
        vm.period = vm.generatePeriod(aDay);

        doctorClass.getADoctor(vm.session.id).then(function () {
            vm.initProfile();
        })
        appointmentClass.getDoctorAppointmentById(vm.session.id, vm.today);
        diseaseClass.getDiseaseList();
        drugClass.getDrugList();

        vm.currentMonth = aDay.getMonth() + 1;
        vm.currentYear = aDay.getFullYear();
    }

    sessionClass.getSession().then(function () {
        vm.init();
    });

    //check if today doctor have or have not appointment
    //not complete appointment.status is broken
    vm.checkAppointment = function (appointment) {
        if (appointment.status) return true
        return false
    }
    
    //check if drug and drugList in drugOrder is duplicated
    vm.checkDrugDup = function (drug) {
        var count = 0;
        for (aDrug of vm.drugOrder.drugList) {
            if (aDrug.drugId == drug.drugId) {
                count++;
            }
            if (count >= 2) return true
        }
        return false
    }

    //setup state from choosen appointment
    vm.setState = function (appointment) {
        vm.focusAppointment = appointment

        patientClass.getAPatient(appointment.patientId);
        patientInfoClass.getPatientInfoById(appointment.patientId, vm.today);
        drugAllergyClass.getPatientAllergyById(appointment.patientId);
        medicalHistoryClass.getAllMedicalHistoryById(appointment.patientId, vm.range).then(function () {
            vm.setHistory(vm.allMedicalHistory[0]);
        })

        vm.newDrugAllergy = [];
        vm.allergySlotCount = 0;
        vm.pState = {};
        vm.range = 1;
        vm.modalControl = ''
        
        vm.currentMonth = aDay.getMonth() + 1;
        vm.currentYear = aDay.getFullYear();
    }
    
    //reset focus state
    vm.clearState = function () {
        vm.newDrugAllergy = [];
        vm.allergySlotCount = 0;
        vm.pState = {};
        vm.focusAppointment = {};
        vm.focusHistory = {};
        vm.newAppointment = {};
    }

    //set showing medical history to choosen medical history 
    vm.setHistory = function (history) {
        vm.focusHistory = history;
        patientInfoClass.getPatientInfoById(vm.aPatient.patientId, vm.focusHistory.date);
        drugUseClass.getDrugUseById(vm.aPatient.patientId, vm.focusHistory.date)
    }

    //get today measurement of focus patient
    vm.getTodayInfo = function () {
        patientInfoClass.getPatientInfoById(vm.aPatient.patientId, vm.today)
    }
    
    //add a new drugAllergy slot
    vm.addAllergySlot = function () {
        var temp = {
            drugAllergyId: '',
            drugId: vm.drugList[0].drugId,
            patientId: vm.aPatient.patientId,
            drugName: ''
        }
        vm.findDrug(temp, temp.drugId);
        vm.newDrugAllergy.push(temp)
        vm.allergySlotCount = vm.allergySlotCount + 1;
    }

    //del choosen new drugAllergy slot
    vm.delAllergySlot = function (index) {
        vm.newDrugAllergy.splice(index, 1);
        vm.allergySlotCount = vm.allergySlotCount - 1;
    }

    //save drugAllergy from new drugAllergy slots to DB for this patient
    vm.saveNewAllergy = function () {
        
        //check duplication between new ones and old ones
        var dup = false;
        for (nDrug of vm.newDrugAllergy) {
            for (oDrug of vm.drugAllergy) {
                if (nDrug.drugId == oDrug.drugId) {
                    dup = true;
                }
            }
        }
        if (dup) {
            sweetAlert('Sorry!!', 'Duplicated', 'error')
            return
        }
        
        //save it
        if (!dup) {
            drugAllergyClass.addAllergyById(vm.aPatient.patientId, vm.newDrugAllergy).then(function () {
                sweetAlert('Well Done!!', 'Information have been Updated', 'success');
                drugAllergyClass.getPatientAllergyById(vm.aPatient.patientId);
                vm.newDrugAllergy = [];
                vm.allergySlotCount = 0;
            });
        }
    }

    //get 10 newer medical history list by step up range by 1
    //unchecked yet need more validate and check bordery
    vm.previousUse = function () {
        if (vm.range == 1) return
        vm.range = vm.range - 1;
        medicalHistoryClass.getAllMedicalHistoryById(vm.focusAppointment.patientId, vm.range).then(function () {
                vm.setHistory(vm.allMedicalHistory[0]);
            })
    }

    //get 10 older medical history list by step down range by 1
    //unchecked yet need more validate and check bordery
    vm.nextUse = function () {
        if (vm.drugUse.length < 10) return
        vm.range = vm.range + 1;
        medicalHistoryClass.getAllMedicalHistoryById(vm.focusAppointment.patientId, vm.range).then(function () {
                vm.setHistory(vm.allMedicalHistory[0]);
            })
    }

    //setup new AppointmentState , set to Available BestAppointment and generateCalendar for Adding new Appointment to DB
    vm.setNewAppointment = function () {
        vm.currentMonth = aDay.getMonth() + 1;
        vm.currentYear = aDay.getFullYear();
        
        vm.newAppointment = {
            appointmentId: '',
            patientId: vm.aPatient.patientId,
            patientFirstName: vm.aPatient.firstName,
            patientLastName: vm.aPatient.lastName,
            patientSsid: vm.aPatient.ssid,
            cause: '',
            staffId: '',
            staffFirstName: '',
            staffLastName: '',
            status: '0',
            departmentNumber: vm.aDoctor.departNumber,
            departmentName: '',
            doctorId: vm.aDoctor.doctorId,
            doctorFirstName: vm.aDoctor.firstName,
            doctorLastName: vm.aDoctor.lastName,
            type: '2',
            date: '',
            period: ''
        }

        appointmentClass.getBestAppointmentByDoctor(vm.aDoctor.departNumber, vm.aDoctor.doctorId)
            .then(function (res) {
                vm.newAppointment.date = res.data.date;
                vm.newAppointment.period = res.data.period;
                appointmentClass.getAvailableAppointment(vm.aDoctor.departNumber, vm.aDoctor.doctorId, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
                    vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear, 0);
                });
            });
    }
    
    //save new appointment state to DB
    vm.saveNewAppointment = function () {
        vm.staffId = null;
        
        appointmentClass.updateAppointment(vm.newAppointment)
        sweetAlert('Well Done!!', 'Information have been Updated', 'success')
        
        vm.clearState();
        vm.setNewAppointment();
    }
    
    //show choosen appointment date / time and newAppointment state Date to choosen date 
    vm.changeDate = function (date, type, num_current_event) {
        vm.newAppointment.date = date.format("YYYY-MM-DD");
        var time = "8.00-11.00";
        if (type == 'info') {
            vm.newAppointment.period = 0
        } else {
            time = "13.00-16.00"
            vm.newAppointment.period = 1
        }
        sweetAlert("Selected!!", vm.newAppointment.date + " " + time, "info");
    }

    //connect to today button in adding Appoinment calendar -> set calendar to current month and year
    vm.toThisMonth = function () {
        vm.currentMonth = aDay.getMonth() + 1;
        vm.currentYear = aDay.getFullYear();
        
        appointmentClass.getAvailableAppointment(vm.aDoctor.departNumber, vm.aDoctor.doctorId, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
            vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
        });
    }

    //connect to Previous button in adding Appoinment calendar -> set Calendar to 1 previous month 
    vm.toPreviousMonth = function () {
        if (vm.currentMonth == 1) {
            vm.currentMonth = 12;
            vm.currentYear = vm.currentYear - 1;
        } else vm.currentMonth = vm.currentMonth - 1;
        
        appointmentClass.getAvailableAppointment(vm.aDoctor.departNumber, vm.aDoctor.doctorId, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
            vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
        });
    }

    //connect to Next button in adding Appoinment calendar -> set Calendar to 1 next month 
    vm.toNextMonth = function () {
        if (vm.currentMonth == 12) {
            vm.currentMonth = 1;
            vm.currentYear = vm.currentYear + 1;
        } else vm.currentMonth = vm.currentMonth + 1;
        
        appointmentClass.getAvailableAppointment(vm.aDoctor.departNumber, vm.aDoctor.doctorId, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
            vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
        });
    }

    //generate information for adding Appoinment calendar according to choosen month and year
    //get appointment list from getAvailableAppointment() by appointmentClass
    vm.generateCalendar = function (appointment, month, year, flag) {
        var cal = true;
        var begin = 0;
        
        if (aDay.getFullYear() > year) cal = false;
        else if (aDay.getFullYear() == year) {
            if (aDay.getMonth() > month) cal = false;
            else if (aDay.getMonth() == month) {
                begin = aDay.getDate() * 2;
                cal = true;
            } else cal = true;
        } else cal = true;
        
        if (cal) {
            vm.events = [];
            for (var i = begin; i < appointment.length; i = i + 2) {
                if (appointment[i].status == 0 && appointment[i + 1].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,

                    }
                } else if (appointment[i].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingAfternoon: 10 - appointment[i + 1].numberOfAppointment
                    }
                } else if (appointment[i + 1].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingMorning: 10 - appointment[i].numberOfAppointment
                    }
                } else {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingMorning: 10 - appointment[i].numberOfAppointment,
                        remainingAfternoon: 10 - appointment[i + 1].numberOfAppointment
                    }
                }
                vm.events.push(ele);
            }
        }
    }

    //generate information for updating task calendar according to choosen month and year
    //get appointment list from getAvailableAppointment() by appointmentClass
    vm.generateCalendarD = function (appointment, month, year, flag) {
        var cal = true;
        var begin = 0;
        
        if (aDay.getFullYear() > year) cal = false;
        else if (aDay.getFullYear() == year) {
            if (aDay.getMonth() > month) cal = false;
            else if (aDay.getMonth() == month) {
                begin = aDay.getDate() * 2;
                cal = true;
            } else cal = true;
        } else cal = true;
        
        if (cal) {
            vm.eventsD = [];
            for (var i = begin; i < appointment.length; i = i + 2) {
                if (appointment[i].status == 0 && appointment[i + 1].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingMorning: -1,
                        remainingAfternoon: -1
                    }
                } else if (appointment[i].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingAfternoon: appointment[i + 1].numberOfAppointment,
                        remainingMorning: -1

                    }
                } else if (appointment[i + 1].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingMorning: appointment[i].numberOfAppointment,
                        remainingAfternoon: -1
                    }

                } else {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingMorning: appointment[i].numberOfAppointment,
                        remainingAfternoon: appointment[i + 1].numberOfAppointment
                    }
                }
                vm.eventsD.push(ele);
            }
        }
    }

    //connect to today button in updating task calendar -> set calendar to current month and year
    vm.toThisMonthD = function () {
        vm.currentMonth = aDay.getMonth() + 1;
        vm.currentYear = aDay.getFullYear();
        
        appointmentClass.getAvailableAppointment(vm.aDoctor.departNumber, vm.aDoctor.doctorId, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
            vm.generateCalendarD(res.data, vm.currentMonth - 1, vm.currentYear);
        });
    }

     //connect to Previous button in updating task calendar -> set Calendar to 1 previous month 
    vm.toPreviousMonthD = function () {
        if (vm.currentMonth == 1) {
            vm.currentMonth = 12;
            vm.currentYear = vm.currentYear - 1;
        } else vm.currentMonth = vm.currentMonth - 1;
        
        appointmentClass.getAvailableAppointment(vm.aDoctor.departNumber, vm.aDoctor.doctorId, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
            vm.generateCalendarD(res.data, vm.currentMonth - 1, vm.currentYear);
        });

    }

     //connect to Next button in updating task calendar -> set Calendar to 1 next month 
    vm.toNextMonthD = function () {
        if (vm.currentMonth == 12) {
            vm.currentMonth = 1;
            vm.currentYear = vm.currentYear + 1;
        } else vm.currentMonth = vm.currentMonth + 1;
        
        appointmentClass.getAvailableAppointment(vm.aDoctor.departNumber, vm.aDoctor.doctorId, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
            vm.generateCalendarD(res.data, vm.currentMonth - 1, vm.currentYear);
        });
    }

    
    
    //setup state for today medicalHistory and today drugOrder
    vm.setNewMedicalHistory = function () {
        
        //find and set state to today medicalHistory and drugOrder if have
        for (aHistory of vm.allMedicalHistory) {
            if (aHistory.date == vm.today) {
                vm.pState = aHistory;
                drugOrderClass.getDrugOrderById(vm.aPatient.patientId, vm.today).then(function (res) {
                    vm.drugOrder.prescriptionId = res.data.prescriptionId;
                })
                return;
            }
        }

        //if have not make a new medical history and drugOrder for today
        vm.pState = {
            medicalId: '',
            diseaseId: vm.diseaseList[0].diseaseId,
            diseaseName: vm.diseaseList[0].diseaseName,
            doctorId: vm.aDoctor.doctorId,
            doctorFirstName: vm.aDoctor.firstName,
            doctorLastName: vm.aDoctor.lastName,
            patientId: vm.aPatient.patientId,
            doctorNote: '',
            date: vm.today
        }
        vm.drugOrder = {
            prescriptionId: '',
            patientId: vm.aPatient.patientId,
            doctorId: vm.aDoctor.doctorId,
            doctorFirstName: vm.aDoctor.firstName,
            doctorLastName: vm.aDoctor.lastName,
            medicalId: '',
            date: vm.today,
            doctorNote: '',
            drugList: []
        }
    }

    //check corectness and save today medicalHistory and drugOrder state to DB
    vm.saveState = function () {
        vm.modalControl = ''
        var valid = true;
        
        if (vm.pState.diseaseId === 'undefined' || vm.pState.diseaseId == null || vm.pState.diseaseId == '' || isNaN(vm.pState.diseaseId)) {
            valid = false;
            sweetAlert("Submission Error!!", "Please Select Disease", "error");
            return
        }
        
        for (drug of vm.drugOrder.drugList) {
            drug.drugOrderId = '';
            if (vm.checkDrugDup(drug)) {
                valid = false;
                sweetAlert('Submission Error!!', 'Drug is Duplicated', 'error')
                return
            }
            if (drug.drugId === 'undefined' || drug.drugId == null || drug.drugId == '' || isNaN(drug.drugId)) {
                valid = false;
                sweetAlert('Submission Error!!', 'Please Select Drug', 'error')
                return
            }
            if (drug.drugQuantity === 'undefined' || drug.drugQuantity == null || drug.drugQuantity == '' || isNaN(drug.drugQuantity) || Number(drug.drugQuantity) >= 1000 || Number(drug.drugQuantity) < 0) {
                valid = false;
                sweetAlert('Submission Error!!', 'Drug Quantity does not Correct', 'error')
                return
            }
        }
        
        if (valid) {
            vm.modalControl = 'modals'
            medicalHistoryClass.updateMedicalHistory(vm.pState).then(function () {
                medicalHistoryClass.getMedicalHistoryById(vm.aPatient.patientId, vm.today).then(function () {
                    vm.drugOrder.medicalId = vm.allMedicalHistory.medicalId;
                    drugOrderClass.updateDrugOrder(vm.drugOrder);
                })
            })
            sweetAlert('Well Done!!', 'Information have been Updated', 'success');
            vm.clearState();
        }
    }

    //add a new Drug slot to DrugOrder list
    vm.addDrug = function () {
        vm.drugOrder.drugList.push({
            drugOrderId: '',
            drugId: '',
            drugName: '',
            drugQuantity: ''
        });
    }

    //remove a new Drug from drugOrder List
    vm.removeDrug = function (index) {
        vm.drugOrder.drugList.splice(index, 1)
    }

    //find drugName of DrugId and set drug.drugName to founded drugName
    vm.findDrug = function (drug, drugId) {
        for (aDrug of vm.drugList) {
            if (aDrug.drugId == drugId) {
                drug.drugName = aDrug.drugName;
            }
        }
    }

    //find diseaseName of diseaseId and set person.diseaseName to founded diseaseName
    vm.findDisease = function (person, diseaseId) {
        for (disease of vm.diseaseList) {
            if (person.diseaseId == disease.diseaseId) {
                person.diseaseName = disease.diseaseName
            }
        }
    }

    //initialize information for current user profile page
    vm.initProfile = function () {
        vm.profile = {
            doctorId: vm.aDoctor.doctorId,
            ssid: vm.aDoctor.ssid,
            firstName: vm.aDoctor.firstName,
            lastName: vm.aDoctor.lastName,
            telephone: vm.aDoctor.telephone,
            address: vm.aDoctor.address,
            departmentNumber: vm.aDoctor.departNumber,
            task: vm.aDoctor.task
        }
    }

    //check correctness and save current user profile state to DB
    vm.saveProfile = function () {
        var valid = true;
        var dId = vm.profile.doctorId
        
        if (vm.profile.firstName == '' || vm.profile.firstName === 'undefined' || vm.profile.firstName == null || vm.profile.firstName.length > 200) {
            valid = false;
            sweetAlert("Sorry!!", "First Name is Incorrect!", "error");
            return
        }
        if (vm.profile.lastName == '' || vm.profile.lastName === 'undefined' || vm.profile.lastName == null || vm.profile.lastName.length > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Last Name is Incorrect!", "error");
            return
        }
        if (vm.profile.telephone == '' || vm.profile.telephone === 'undefined' || isNaN(vm.profile.telephone) || vm.profile.telephone == null || vm.profile.telephone.length > 10) {
            valid = false;
            sweetAlert("Sorry!!", "Telephone is Incorrect!", "error");
            return
        }
        if (vm.profile.address == '' || vm.profile.address === 'undefined' || vm.profile.address == null || vm.profile.address > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Address is Incorrect!", "error");
            return
        }
        
        if (valid) {
            doctorClass.updateDoctor(vm.profile).then(function () {
                doctorClass.getADoctor(dId).then(function () {
                    vm.initProfile();
                })
            })
            sweetAlert("Well Done!!", "Information have been Updated", "success");
        }
    }

    //check correctness and save current user Password
    vm.changePassword = function () {
        var valid = true;
        if (vm.newPassword == '' || vm.newPassword === 'undefined' || vm.newPassword == null || vm.newPassword.length > 13) {
            valid = false;
            sweetAlert("Sorry", "New Password is Invalid. Please Try Again", "error");
            vm.newPassword = ''
        }
        if (valid) {
            sessionClass.changePassword(vm.newPassword);
            sweetAlert("Well Done!!", "Information have been Updated", "success");
            vm.newPassword = ''
        }
    }
    
    
    
    // *** this part is controller and method for calendar ***
    vm.addNew = false;
    
    vm.isAdd = function () {
        return vm.addNew;
    }

    vm.toggleAdd = function () {
        vm.addNew = !vm.addNew;
    }

    //These variables MUST be set as a minimum for the calendar to work
    vm.calendarView = 'month';
    vm.calendarDay = new Date();
    vm.events = [];

    //    function showModal1(action, event) {
    //        $modal.open({
    //            templateUrl: 'modalContent.html',
    //            controller: function () {
    //                var vm = this;
    //                vm.action = action;
    //                vm.event = event;
    //            },
    //            controllerAs: 'vm'
    //        });
    //    }
    //
    //    function showModal2(action, event) {
    //        $modal.open({
    //            templateUrl: 'modalContent2.html',
    //            controller: function () {
    //                var vm = this;
    //                vm.action = action;
    //                vm.event = event;
    //            },
    //            controllerAs: 'vm'
    //        });
    //    }

    vm.eventClicked = function (event) {
        showModal1('Clicked', event);
    };

    vm.eventEdited = function (event) {
        showModal1('Edited', event);
    };

    vm.eventDeleted = function (event) {
        //        showModal2('Deleted', event);
        vm.delEvents(event.id);

    };

    vm.delEvents = function (eid) {
        var pos
        for (event in vm.events) {
            if (vm.events[event].id == eid) {
                pos = event;
            }
        }
        vm.events.splice(pos, 1);
    }

    vm.eventTimesChanged = function (event) {
        showModal1('Dropped or resized', event);
    };

    vm.toggle = function ($event, field, event) {
        $event.preventDefault();
        $event.stopPropagation();
        event[field] = !event[field];
    };
    
    vm.currentOperationDay = 0;
    vm.currentOperationType = "info";
    vm.currentOperation = "del";
    
    //operation click button calendar 
    vm.setCurrentCeilOperation = function (date, type, num_current_event) {
        vm.currentOperationDay = date.add(1, 'd'); //type data is date
        vm.currentOperationType = type; //type data is "info" or "warning"
        
        var vMonth = date.get('month') + 1;
        var vDay = date.get('date') - 1;
        var vDate = date.get("year") + "-" + vMonth + "-" + vDay
        
        if (num_current_event >= 0) {
            var period = 0
            if (type == "info") {
                period = 0
            } else {
                period = 1
            }
            appointmentClass.deleteAvailableDate(vm.aDoctor.doctorId, vDate, period).then(function () {
                appointmentClass.addUnavailableDate(vm.aDoctor.doctorId, vDate, period).then(function () {
                    appointmentClass.getAvailableAppointment(vm.aDoctor.departNumber, vm.aDoctor.doctorId, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
                        vm.generateCalendarD(res.data, vm.currentMonth - 1, vm.currentYear, 0);
                        sweetAlert("Success!!", "UnAvailable Date has been Added.", "success");
                    });
                })
            })
        } else {
            vm.currentOperation = "add";
            vm.addNewWorkDay(vm.currentOperationDay, vm.currentOperationType);
            var period = 0
            if (type == "info") {
                period = 0
            } else {
                period = 1
            }
            appointmentClass.deleteUnavailableDate(vm.aDoctor.doctorId, vDate, period).then(function () {
                    appointmentClass.addAvailableDate(vm.aDoctor.doctorId, vDate, period).then(function () {
                        appointmentClass.getAvailableAppointment(vm.aDoctor.departNumber, vm.aDoctor.doctorId, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
                            vm.generateCalendarD(res.data, vm.currentMonth - 1, vm.currentYear, 0);
                            sweetAlert("Success!!", "Available Date has been Added.", "success");
                        });
                    })
                })
        }
    }

    vm.addNewWorkDay = function (currentOperationDay, currentOperationType) {
        if (vm.currentOperationType == "info") {
            var start = vm.currentOperationDay.format("YYYY-M-D") + " 8:00"
            var end = vm.currentOperationDay.format("YYYY-M-D") + " 11:00"

        } else {
            var start = vm.currentOperationDay.format("YYYY-M-D") + " 13:00"
            var end = vm.currentOperationDay.format("YYYY-M-D") + " 16:00"
        }
        
        var elem = {
            title: "<dummy>",
            type: currentOperationType,
            startsAt: moment(start).toDate(),
            endsAt: moment(end).toDate(),
            draggable: true,
            resizable: true,
        }
        vm.events.push(elem);
    }
    
    vm.deleteWorkDay = function () {
        for (var ceil = 0; ceil < vm.events.length; ceil++) {
            if (vm.events[ceil].startsAt.getDate() == vm.currentOperationDay.toDate().getDate() && vm.events[ceil].type === vm.currentOperationType) {
                vm.events.splice(ceil, 1);
                ceil--;
            }
        }
    }
        
});
