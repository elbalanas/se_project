app.controller('patientCtrl', function ($modal, moment, patientClass, appointmentClass, departmentClass, doctorClass, $rootScope, $scope, sessionClass) {

    var vm = this;



    vm.aPatient = patientClass.patient;
    vm.allAppointment = appointmentClass.appointment;
    vm.allDepartment = departmentClass.department
    vm.allDoctor = doctorClass.doctor;
    vm.session = sessionClass.session;
    var aDay = new Date();

    vm.tranSex = function (sex) {
        if (sex == 1) return 'Female'
        if (sex == 0) return 'Male'
        return '-'
    }

    vm.generatePeriod = function (aDay) {
        var hour = aDay.getHours();
        var min = aDay.getMinutes();
        min = (hour * 60) + min;
        if (min > 720) {
            return '1'
        }
        if (min <= 720) {
            return '0'
        }
    }
    
    vm.timeByPeriod = function (period, set) {
        if (period == 0) {
            if (set == 0) return '8:00'
            else return '11:00'
        }
        if (period == 1) {
            if (set == 0) return '13:00'
            else return '16:00'
        }
    }

    vm.showChangeDate = false;

    //toggle to show calendar or not
    vm.toggleChangeDate = function () {
        vm.showChangeDate = !vm.showChangeDate;
    }

    //turn off state modals after confirm to delete 
    vm.deleteAssignment = function () {
        $rootScope.Ui.turnOff("modals");
    }

    //initialize page
    vm.init = function () {
        vm.newPassword = '';

        vm.today = aDay.getFullYear() + '-' + (aDay.getMonth() + 1) + '-' + aDay.getDate();
        vm.period = vm.generatePeriod(aDay);

        vm.appointmentIndex = '0';

        patientClass.getAPatient(vm.session.id).then(function () {
            vm.initProfile();
        })
        appointmentClass.getPatientAppointById(vm.session.id);
        departmentClass.getDepartmentList();
    }

    sessionClass.getSession().then(function () {
        vm.init();
    });

    //set new Adding Appointment State (new)
    vm.setNewAppointment = function () {
        vm.pState = {
            appointmentId: '',
            patientId: vm.aPatient.patientId,
            patientFirstName: vm.aPatient.firstName,
            patientLastName: vm.aPatient.lastName,
            patientSsid: vm.aPatient.ssid,
            cause: '',
            staffId: '',
            staffFirstName: '',
            staffLastName: '',
            status: 'false',
            departmentNumber: '',
            departmentName: '',
            doctorId: '',
            doctorFirstName: '',
            doctorLastName: '',
            type: '0',
            date: '',
            period: ''
        }

        vm.choosenDepartment = vm.allDepartment[0].departmentNumber;
        doctorClass.getDoctorByDepartment(vm.choosenDepartment).then(function () {
            vm.showChangeDate = false;
            vm.currentMonth = aDay.getMonth() + 1;
            vm.currentYear = aDay.getFullYear();
            vm.pickFromDepartment(vm.choosenDepartment);
        })
    }

    //set appointment state to choosen appointment (edit)
    vm.setState = function (appointment) {
        vm.pState = appointment;

        vm.currentDepartment = {}
        vm.choosenDepartment = vm.pState.departmentNumber;
        vm.showChangeDate = false;
        vm.currentMonth = aDay.getMonth() + 1;
        vm.currentYear = aDay.getFullYear();

        doctorClass.getDoctorByDepartment(vm.choosenDepartment);

        vm.choosenDoctor = vm.pState.doctorId;
        vm.toThisMonth();
    }

    //save editing appointment state to DB (edit)
    vm.saveState = function () {
        vm.pState.departmentNumber = vm.choosenDepartment;
        vm.pState.doctorId = vm.choosenDoctor;
        vm.staffId = null;

        appointmentClass.updateAppointment(vm.pState)
        sweetAlert('Well Done!!', 'Information have been Updated', 'success')

        vm.clearState();
        vm.setNewAppointment();
    }

    //del editing appointment state from DB
    vm.delState = function () {
        appointmentClass.deleteAppointmentById(vm.pState.appointmentId).then(function () {
            appointmentClass.getPatientAppointById(vm.aPatient.patientId);
        })
        sweetAlert('Well Done!!', 'Information have been Deleted', 'success')
        
        vm.clearState();
    }
    
    //reset editing appointment state 
    vm.clearState = function () {
        vm.pState = {};
        vm.choosenDepartment = '';
        vm.choosenDoctor = '';
        vm.currentMonth = aDay.getMonth() + 1;
        vm.currentYear = aDay.getFullYear();
        
        appointmentClass.getPatientAppointById(vm.session.id);
    }

    //get doctor List of a department according to department number and set choosen doctor to the first one
    vm.changeDepartment = function (departmentNumber) {
        doctorClass.getDoctorByDepartment(departmentNumber).then(function () {
            vm.choosenDoctor = vm.allDoctor[0].doctorId;
        })
    }

    //pick best available appointment date for choosen department (dont care doctor) and generate calendar
    vm.pickFromDepartment = function (choosenDepartment) {
        appointmentClass.getBestAppointmentByDepartment(choosenDepartment).then(function (res) {
            vm.choosenDoctor = res.data.doctorId;
            vm.pState.date = res.data.date;
            vm.pState.period = res.data.period;
            
            appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
                vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
            });
        });
    }

    //pick best available appointment date for choosen doctor and department and generate calendar
    vm.pickFromDoctor = function (choosenDepartment, choosenDoctor) {
        appointmentClass.getBestAppointmentByDoctor(choosenDepartment, choosenDoctor).then(function (res) {
            vm.pState.date = res.data.date;
            vm.pState.period = res.data.period;
            
            appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
                vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
            });
        });
    }

    //connect to today button in calendar -> set and genrate calendar to This month and year
    vm.toThisMonth = function () {
        vm.currentMonth = aDay.getMonth() + 1;
        vm.currentYear = aDay.getFullYear();
        
        appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
            vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
        });
    }

    //connect to previous button in calendar -> set and generate calendar step down by 1 previous month
    vm.toPreviousMonth = function () {
        if (vm.currentMonth == 1) {
            vm.currentMonth = 12;
            vm.currentYear = vm.currentYear - 1;
        } else vm.currentMonth = vm.currentMonth - 1;
        
        appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
            vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
        });

    }

    //connect to next button in calendar -> set and generate calendar step up by 1 next month
    vm.toNextMonth = function () {
        if (vm.currentMonth == 12) {
            vm.currentMonth = 1;
            vm.currentYear = vm.currentYear + 1;
        } else vm.currentMonth = vm.currentMonth + 1;
        
        appointmentClass.getAvailableAppointment(vm.choosenDepartment, vm.choosenDoctor, vm.currentMonth + "", vm.currentYear + '').then(function (res) {
            vm.generateCalendar(res.data, vm.currentMonth - 1, vm.currentYear);
        });
    }

    //generate data for calendar according to month / year
    //appointment get from getAvalableAppointment() by appointment class
    vm.generateCalendar = function (appointment, month, year) {
        var cal = true;
        var begin = 0;
        
        if (aDay.getFullYear() > year) cal = false;
        else if (aDay.getFullYear() == year) {
            if (aDay.getMonth() > month) cal = false;
            else if (aDay.getMonth() == month) {
                begin = aDay.getDate() * 2;
                console.log(begin)
                cal = true;
            } else cal = true;
        } else cal = true;
        
        vm.events = [];
        if (cal) {
            for (var i = begin; i < appointment.length; i = i + 2) {
                if (appointment[i].status == 0 && appointment[i + 1].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                    }
                } else if (appointment[i].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingAfternoon: 10 - appointment[i + 1].numberOfAppointment
                    }
                } else if (appointment[i + 1].status == 0) {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingMorning: 10 - appointment[i].numberOfAppointment
                    }
                } else {
                    var ele = {
                        type: "info",
                        startsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        endsAt: moment([year, month, 1]).add(Math.floor(i / 2), 'd').toDate(),
                        draggable: true,
                        resizable: true,
                        remainingMorning: 10 - appointment[i].numberOfAppointment,
                        remainingAfternoon: 10 - appointment[i + 1].numberOfAppointment
                    }
                }
                vm.events.push(ele);
            }
        }
    }

    //initialize current user profile page
    vm.initProfile = function () {
        vm.profile = {
            patientId: vm.aPatient.patientId,
            ssid: vm.aPatient.ssid,
            firstName: vm.aPatient.firstName,
            lastName: vm.aPatient.lastName,
            telephone: vm.aPatient.telephone,
            address: vm.aPatient.address,
            sex: vm.aPatient.sex,
            email: vm.aPatient.email
        }
    }

    //check and save current user Profile toDB
    vm.saveProfile = function () {
        var valid = true;
        var pId = vm.profile.patientId
        
        if (vm.profile.firstName == '' || vm.profile.firstName === 'undefined' || vm.profile.firstName == null || vm.profile.firstName.length > 200) {
            valid = false;
            sweetAlert("Sorry!!", "First Name is Incorrect!", "error");
            return
        }
        if (vm.profile.lastName == '' || vm.profile.lastName === 'undefined' || vm.profile.lastName == null || vm.profile.lastName.length > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Last Name is Incorrect!", "error");
            return
        }
        if (vm.profile.telephone == '' || vm.profile.telephone === 'undefined' || isNaN(vm.profile.telephone) || vm.profile.telephone == null || vm.profile.telephone.length > 10) {
            valid = false;
            sweetAlert("Sorry!!", "Telephone is Incorrect!", "error");
            return
        }
        if (vm.profile.address == '' || vm.profile.address === 'undefined' || vm.profile.address == null || vm.profile.address > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Address is Incorrect!", "error");
            return
        }
        if (vm.profile.email == '' || vm.profile.email === 'undefined' || vm.profile.email == null || vm.profile.email.length > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Email is Incorrect!", "error");
            return
        }
        
        if (valid) {
            patientClass.updatePatient(vm.profile).then(function () {
                patientClass.getAPatient(pId).then(function () {
                    vm.initProfile();
                })
            })
            sweetAlert("Well Done!!", "Information have been Updated", "success");
        }
    }

    //check and save current user password
    vm.changePassword = function () {
        var valid = true;
        
        if (vm.newPassword == '' || vm.newPassword === 'undefined' || vm.newPassword == null || vm.newPassword.length > 13) {
            valid = false;
            sweetAlert("Sorry", "New Password is Invalid. Please Try Again", "error");
            vm.newPassword = ''
        }
        
        if (valid) {
            sessionClass.changePassword(vm.newPassword);
            sweetAlert("Well Done!!", "Information have been Updated", "success");
            vm.newPassword = ''
        }
    }



    // *** this part is controller and method for calendar ***

    vm.addNew = false;

    vm.isAdd = function () {
        return vm.addNew;
    }

    vm.toggleAdd = function () {
        vm.addNew = !vm.addNew;
    }

    //These variables MUST be set as a minimum for the calendar to work
    vm.calendarView = 'month';
    vm.calendarDay = new Date();
    vm.events = [];

    function showModal1(action, event) {
        $modal.open({
            templateUrl: 'modalContent.html',
            controller: function () {
                var vm = this;
                vm.action = action;
                vm.event = event;
            },
            controllerAs: 'vm'
        });
    }

    function showModal2(action, event) {
        $modal.open({
            templateUrl: 'modalContent2.html',
            controller: function () {
                var vm = this;
                vm.action = action;
                vm.event = event;
            },
            controllerAs: 'vm'
        });
    }

    vm.eventClicked = function (event) {
        showModal1('Clicked', event);
    };

    vm.eventEdited = function (event) {
        showModal1('Edited', event);
    };

    vm.eventDeleted = function (event) {
        vm.delEvents(event.id);
    };

    vm.delEvents = function (eid) {
        var pos
        for (event in vm.events) {
            if (vm.events[event].id == eid) {
                pos = event;
            }
        }
        vm.events.splice(pos, 1);
    }

    vm.eventTimesChanged = function (event) {
        showModal1('Dropped or resized', event);
    };

    vm.toggle = function ($event, field, event) {
        $event.preventDefault();
        $event.stopPropagation();
        event[field] = !event[field];
    };
    
    vm.currentOperationDay = 0;
    vm.currentOperationType = "info";
    vm.currentOperation = "del";

    //button operation in appointment calendar -> set date and show alert for choosen date
    vm.setCurrentCeilOperation = function (date, type, num_current_event) {
        //console.log(date);
        //        vm.currentOperationDay = date; //type data is date
        //        vm.currentOperationType = type; //type data is "info" or "warning"
        //vm.pState.period=;
        //console.log(date.getDate());
        vm.pState.date = date.format("YYYY-MM-DD");
        //        if (num_current_event > 0) {
        //            vm.currentOperation = "del";
        //            $rootScope.Ui.turnOn("modalsAddAppointment");
        //            //console.log(date.toDate();
        //            //console.log(vm.nState.date);
        //            //vm.nState.date = date.toDate();
        var time = "8.00-11.00";
        if (type == 'info') {
            vm.pState.period = 0
        } else {
            time = "13.00-16.00"
            vm.pState.period = 1
        }
        sweetAlert("Selected!!", vm.pState.date + " " + time, "info");
        //        } else {
        //            vm.currentOperation = "add";
        //            vm.addNewWorkDay(vm.currentOperationDay, vm.currentOperationType);
        //        }
    }
    
    vm.dateAppointment = "12-08-2015 8:00-11:00";
    
    //button operation in appointment calendar -> set date and show alert for choosen date
    vm.changeDate = function (date, type, num_current_event) {
        //        vm.appState.date = new Date(date.toDate().setHours(0, 0, 0, 0));
        //        if (type == 'info') vm.appState.period = 'morning';
        //        else vm.appState.period = 'afternoon';
        //        vm.calendarDay = vm.appState.date;
        vm.pState.date = date.format("YYYY-MM-DD");
        //        if (num_current_event > 0) {
        //            vm.currentOperation = "del";
        //            $rootScope.Ui.turnOn("modalsAddAppointment");
        //            //console.log(date.toDate();
        //            //console.log(vm.nState.date);
        //            //vm.nState.date = date.toDate();
        var time = "8.00-11.00";
        if (type == 'info') {
            vm.pState.period = 0
        } else {
            time = "13.00-16.00"
            vm.pState.period = 1
        }
        sweetAlert("Selected!!", vm.pState.date + " " + time, "info");
        //        if(type=="info") vm.dateAppointment=date.format("YYYY-M-D")+" 8:00-11:00";
        //        else vm.dateAppointment=date.format("YYYY-M-D")+" 13:00-16:00";
    }

    vm.addNewWorkDay = function (currentOperationDay, currentOperationType) {
        if (vm.currentOperationType == "info") {
            var start = vm.currentOperationDay.format("YYYY-M-D") + " 8:00"
            var end = vm.currentOperationDay.format("YYYY-M-D") + " 11:00"
        } else {
            var start = vm.currentOperationDay.format("YYYY-M-D") + " 13:00"
            var end = vm.currentOperationDay.format("YYYY-M-D") + " 16:00"
        }
        
        var elem = {
            title: "<dummy>",
            type: currentOperationType,
            startsAt: moment(start).toDate(),
            endsAt: moment(end).toDate(),
            draggable: true,
            resizable: true,
        }
        vm.events.push(elem);
    }
    
    vm.deleteWorkDay = function () {
        for (var ceil = 0; ceil < vm.events.length; ceil++) {
            if (vm.events[ceil].startsAt.getDate() == vm.currentOperationDay.toDate().getDate() && vm.events[ceil].type === vm.currentOperationType) {
                vm.events.splice(ceil, 1);
                console.log(ceil);
                ceil--;
            }
        }
    }
    
});
