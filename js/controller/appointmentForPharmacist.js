app.controller('pharmacistCtrl', function ($modal,appointmentClass, patientClass, patientInfoClass, pharmacistClass, drugAllergyClass, drugUseClass, medicalHistoryClass, drugOrderClass, drugClass, $rootScope, sessionClass) {

    var vm = this;

    /*



        Space for Comment



    */

    vm.aPatient = patientClass.patient
    vm.aPharmacist = pharmacistClass.pharmacist;
    vm.allAppointment = appointmentClass.appointment;
    vm.aPatientInfo = patientInfoClass.info;
    vm.drugAllergy = drugAllergyClass.allergy;
    vm.drugOrder = drugOrderClass.order;
    vm.drugUse = drugUseClass.drugUse;
    vm.aMedicalHistory = medicalHistoryClass.history;
    vm.allDrug = drugClass.drug;
    vm.session = sessionClass.session;

    vm.generatePeriod = function (aDay) {
        var hour = aDay.getHours();
        var min = aDay.getMinutes();
        min = (hour * 60) + min;
        if (min > 720) {
            return '1'
        }
        if (min <= 720) {
            return '0'
        }
    }
    
    vm.tranSex = function (sex) {
        if (sex == '0') return 'Male'
        if (sex == '1') return 'Female'
    }
    
    vm.checkCause = function (cause) {
        if (cause === 'undefined' || cause == null) return '-'
        else return cause
    }

    vm.tranSex = function (sex) {
        if (sex == '0') return 'Male'
        if (sex == '1') return 'Female'
    }
    
    //check if a drugUse is duplicated
    vm.checkDrugDup = function (aDrugId) {
        var drugCount = 0;
        for (drug of vm.pState.drugList) {
            if (aDrugId == drug.drugId) {
                drugCount = drugCount + 1
            }
        }
        return drugCount;
    }

    //initialize this page
    vm.init = function () {
        var aDay = new Date();
        vm.today = aDay.getFullYear() + '-' + (aDay.getMonth() + 1) + '-' + aDay.getDate();
        vm.period = vm.generatePeriod(aDay);
        
        vm.searchArg = "";
        
        pharmacistClass.getAPharmacist(vm.session.id).then(function(){
            vm.initProfile();
        })
        appointmentClass.getAllTodayAppointment();
        drugClass.getDrugList();
    }
    
    sessionClass.getSession().then(function () {
        vm.init();
    });

    //set today drugUse for choosen patient
    vm.setState = function (person) {
        patientClass.getAPatient(person.patientId);
        patientInfoClass.getPatientInfoById(person.patientId, vm.today);
        medicalHistoryClass.getMedicalHistoryById(person.patientId, vm.today);
        drugAllergyClass.getPatientAllergyById(person.patientId);

        vm.pState = {
            drugUseId: '',
            prescriptionId: '',
            patientId: vm.aPatient.patientId,
            doctorId: '',
            doctorFirstName: '',
            doctorLastName: '',
            pharmacistId: vm.aPharmacist.pharmacistId,
            pharmacistFirstName: vm.aPharmacist.firstName,
            pharmacistLastName: vm.aPharmacist.lastName,
            medicalId: '',
            date: vm.today,
            drugList: []
        }

        //set state to drugUse if have
        drugUseClass.getAllDrugUseById(person.patientId, vm.range).then(function () {
            for (aUse of vm.drugUse) {
                if (aUse.date == vm.today) {
                    var temp = aUse;
                    angular.copy(temp,vm.pState);
                    break;
                }
            }
            vm.focusDrugUse = vm.drugUse[0];
        });

        //get drugOrder from doctor information and set it to druguse
        drugOrderClass.getDrugOrderById(person.patientId, vm.today).then(function (res) {
            vm.pState.prescriptionId = res.data.prescriptionId;
            vm.pState.patientId = person.patientId;
            vm.pState.doctorId = vm.drugOrder.doctorId;
            vm.pState.doctorFirstName = vm.drugOrder.doctorFirstName;
            vm.pState.doctorLastName = vm.drugOrder.doctorLastName;
            vm.pState.medicalId = vm.drugOrder.medicalId;
        });
        
        vm.range = 1;
        vm.focusPerson = person;
        vm.newDrugAllergy = [];
        vm.allergySlotCount = 0;
    }

    //reset drugUse state
    vm.clearState = function () {
        vm.focusPerson = {};
        vm.pState = {};
        vm.newDrugAllergy = [];
        vm.range = 1;
    }
    
    //check and save DrugUse to DB
    vm.saveState = function () {
        var valid = true;
        vm.modalControl = '';
        
        for (drug of vm.pState.drugList) {
            drug.drugUseOrderId = '';
            if (drug.drugId == '' || isNaN(drug.drugId) || drug.drugId === 'undefined' || drug.drugId == null) {
                valid = false;
                sweetAlert('Submission Error!!', 'Please Select Drug', 'error');
                return
            }
            if (drug.drugQuantity == '' || isNaN(drug.drugQuantity) || drug.drugQuantity === 'undefined' || drug.drugQuantity == null || Number(drug.drugQuantity)>=1000 || Number(drug.drugQuantity)<0) {
                valid = false;
                sweetAlert('Submission Error!!', 'Drug Quantity dose not Correct', 'error');
                return
            }
            if (vm.checkDrugDup(drug.drugId) > 1) {
                valid = false;
                sweetAlert('Sorry!!', 'Drug Use List has been Duplicated', 'error');
                return
            }
        }

        if (valid) {
            vm.modalControl = 'modals'
            vm.pState.pharmacistId= vm.aPharmacist.pharmacistId
            vm.pState.pharmacistFirstName= vm.aPharmacist.firstName
            vm.pState.pharmacistLastName= vm.aPharmacist.lastName
            
            drugUseClass.updateDrugUse(vm.pState).then(function(){
                //drugUseClass.getDrugUseById(vm.aPatient.patientId,vm.today);
            })
            
            sweetAlert('Well Done!!', 'Information has been Updated', 'success');
            vm.clearState();
        }  
    }

    //not use
    //check if patient already received drug
    vm.checkComplete = function () {
        for (drug of vm.pState.drugList) {
            if (drug.drugId == '' || drug.drugName == '' || drug.drugQuantity == '') {
                return true;
            }
        }
        return false
    }

    //get a 10 newer drugUse History list and set focusdrugUse history to first one
    //unchecked yet need more improve in bordery checking
    vm.previousUse = function () {
        if (vm.range == 1) return
        
        vm.range = vm.range - 1;
        
        drugUseClass.getAllDrugUseById(vm.focusPerson.patientId, vm.range).then(function(){
            vm.focusDrugUse = vm.drugUse[0];
        })
    }

    //get a 10 older drugUse History list and set focusdrugUse history to first one
    //unchecked yet need more improve in bordery checking
    vm.nextUse = function () {
        if (vm.drugUse.length < 10) return
        
        vm.range = vm.range + 1;
        
        drugUseClass.getAllDrugUseById(vm.focusPerson.patientId, vm.range).then(function(){
            vm.focusDrugUse = vm.drugUse[0];
        })
    }

    //set drugUseHistory focus to this drugUseHistory
    vm.setDrugUse = function (drugUse) {
        vm.focusDrugUse = drugUse;
    }
    
    vm.checkOrder = function (order) {
        if (vm.findInState(order) != -1) return true
        else return false;
    }
    
    //check if drugUse have already been in drugOrder
    vm.findInState = function (order) {
        for (state in vm.pState.drugList) {
            if (order.drugId == vm.pState.drugList[state].drugId) {
                return state
            }
        }
        return -1;
    }

    //del a drug in drugUse List which is same in drugOrder
    vm.delToState = function (drug) {
        var index = vm.findInState(drug);
        
        if (index == -1) return;
        else {
            vm.pState.drugList.splice(index, 1);
        }
    }

    //add a drug in DrugOrder to drugUseList
    vm.addToState = function (drug) {
        var index = vm.findInState(drug);
        
        if (index == -1) {
            var temp = drug;
            delete drug['drugOrderId']
            temp.drugUseOrderId = '';
            vm.pState.drugList.push(temp)
        }
    }

    //delete a slot from drugUseList
    vm.delState = function (index) {
        vm.pState.drugList.splice(index, 1);
    }

    //add a empty slot to drugUseList
    vm.addState = function () {
        var temp = {
            drugUseOrderId: '',
            drugName: '',
            drugId: vm.allDrug[0].drugId,
            drugQuantity: ''
        }
        vm.findDrug(temp, temp.drugId);
        vm.pState.drugList.push(temp)
    }

    //find and set drugName from drugId to aDrug
    vm.findDrug = function (aDrug, drugId) {
        for (drug of vm.allDrug) {
            if (drugId == drug.drugId) {
                aDrug.drugName = drug.drugName
            }
        }
    }

    //add a new allergy slot
    vm.addAllergySlot = function () {
        var temp = {
            drugAllergyId: '',
            drugId: vm.allDrug[0].drugId,
            patientId: vm.aPatient.patientId,
            drugName: ''
        }
        
        vm.findDrug(temp, temp.drugId);

        vm.newDrugAllergy.push(temp)
        vm.allergySlotCount = vm.allergySlotCount + 1;
    }

    //delete a new allergy slot from new allergy list
    vm.delAllergySlot = function (index) {
        vm.newDrugAllergy.splice(index, 1);
        vm.allergySlotCount = vm.allergySlotCount - 1;
    }

    //save allergy list to DB
    vm.saveNewAllergy = function () {
        var dup = false;
        
        for (nDrug of vm.newDrugAllergy) {
            for (oDrug of vm.drugAllergy) {
                if (nDrug.drugId == oDrug.drugId) {
                    dup = true;
                }
            }
        }
        
        if (dup) {
            sweetAlert('Sorry!!', 'Duplicated', 'error')
            return
        }
        if (!dup) {
            drugAllergyClass.addAllergyById(vm.aPatient.patientId, vm.newDrugAllergy).then(function () {
                sweetAlert('Well Done!!', 'Information have been Updated', 'success');
                drugAllergyClass.getPatientAllergyById(vm.aPatient.patientId);
                vm.newDrugAllergy = [];
                vm.allergySlotCount = 0;
            })
        }
    }

    //search today patient from their information
    vm.pSearch = function (person) {
        if (vm.searchArg == '') {
            return true;
        } else {
            if (person.doctorFirstName.search(vm.searchArg) != -1) {
                return true;
            }
            if (person.doctorLastName.search(vm.searchArg) != -1) {
                return true;
            }
            if (person.departmentName.search(vm.searchArg) != -1) {
                return true;
            }
            if (person.patientFirstName.search(vm.searchArg) != -1) {
                return true;
            }
            if (person.patientLastName.search(vm.searchArg) != -1) {
                return true;
            }
            if (person.patientSsid.search(vm.searchArg) != -1) {
                return true;
            }
        }
    } 
    
    //initialize current user profile page
    vm.initProfile = function () {
        vm.profile = {
            pharmacistId: vm.aPharmacist.pharmacistId,
            ssid: vm.aPharmacist.ssid,
            firstName: vm.aPharmacist.firstName,
            lastName: vm.aPharmacist.lastName,
            telephone: vm.aPharmacist.telephone,
            address: vm.aPharmacist.address
        }
    }

    //check and save current user profile to DB
    vm.saveProfile = function () {
        var valid = true;
        var pId = vm.profile.pharmacistId
        
        if (vm.profile.firstName == '' || vm.profile.firstName === 'undefined' || vm.profile.firstName == null || vm.profile.firstName.length>200) {
            valid = false;
            sweetAlert("Sorry!!", "First Name is Incorrect!", "error");
            return
        }
        if (vm.profile.lastName == '' || vm.profile.lastName === 'undefined' || vm.profile.lastName == null || vm.profile.lastName.length>200) {
            valid = false;
            sweetAlert("Sorry!!", "Last Name is Incorrect!", "error");
            return
        }
        if (vm.profile.telephone == '' || vm.profile.telephone === 'undefined' || isNaN(vm.profile.telephone) || vm.profile.telephone == null || vm.profile.telephone.length>10) {
            valid = false;
            sweetAlert("Sorry!!", "Telephone is Incorrect!", "error");
            return
        }
        if (vm.profile.address == '' || vm.profile.address === 'undefined' || vm.profile.address == null || vm.profile.address>200) {
            valid = false;
            sweetAlert("Sorry!!", "Address is Incorrect!", "error");
            return
        }
        
        if (valid) {
            pharmacistClass.updatePharmacist(vm.profile).then(function () {
                pharmacistClass.getAPharmacist(pId).then(function(){
                    vm.initProfile();  
                })
            })
            sweetAlert("Well Done!!", "Information have been Updated", "success");
        }
    }

    //save current user new password
    vm.changePassword = function () {
        var valid = true;
        
        if (vm.newPassword == '' || vm.newPassword === 'undefined' || vm.newPassword == null || vm.newPassword.length > 13) {
            valid = false;
            sweetAlert("Sorry", "New Password is Invalid. Please Try Again", "error");
            vm.newPassword = ''
        }
        
        if (valid) {
            sessionClass.changePassword(vm.newPassword);
            sweetAlert("Well Done!!", "Information have been Updated", "success");
            vm.newPassword = ''
        }
    }
    
});
