app.controller('adminCtrl', function ($modal, patientClass, doctorClass, pharmacistClass, nurseClass, departmentClass, staffClass, drugClass, diseaseClass, $rootScope, sessionClass) {

    var vm = this;



    /*



        Space for Comment



    */
    
    vm.allNurse = nurseClass.nurse;
    vm.allDoctor = doctorClass.doctor;
    vm.allDepartment = departmentClass.department;
    vm.allStaff = staffClass.staff;
    vm.allPharmacist = pharmacistClass.pharmacist;
    vm.allDrug = drugClass.drug;
    vm.allDisease = diseaseClass.disease;
    vm.session = sessionClass.session;

    
    vm.tranSex = function (sex) {
        if (sex == '0') return 'Male'
        if (sex == '1') return 'Female'
    }

    vm.generatePeriod = function (aDay) {
        var hour = aDay.getHours();
        var min = aDay.getMinutes();
        min = (hour * 60) + min;
        if (min > 720) {
            return '1'
        }
        if (min <= 720) {
            return '0'
        }
    }
    
    //translation adminFlag to Letter
    vm.tranAdminFlag = function (adminFlag) {
        if (adminFlag == 0) return "Staff"
        if (adminFlag == 1) return "Admin"
    }
    
    //initialize this page
    vm.init = function () {
        
        var aDay = new Date();
        vm.today = aDay.getFullYear() + '-' + (aDay.getMonth() + 1) + '-' + aDay.getDate();
        vm.period = vm.generatePeriod(aDay);
        
        vm.aStaff = [];
        staffClass.getAStaff(vm.session.id).then(function () {
            vm.aStaff = vm.allStaff;
            vm.initProfile();
            staffClass.getStaffList();
        })
        
        vm.selectType = 'doctor'
        
        doctorClass.getDoctorList();
        departmentClass.getDepartmentList();
        nurseClass.getNurseList();
        pharmacistClass.getPharmacistList();
        drugClass.getDrugList();
        diseaseClass.getDiseaseList();
        
        vm.focusObject = vm.allDoctor;
        vm.searchArg = "";
        vm.editing = 0;

    }

    sessionClass.getSession().then(function () {
        vm.init();
    });

    vm.capitalize = function (word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    }

    //change Object according to the type
    vm.changeType = function () {
        vm.searchArg = '';
        if (vm.selectType == 'doctor') vm.focusObject = vm.allDoctor;
        if (vm.selectType == 'nurse') vm.focusObject = vm.allNurse;
        if (vm.selectType == 'staff') vm.focusObject = vm.allStaff;
        if (vm.selectType == 'pharmacist') vm.focusObject = vm.allPharmacist;
        if (vm.selectType == 'drug') vm.focusObject = vm.allDrug;
        if (vm.selectType == 'disease') vm.focusObject = vm.allDisease;
        if (vm.selectType == 'department') vm.focusObject = vm.allDepartment;
    }

    //set state to the choosen object
    vm.setState = function (object) {
        vm.pState = {}
        var temp = object;
        angular.copy(temp, vm.pState)
    }

    //reset state
    vm.clearState = function () {
        vm.pState = {};
    }
    
    //check correctness of input and save state to DB according to its type 
    vm.saveState = function () {
        
        vm.modalControl = ''
        var valid = true;

        if (vm.selectType == 'doctor' || vm.selectType == 'nurse' || vm.selectType == 'staff' || vm.selectType == 'pharmacist') {
            if (vm.pState.firstName == '' || vm.pState.firstName === 'undefined' || vm.pState.firstName == null || vm.pState.firstName.length > 200) {
                valid = false;
                sweetAlert("Submission Error!!", "Please fill Firstname Correctly", "error");
                return
            }
            if (vm.pState.lastName == '' || vm.pState.lastName === 'undefined' || vm.pState.lastName == null || vm.pState.lastName.length > 200) {
                valid = false;
                sweetAlert("Submission Error!!", "Please fill Lastname Correctly", "error");
                return
            }
            if (vm.pState.ssid == '' || vm.pState.ssid === 'undefined' || isNaN(vm.pState.ssid) || vm.pState.ssid.length != 13 || vm.pState.ssid == null) {
                valid = false;
                sweetAlert("Submission Error!!", "Please fill SSID Correctly", "error");
                return
            }
            if (vm.pState.telephone == '' || vm.pState.telephone === 'undefined' || isNaN(vm.pState.telephone) || vm.pState.telephone == null || vm.pState.telephone.length > 10) {
                valid = false;
                sweetAlert("Submission Error!!", "Please fill Telephone Number Correctly", "error");
                return
            }

            if (vm.pState.address == '' || vm.pState.address === 'undefined' || vm.pState.address == null || vm.pState.address.length > 200) {
                valid = false;
                sweetAlert("Submission Error!!", "Please fill Address Correctly", "error");
                return
            }
            if (valid) {
                vm.pState.password = vm.pState.ssid;
            }
        }

        if (vm.selectType == 'doctor') {
            if (vm.pState.departmentNumber == '' || vm.pState.departmentNumber === 'undefined' || isNaN(vm.pState.departmentNumber) || vm.pState.departmentNumber == null) {
                valid = false;
                sweetAlert("Submission Error!!", "Please Select Department", "error");
                return
            }
            if (valid) {
                vm.modalControl = 'employeeModals'
                if (vm.editing == 0) {
                    vm.pState.doctorId = '';
                }
                doctorClass.updateDoctor(vm.pState).then(function () {
                    doctorClass.getDoctorList();
                    sweetAlert("Well Done!!", "Information has been Updated", "success");
                    vm.clearState();
                })
            }
        }

        if (vm.selectType == 'nurse' && valid) {
            vm.modalControl = 'employeeModals'
            if (vm.editing == 0) {
                vm.pState.nurseId = '';
            }
            nurseClass.updateNurse(vm.pState).then(function () {
                nurseClass.getNurseList();
                console.log(vm.pState);
                sweetAlert("Well Done!!", "Information has been Updated", "success");
                vm.clearState();
            })

        }

        if (vm.selectType == 'staff' && valid) {
            vm.modalControl = 'employeeModals'
            if (vm.editing == 0) {
                vm.pState.staffId = '';
            }
            staffClass.updateStaff(vm.pState).then(function () {
                staffClass.getStaffList();
                console.log(vm.pState);
                sweetAlert("Well Done!!", "Information has been Updated", "success");
                vm.clearState();
            })
        }

        if (vm.selectType == 'pharmacist' && valid) {
            vm.modalControl = 'employeeModals'
            if (vm.editing == 0) {
                vm.pState.pharmacistId = '';
            }
            pharmacistClass.updatePharmacist(vm.pState).then(function () {
                pharmacistClass.getPharmacistList();
                sweetAlert("Well Done!!", "Information has been Updated", "success");
                vm.clearState();
            })
        }

        if (vm.selectType == 'drug') {
            if (vm.pState.drugName == '' || vm.pState.drugName === 'undefined' || vm.pState.drugName == null || vm.pState.drugName.length > 100) {
                valid = false;
                sweetAlert("Submission Error!!", "Please fill Drug Name Correctly", "error");
                return
            }
            if (valid) {
                vm.modalControl = 'drugModals'
                if (vm.editing == 0) {
                    vm.pState.drugId = '';
                }
                drugClass.updateDrug(vm.pState).then(function () {
                    drugClass.getDrugList();
                    sweetAlert("Well Done!!", "Information has been Updated", "success");
                    vm.clearState();
                })
            }
        }


        if (vm.selectType == 'disease') {
            if (vm.pState.diseaseName == '' || vm.pState.diseaseName === 'undefined' || vm.pState.diseaseName == null || vm.pState.diseaseName.length > 100) {
                valid = false;
                sweetAlert("Submission Error!!", "Please fill Disease Name Correctly", "error");
                return
            }
            if (valid) {
                vm.modalControl = 'diseaseModals'
                if (vm.editing == 0) {
                    vm.pState.diseaseId = '';
                }
                diseaseClass.updateDisease(vm.pState).then(function () {
                    diseaseClass.getDiseaseList();
                    console.log(vm.pState);
                    sweetAlert("Well Done!!", "Information has been Updated", "success");
                    vm.clearState();
                })
            }
        }

        if (vm.selectType == 'department') {
            if (vm.pState.departmentName == '' || vm.pState.departmentName === 'undefined' || vm.pState.departmentName == null || vm.pState.departmentName.length > 100) {
                valid = false;
                sweetAlert("Submission Error!!", "Please fill Department Name Correctly", "error");
                return
            }
            if (valid) {
                vm.modalControl = 'departmentModals'
                if (vm.editing == 0) {
                    vm.pState.departmentNumber = '';
                }
                departmentClass.updateDepartment(vm.pState).then(function () {
                    departmentClass.getDepartmentList();
                    console.log(vm.pState);
                    sweetAlert("Well Done!!", "Information has been Updated", "success");
                    vm.clearState();
                })
            }
        }
        
    }

    //del current state from DB according to its type
    vm.delState = function () {
        
        if (vm.selectType == 'doctor') {
            doctorClass.deleteDoctorById(vm.pState.doctorId).then(function () {
                doctorClass.getDoctorList();
                sweetAlert('Well Done!!', 'Information havee been Deleted', 'success')
                vm.clearState();
            })
        }
        
        if (vm.selectType == 'nurse') {
            nurseClass.deleteNurseById(vm.pState.nurseId).then(function () {
                nurseClass.getNurseList();
                sweetAlert('Well Done!!', 'Information havee been Deleted', 'success')
                vm.clearState();
            })
        }
        
        if (vm.selectType == 'staff') {
            staffClass.deleteStaffById(vm.pState.staffId).then(function () {
                staffClass.getStaffList();
                sweetAlert('Well Done!!', 'Information havee been Deleted', 'success')
                vm.clearState();
            })
        }
        
        if (vm.selectType == 'pharmacist') {
            pharmacistClass.deletePharmacistById(vm.pState.pharmacistId).then(function () {
                pharmacistClass.getPharmacistList();
                sweetAlert('Well Done!!', 'Information havee been Deleted', 'success')
                vm.clearState();
            })
        }
        
        if (vm.selectType == 'drug') {
            drugClass.deleteDrugById(vm.pState.drugId).then(function () {
                drugClass.getDrugList();
                sweetAlert('Well Done!!', 'Information havee been Deleted', 'success')
                vm.clearState();
            })
        }
        
        if (vm.selectType == 'disease') {
            diseaseClass.deleteDiseaseById(vm.pState.diseaseId).then(function () {
                diseaseClass.getDiseaseList()
                sweetAlert('Well Done!!', 'Information havee been Deleted', 'success')
                vm.clearState();
            })
        }
        
        if (vm.selectType == 'department') {
            departmentClass.deleteDepartmentById(vm.pState.departmentNumber).then(function () {
                departmentClass.getDepartmentList();
                sweetAlert('Well Done!!', 'Information havee been Deleted', 'success')
                vm.clearState();
            })
        }
        
    }

    //change Authority Status Between Admin to Staff and Staff to Admin
    vm.changeAuthor = function () {
        if (vm.pState.adminFlag == 0 || vm.pState.adminFlag == "0") {
            vm.pState.adminFlag = 1
        } else {
            vm.pState.adminFlag = 0
        }
        staffClass.changeAuthor(vm.pState.ssid, vm.pState.staffId, vm.pState.adminFlag).then(function () {
            staffClass.getStaffList().then(function () {
                for (staff of vm.allStaff) {
                    if (staff.staffId == vm.pState) {
                        vm.setState(staff);
                    }
                }
            })
        })
    }

    //search employee by their information
    vm.employeeSearch = function (person) {
        if (vm.searchArg == '') {
            return true;
        }
        if (person.firstName.search(vm.searchArg) != -1) {
            return true
        }
        if (person.lastName.search(vm.searchArg) != -1) {
            return true
        }
        if (person.ssid.search(vm.searchArg) != -1) {
            return true
        }
        return false;
    }
    
    //search drug by their information
    vm.drugSearch = function (object) {
        if (vm.searchArg == '') {
            return true;
        }
        if (object.drugName.search(vm.searchArg) != -1) {
            return true
        }
        if (object.drugId.search(vm.searchArg) != -1) {
            return true
        }
        return false;
    }

    //search department by their information
    vm.departmentSearch = function (object) {
        if (vm.searchArg == '') {
            return true;
        }
        if (object.departmentName.search(vm.searchArg) != -1) {
            return true
        }
        if (object.departmentNumber.search(vm.searchArg) != -1) {
            return true
        }
        return false;
    }

    //search desease by their information
    vm.diseaseSearch = function (object) {
        if (vm.searchArg == '') {
            return true;
        }
        if (object.diseaseName.search(vm.searchArg) != -1) {
            return true
        }
        if (object.diseaseId.search(vm.searchArg) != -1) {
            return true
        }
        return false;
    }

    //initialize profile for profile page
    vm.initProfile = function () {
        vm.profile = {
            staffId: vm.aStaff.staffId,
            ssid: vm.aStaff.ssid,
            firstName: vm.aStaff.firstName,
            lastName: vm.aStaff.lastName,
            telephone: vm.aStaff.telephone,
            address: vm.aStaff.address,
            adminFlag: vm.aStaff.adminFlag
        }
    }

    //check input correctness and save current user profile to DB 
    vm.saveProfile = function () {
        var valid = true;
        var sId = vm.profile.staffId
        
        if (vm.profile.firstName == '' || vm.profile.firstName === 'undefined' || vm.profile.firstName == null || vm.profile.firstName.length > 200) {
            valid = false;
            sweetAlert("Sorry!!", "First Name is Incorrect!", "error");
            return
        }
        if (vm.profile.lastName == '' || vm.profile.lastName === 'undefined' || vm.profile.lastName == null || vm.profile.lastName.length > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Last Name is Incorrect!", "error");
            return
        }
        if (vm.profile.telephone == '' || vm.profile.telephone === 'undefined' || isNaN(vm.profile.telephone) || vm.profile.telephone == null || vm.profile.telephone.length > 10) {
            valid = false;
            sweetAlert("Sorry!!", "Telephone is Incorrect!", "error");
            return
        }
        if (vm.profile.address == '' || vm.profile.address === 'undefined' || vm.profile.address == null || vm.profile.address > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Address is Incorrect!", "error");
            return
        }
        if (valid) {
            staffClass.updateStaff(vm.profile).then(function () {
                staffClass.getAStaff(sId).then(function () {
                    vm.initProfile();
                })
            })
            sweetAlert("Well Done!!", "Information have been Updated", "success");
        }
    }

    //check and save current user password 
    vm.changePassword = function () {
        var valid = true;
        if (vm.newPassword == '' || vm.newPassword === 'undefined' || vm.newPassword == null || vm.newPassword.length > 13) {
            valid = false;
            sweetAlert("Sorry", "New Password is Invalid. Please Try Again", "error");
            vm.newPassword = ''
        }
        if (valid) {
            sessionClass.changePassword(vm.newPassword);
            sweetAlert("Well Done!!", "Information have been Updated", "success");
            vm.newPassword = ''
        }
    }


});
