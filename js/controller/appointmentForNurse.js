app.controller('nurseCtrl', function ($modal, patientClass, nurseClass, patientInfoClass, appointmentClass, $rootScope, sessionClass) {

    var vm = this;

    /*



        Space for Comment



    */


    vm.aPatient = patientClass.patient;
    vm.aNurse = nurseClass.nurse;
    vm.aPatientInfo = patientInfoClass.info;
    vm.allAppointment = appointmentClass.appointment;
    vm.session = sessionClass.session;

    vm.generatePeriod = function (aDay) {
        var hour = aDay.getHours();
        var min = aDay.getMinutes();
        min = (hour * 60) + min;
        if (min > 720) {
            return '1'
        }
        if (min <= 720) {
            return '0'
        }
    }

    //initialize this page
    vm.init = function () {
        var aDay = new Date();
        vm.today = aDay.getFullYear() + '-' + (aDay.getMonth() + 1) + '-' + aDay.getDate();
        vm.period = vm.generatePeriod(aDay);
        
        vm.searchArg = "";
        
        nurseClass.getANurse(vm.session.id).then(function(){
            vm.initProfile();
        })
        appointmentClass.getAllTodayAppointment();
    }


    
    sessionClass.getSession().then(function () {
        vm.init();
    });
    
    vm.tranSex = function (sex) {
        if (sex == '0') return 'Male'
        if (sex == '1') return 'Female'
    }

    vm.checkCause = function (cause) {
        if (typeof cause === 'undefined' || cause == null) {
            return '-'
        } else return cause
    }
    
    //future work
    //check to show that this person information has already been collected
    vm.checkStatus = function (person) {
    }

    //set state of focus patient
    vm.setState = function (person) {
        patientClass.getAPatient(person.patientId);
        patientInfoClass.getPatientInfoById(person.patientId, vm.today).then(function () {
            vm.pState = vm.aPatientInfo
        })
        
        vm.focusPerson = person;
    }

    // reset state from focus
    vm.clearState = function () {
        vm.pState = {};
        vm.focusPerson = {};
    }

    //check and save measurement State to DB 
    vm.saveState = function () {
        vm.modalControl = 'none'
        var valid = true;
        
        if (vm.pState.weight == '' || isNaN(vm.pState.weight) || vm.pState.weight === 'undefined' || vm.pState.weight == null || Number(vm.pState.weight) >= 1000 || Number(vm.pState.weight) < 0) {
            valid = false;
            sweetAlert("Submission Error!!", "Please fill Weight Correctly!!", "error");
            return
        }
        if (vm.pState.height == '' || isNaN(vm.pState.height) || vm.pState.height === 'undefined' || vm.pState.height == null || Number(vm.pState.height) >= 1000 || Number(vm.pState.height) < 0) {
            valid = false;
            sweetAlert("Submission Error!!", "Please fill Height Correctly!!", "error");
            return
        }
        if (vm.pState.temperature == '' || isNaN(vm.pState.temperature) || vm.pState.temperature === 'undefined' || vm.pState.temperature == null || Number(vm.pState.temperature) >= 100 || Number(vm.pState.temperature) < 0) {
            valid = false;
            sweetAlert("Submission Error!!", "Please fill Temperature Correctly!!", "error");
            return
        }
        if (vm.pState.systolic == '' || isNaN(vm.pState.systolic) || vm.pState.systolic === 'undefined' || vm.pState.systolic == null || Number(vm.pState.systolic) >= 1000 || Number(vm.pState.systolic) < 0) {
            valid = false;
            sweetAlert("Sorry!!", "Please fill Blood Pressure Systolic Correctly!!", "error");
            return
        }
        if (vm.pState.diastolic == '' || isNaN(vm.pState.diastolic) || vm.pState.diastolic === 'undefined' || vm.pState.diastolic == null || Number(vm.pState.diastolic) >= 1000 || Number(vm.pState.diastolic) < 0) {
            valid = false;
            sweetAlert("Sorry!!", "Please fill Blood Pressure Diastolic Correctly!!", "error");
            return
        }
        if (vm.pState.heartRate == '' || isNaN(vm.pState.heartRate) || vm.pState.heartRate === 'undefined' || vm.pState.heartRate == null || Number(vm.pState.heartRate) >= 1000 || Number(vm.pState.heartRate) < 0) {
            valid = false;
            sweetAlert("Sorry!!", "Please fill Heart Rate Correctly!!", "error");
            return
        }

        if (valid) {
            vm.pState.nurseId = vm.aNurse.nurseId;
            vm.pState.nurseFirstName = vm.aNurse.firstName;
            vm.pState.nurseLastName = vm.aNurse.lastName;
            vm.pState.patientId = vm.focusPerson.patientId;
            vm.pState.patientFirstName = vm.focusPerson.patientFirstName;
            vm.pState.patientLastName = vm.focusPerson.patientLastName;
            vm.pState.date = vm.today;
            
            vm.modalControl = 'modals'
            
            patientInfoClass.updatePatientInfo(vm.pState);
            sweetAlert("Well Done!!", "Information has been Updated", "success");
            
            vm.clearState();
        }
    }

    //search today patient from their information
    vm.pSearch = function (person) {
        if (vm.searchArg == '') {
            return true;
        } else {
            if (person.patientFirstName.search(vm.searchArg) != -1) {
                return true;
            }
            if (person.patientLastName.search(vm.searchArg) != -1) {
                return true;
            }
            if (person.patientSsid.search(vm.searchArg) != -1) {
                return true;
            }
            if (person.doctorFirstName.search(vm.searchArg) != -1) {
                return true;
            }
            if (person.doctorLastName.search(vm.searchArg) != -1) {
                return true;
            }
            if (person.departmentName.search(vm.searchArg) != -1) {
                return true;
            }
        }
    }

    //initialize current user profile page
    vm.initProfile = function () {
        vm.profile = {
            nurseId: vm.aNurse.nurseId,
            ssid: vm.aNurse.ssid,
            firstName: vm.aNurse.firstName,
            lastName: vm.aNurse.lastName,
            telephone: vm.aNurse.telephone,
            address: vm.aNurse.address
        }
    }

    //check and save current user profile to DB
    vm.saveProfile = function () {
        var valid = true;
        var nId = vm.profile.nurseId
        
        if (vm.profile.firstName == '' || vm.profile.firstName === 'undefined' || vm.profile.firstName == null || vm.profile.firstName.length>200) {
            valid = false;
            sweetAlert("Sorry!!", "First Name is Incorrect!", "error");
            return
        }
        if (vm.profile.lastName == '' || vm.profile.lastName === 'undefined' || vm.profile.lastName == null || vm.profile.lastName.length>200) {
            valid = false;
            sweetAlert("Sorry!!", "Last Name is Incorrect!", "error");
            return
        }
        if (vm.profile.telephone == '' || vm.profile.telephone === 'undefined' || isNaN(vm.profile.telephone) || vm.profile.telephone == null || vm.profile.telephone.length>10) {
            valid = false;
            sweetAlert("Sorry!!", "Telephone is Incorrect!", "error");
            return
        }
        if (vm.profile.address == '' || vm.profile.address === 'undefined' || vm.profile.address == null || vm.profile.address>200) {
            valid = false;
            sweetAlert("Sorry!!", "Address is Incorrect!", "error");
            return
        }
        
        if (valid) {
            nurseClass.updateNurse(vm.profile).then(function () {
                nurseClass.getANurse(nId).then(function(){
                    vm.initProfile();  
                })
            })
            sweetAlert("Well Done!!", "Information have been Updated", "success");
        }
    }

    //check and save new Passward
    vm.changePassword = function () {
        var valid = true;
        if (vm.newPassword == '' || vm.newPassword === 'undefined' || vm.newPassword == null || vm.newPassword.length > 13) {
            valid = false;
            sweetAlert("Sorry", "New Password is Invalid. Please Try Again", "error");
            vm.newPassword = ''
        }
        if (valid) {
            sessionClass.changePassword(vm.newPassword);
            sweetAlert("Well Done!!", "Information have been Updated", "success");
            vm.newPassword = ''
        }
    }

});
