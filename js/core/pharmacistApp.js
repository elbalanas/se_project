var app = angular.module('myApp', ["mobile-angular-ui", 'ngRoute', 'mwl.calendar', 'ui.bootstrap', 'ngAnimate']);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: "dashboard.php",
        controller: 'pharmacistCtrl',
        controllerAs: 'vm'
    });
    $routeProvider.when('/dashboard', {
        templateUrl: "dashboard.php",
        controller: 'pharmacistCtrl',
        controllerAs: 'vm'
    });
    $routeProvider.when('/Profile', {
        templateUrl: "profile.php",
        controller: 'pharmacistCtrl',
        controllerAs: 'vm'
    });
    $routeProvider.otherwise({
        redirectTo: '/'
    });

});
