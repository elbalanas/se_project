var app = angular.module('myApp', ["mobile-angular-ui"]);

app.controller('firstPageCtrl', function (patientClass, $scope, SharedState) {

    $scope.pState = {};
    
    $scope.signUp = function () {
        var valid = true;
        if ($scope.pState.ssid == '' || $scope.pState.ssid === 'undefined' || isNaN($scope.pState.ssid) || $scope.pState.ssid == null || $scope.pState.ssid.length != 13 ) {
            valid = false;
            sweetAlert("Sorry!!", "Username is Incorrect!", "error");
            return
        }
        if ($scope.pState.firstName == '' || $scope.pState.firstName === 'undefined' || $scope.pState.firstName == null || $scope.pState.firstName > 200) {
            valid = false;
            sweetAlert("Sorry!!", "First Name is Incorrect!", "error");
            return
        }
        if ($scope.pState.lastName == '' || $scope.pState.lastName === 'undefined' || $scope.pState.lastName == null || $scope.pState.lastName.length > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Last Name is Incorrect!", "error");
            return
        }
        if ($scope.pState.sex == '' || $scope.pState.sex === 'undefined' || $scope.pState.sex == null || $scope.pState.sex > 1 || $scope.pState.sex<0) {
            valid = false;
            sweetAlert("Sorry!!", "Sex is Incorrect!", "error");
            return
        }
        if ($scope.pState.address == '' || $scope.pState.address === 'undefined' || $scope.pState.address == null || $scope.pState.address > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Address is Incorrect!", "error");
            return
        }
        if ($scope.pState.email == '' || $scope.pState.email === 'undefined' || $scope.pState.email == null || $scope.pState.email > 200) {
            valid = false;
            sweetAlert("Sorry!!", "Email is Incorrect!", "error");
            return
        }
        if ($scope.pState.telephone == '' || $scope.pState.telephone === 'undefined' || isNaN($scope.pState.telephone) || $scope.pState.telephone == null || $scope.pState.telephone.length > 10) {
            valid = false;
            sweetAlert("Sorry!!", "Telephone is Incorrect!", "error");
            return
        }
        if(valid){
        SharedState.setOne("activeTab", 1);
        patientClass.addPatient($scope.pState);
        console.log($scope.pState);
        //sweetAlert("Well Done!!", "You have successfully registered and logged in.", "success");
        swal({
            title: "Well Done!!",
            text: "You have successfully registered.",
            type: "success",
            //showCancelButton: true,
            confirmButtonColor: "#55ff55",
            confirmButtonText: "Go to Sign in!",
            closeOnConfirm: true
        });

        }
        $scope.pState;
    }

});
