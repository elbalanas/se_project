var app = angular.module('myApp', ["mobile-angular-ui", 'ngRoute', 'mwl.calendar', 'ui.bootstrap', 'ngAnimate']);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: "dashboard.php",
        controller: 'patientCtrl',
        controllerAs: 'vm'
    });
    $routeProvider.when('/dashboard', {
        templateUrl: "dashboard.php",
        controller: 'patientCtrl',
        controllerAs: 'vm'
    });
    $routeProvider.when('/Profile', {
        templateUrl: "profile.php",
        controller: 'patientCtrl',
        controllerAs: 'vm'
    });
    $routeProvider.otherwise({
        redirectTo: '/'
    });

});
