<!DOCTYPE html>
<html>


<?php

	include("../inc/config.php");
    include(root."inc/head.php");
    //include(root."inc/check_sign_in.php");
	//echo '<script src="'.base_url.'js/core/AppHome.js"></script>';
    //echo '<script src="'.base_url.'js/app.js"></script>';

	?>
    <?php echo '<link rel="stylesheet" type="text/css" href="'.base_url.'lib/sweet-alert/dist/sweetalert.css">' ?>
    <?php echo '<link rel="stylesheet" href="'.base_url.'lib/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css">' ?>










        <body>

            <div ng-app="myApp" class="scrollable">

                <div class="navbar navbar-app navbar-absolute-top ">
                    <div class="container">
                        <div class="row">

                            <div class="col-xs-4 text-left" style="margin-bottom:10px">
                                <p></p>
                                <h3>OPD System</h3>
                            </div>
                            <div class="col-xs-4 col-xs-offset-4 text-right">
                                <div class="btn-group" role="group" aria-label="...">

<!--                                    <button type="button" class="btn btn-nav">Boom Ittisut</button>-->

                                    <div class="btn-group" role="group">
                                        <a ui-turn-on='myDropdown' class='btn'>
                                            <i class="fa fa-ellipsis-v"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right " ui-outer-click="Ui.turnOff('myDropdown')" ui-outer-click-if="Ui.active('myDropdown')" role="menu" ui-show="myDropdown" ui-state="myDropdown" ui-turn-off="myDropdown">

                                            <li><a href=<?php echo base_url."admin/#/Profile"; ?> > Profile</a></li>
                                            <li><a href=<?php echo base_url."admin/#/Dashboard"; ?> > Dashboard</a></li>
                                            <li class="divider"></li>
                                            <li ><a href=<?php echo base_url."backend/log_out.php";?> >Log out</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class='app-body'>
                    <div class='app-content'>
                        <ng-view class="scrollable"></ng-view>
                    </div>
                </div>
            </div>


        </body>

        <?php echo '<script src="'.base_url.'lib/angular-1.4.6/angular-animate.min.js"></script>' ?>
            <?php echo '<script src="'.base_url.'lib/moment/moment.js"></script>' ?>
                <?php echo '<script src="'.base_url.'lib/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.js"></script>' ?>
                    <?php echo '<script src="'.base_url.'lib/angular-bootstrap/ui-bootstrap-tpls.js"></script>' ?>

                        <?php echo '<script src="'.base_url.'js/core/adminApp.js"></script>' ?>
    <?php echo '<script src="'.base_url.'js/factory/patientClass.js"></script>' ?>
    <?php echo '<script src="'.base_url.'js/factory/doctorClass.js"></script>' ?>
    <?php echo '<script src="'.base_url.'js/factory/nurseClass.js"></script>' ?>
    <?php echo '<script src="'.base_url.'js/factory/pharmacistClass.js"></script>' ?>
    <?php echo '<script src="'.base_url.'lib/sweet-alert/dist/sweetalert.min.js"></script>' ?>
    <?php echo '<script src="'.base_url.'js/factory/drugClass.js"></script>' ?>
    <?php echo '<script src="'.base_url.'js/factory/staffClass.js"></script>' ?>
    <?php echo '<script src="'.base_url.'js/factory/diseaseClass.js"></script>' ?>
    <?php echo '<script src="'.base_url.'js/factory/departmentClass.js"></script>' ?>
    <?php echo '<script src="'.base_url.'js/factory/sessionClass.js"></script>' ?>
                            <?php echo '<script src="'.base_url.'js/controller/appointmentForAdmin.js"></script>'; ?>

</html>
