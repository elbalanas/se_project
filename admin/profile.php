<div class="scrollable-header section" style="z-index:900">
    <div class="container section">
        <div class="row">
            <div class="col-md-12">
                <h2>Admin Profile</h2>
            </div>
        </div>
    </div>
</div>
<div class="scrollable-content" style="position: relative;overflow: hidden;">
    <div style="height:inherit">
        <div style="position:relative;height:90%">
            <div class="section scrollable">
                <div class="scrollable-content">
                    <div class="container" style="height:inherit">
                        <div class="row" style="height:inherit">
                            <div class="col-md-12" style="height:inherit">
                                <div class='row'>
                                    <div class='panel panel-info'>
                                        <div class='panel-heading'>
                                            General Information
                                        </div>
                                        <div class='section'>
                                            <br>
                                            <div class='row'>
                                                <div class="col-md-3 col-md-offset-1">
                                                    <img ng-src="../img/jack.jpg" class='img-rounded img-responsive' height="250" width="150" />
                                                </div>

                                                <div class='col-md-8'>
                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>First Name</label>
                                                            <div class="form-group">
                                                                <input type="text" ng-model='vm.profile.firstName' class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class='col-md-4'>
                                                            <label>Last Name</label>
                                                            <div class="form-group">
                                                                <input type="text" ng-model='vm.profile.lastName' class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class='col-md-3'>
                                                            <label>SSID</label>
                                                            <p>{{vm.profile.ssid}}</p>
                                                        </div>
                                                    </div>
                                                        <br>
                                                        <div class='row'>
                                                            <div class='col-md-4'>
                                                                <label>Telephone</label>
                                                                <div class="form-group">
                                                                    <input type="text" ng-model='vm.profile.telephone' class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class='row'>
                                                            <div class='col-md-8'>
                                                                <label>Address</label>
                                                                <div class="form-group">
                                                                    <input type="text" ng-model='vm.profile.address' class="form-control">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <br>
                                                        <div class='row'>
                                                            <div class='col-md-11'>
                                                                <div class='btn btn-primary pull-right' ng-click='vm.saveProfile()'>Save Profile</div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class='panel panel-info'>
                                        <div class='panel-heading'>
                                            Change Password
                                        </div>
                                        <div class='section'>
                                            <br>
                                            <div class='row'>
                                                <div class="col-md-6 col-md-offset-3">
                                                    <label for="InputPassword1">
                                                        New Password
                                                    </label>
                                                    <input type="password" class="form-control" id="InputPassword1" placeholder="New Password" name="password" ng-model="vm.newPassword">
                                                </div>
                                            </div>
                                            <br>
                                            <div class='row'>
                                                <div class='col-md-2 col-md-offset-7'>
                                                    <div class="btn btn-primary pull-left" ng-click="vm.changePassword()">Save Password</div>
                                                </div>
                                                
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
