<?php include("../inc/config.php"); ?>



    <div ui-yield-to="employeeModals"></div>
    <div ui-content-for="employeeModals">
        <div class="modal" ui-if="employeeModals" ui-state='employeeModals'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog" style='height:100%;'>
                <div class="modal-content" style='height:90%;'>
                    <div class="modal-header">
                        <button class="close" ui-turn-off="employeeModals">&times;</button>
                        <h4 class="modal-title">Employee</h4>
                    </div>

                    <div class="modal-body" style='height:85%; '>

                        <div class="container" style='height:100%; overflow:auto;'>
                            <div class="col-md-12" style='height:90%; overflow:auto;'>
                                <div>
                                    <div>
                                        <br>
                                        <div class='panel panel-info'>
                                            <div class='panel-heading' ng-show='vm.selectType!="staff"'>{{vm.capitalize(vm.selectType)}} <strong>{{vm.pState.firstName}} {{vm.pState.lastName}}</strong></div>
                                            <div class='panel-heading' ng-show='vm.selectType=="staff"'>{{vm.tranAdminFlag(vm.pState.adminFlag)}} <strong>{{vm.pState.firstName}} {{vm.pState.lastName}}</strong><i class="fa fa-user-secret fa-lg pull-right" ng-show='vm.pState.adminFlag == 1' style='color:red'></i></div>
                                            <br>
                                            <div class='row'>
                                                <div class='col-md-3 col-md-offset-1'>
                                                    <img src='../img/jack.jpg' class='img-rounded img-responsive' height="250" width="150">
                                                </div>
                                                <div class='col-md-8'>
                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>First Name</label>
                                                            <div class="form-group">
                                                                <input type="text" ng-model='vm.pState.firstName' class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class='col-md-4'>
                                                            <label>Last Name</label>
                                                            <div class="form-group">
                                                                <input type="text" ng-model='vm.pState.lastName' class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class='col-md-3'>
                                                            <label>SSID</label>
                                                            <div class="form-group">
                                                                <input type="text" ng-model='vm.pState.ssid' class="form-control" ng-disabled="vm.editing==1">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>Telephone</label>
                                                            <div class="form-group">
                                                                <input type="text" ng-model='vm.pState.telephone' class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-8'>
                                                            <label>Address</label>
                                                            <div class="form-group">
                                                                <input type="text" ng-model='vm.pState.address' class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class='row' ng-show='vm.selectType=="staff"&&vm.editing==1'>
                                                        <div class='col-md-4'>
                                                            <label>Authority</label>
                                                            <p>{{vm.tranAdminFlag(vm.pState.adminFlag)}}</p>
                                                        </div>
                                                        <div class=' btn btn-primary' ng-click='vm.changeAuthor()'>
                                                            <p>Change Authority</p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class='row' ng-show='vm.selectType=="doctor"'>
                                                        <div class='col-md-6'>
                                                            <label>Department</label>
                                                            <select class='form-control' ng-model='vm.pState.departmentNumber'>
                                                                <option value='{{department.departmentNumber}}' ng-repeat='department in vm.allDepartment'>{{department.departmentName}}</option>
                                                            </select>
                                                        </div>
                                                        <div class='col-md-3'>
                                                            <label>Department Number</label>
                                                            <p>{{vm.pState.departmentNumber}}</p>
                                                        </div>
                                                    </div>
                                                    <!--
                                                    <div class='row' ng-show='vm.selectType=="doctor"&&vm.editing==0'>
                                                        <div class='col-md-10'>
                                                            <label>Task</label>
                                                            <div class="checkbox" ng-repeat='i in getNumber(14)'>
                                                                <label>
                                                                    <input type="checkbox" ng-model='vm.pState.task[$index]'>{{$index+1}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
-->
                                                    <br>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer lg">

                    <button ui-turn-off="employeeModals" class="btn btn-default" ng-click='vm.clearState()'>Close</button>
                    <button ui-turn-on="deleteModals" ui-turn-off='employeeModals' ng-show='vm.editing==1' class="btn btn-danger" ng-click='vm.currentModal="employeeModals"'>Delete</button>
                    <button ui-turn-off="{{vm.modalControl}}" class="btn btn-primary" ng-click='vm.saveState()'>Save changes</button>
                </div>
            </div>
        </div>
    </div>




    <div ui-yield-to="drugModals"></div>
    <div ui-content-for="drugModals">
        <div class="modal" ui-if="drugModals" ui-state='drugModals'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog" style='height:100%;'>
                <div class="modal-content" style='height:90%;'>
                    <div class="modal-header">
                        <button class="close" ui-turn-off="drugModals">&times;</button>
                        <h4 class="modal-title">Drug</h4>
                    </div>

                    <div class="modal-body" style='height:85%; '>

                        <div class="container" style='height:100%; overflow:auto;'>
                            <div class="col-md-12" style='height:80%; overflow:auto;'>
                                <div>
                                    <div class='panel panel-info'>
                                        <div class='panel-heading'>Drug Information</div>
                                        <br>
                                        <div class='row'>
                                            <div class='col-md-10 col-md-offset-1'>
                                                <div class='row'>
                                                    <div class='col-md-6 col-md-offset-1'>
                                                        <label>Drug Name</label>
                                                        <div class="form-group">
                                                            <input type="text" ng-model='vm.pState.drugName' class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class='col-md-3 col-md-offset-1' ng-show='vm.editing==1'>
                                                        <label>Drug ID</label>
                                                        <p>{{vm.pState.drugId}}</p>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer lg">
                    <button ui-turn-off="drugModals" class="btn btn-default" ng-click='vm.clearState()'>Close</button>
                    <button ui-turn-on="deleteModals" ui-turn-off='drugModals' ng-show='vm.editing==1' class="btn btn-danger" ng-click='vm.currentModal="drugModals"'>Delete</button>
                    <button ui-turn-off="{{vm.modalControl}}" class="btn btn-primary" ng-click='vm.saveState()'>Save</button>
                </div>
            </div>

        </div>
    </div>

    <div ui-yield-to="diseaseModals"></div>
    <div ui-content-for="diseaseModals">
        <div class="modal" ui-if="diseaseModals" ui-state='diseaseModals'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog" style='height:100%;'>
                <div class="modal-content" style='height:90%;'>
                    <div class="modal-header">
                        <button class="close" ui-turn-off="diseaseModals">&times;</button>
                        <h4 class="modal-title">Disease</h4>
                    </div>

                    <div class="modal-body" style='height:85%; '>

                        <div class="container" style='height:100%; overflow:auto;'>
                            <div class="col-md-12" style='height:80%; overflow:auto;'>
                                <div>
                                    <br>
                                    <div>
                                        <div class='panel panel-info'>
                                            <div class='panel-heading'>Disease Information</div>
                                            <br>
                                            <div class='row'>
                                                <div class='col-md-10 col-md-offset-1'>
                                                    <div class='row'>
                                                        <div class='col-md-5 col-md-offset-1'>
                                                            <label>Disease Name</label>
                                                            <div class="form-group">
                                                                <input type="text" ng-model='vm.pState.diseaseName' class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class='col-md-4 col-md-offset-1' ng-show='vm.editing==1'>
                                                            <label>Disease ID</label>
                                                            <p>{{vm.pState.diseaseId}}</p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>



                                </div>



                            </div>
                        </div>



                    </div>

                </div>
                <div class="modal-footer lg">

                    <button ui-turn-off="diseaseModals" class="btn btn-default" ng-click='vm.clearState()'>Close</button>
                    <button ui-turn-on="deleteModals" ui-turn-off='diseaseModals' ng-show='vm.editing==1' class="btn btn-danger" ng-click='vm.currentModal="diseaseModals"'>Delete</button>
                    <button ui-turn-off="{{vm.modalControl}}" class="btn btn-primary" ng-click='vm.saveState()'>Save</button>
                </div>
            </div>

        </div>
    </div>

    <div ui-yield-to="departmentModals"></div>
    <div ui-content-for="departmentModals">
        <div class="modal" ui-if="departmentModals" ui-state='departmentModals'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog" style='height:100%;'>
                <div class="modal-content" style='height:90%;'>
                    <div class="modal-header">
                        <button class="close" ui-turn-off="departmentModals">&times;</button>
                        <h4 class="modal-title">Department</h4>
                    </div>

                    <div class="modal-body" style='height:85%; '>

                        <div class="container" style='height:100%; overflow:auto;'>
                            <div class="col-md-12" style='height:80%; overflow:auto;'>
                                <div>
                                    <br>
                                    <div>
                                        <div class='panel panel-info'>
                                            <div class='panel-heading'>Department Information</div>
                                            <br>
                                            <div class='row'>
                                                <div class='col-md-10 col-md-offset-1'>
                                                    <div class='row'>
                                                        <div class='col-md-6 col-md-offset-1'>
                                                            <label>Department Name</label>
                                                            <div class="form-group">
                                                                <input type="text" ng-model='vm.pState.departmentName' class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class='col-md-4 col-md-offset-1' ng-show='vm.editing==1'>
                                                            <label>Department Number</label>
                                                            <p>{{vm.pState.departmentNumber}}</p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer lg">

                    <button ui-turn-off="departmentModals" class="btn btn-default" ng-click='vm.clearState()'>Close</button>
                    <button ui-turn-on="deleteModals" ui-turn-off='departmentModals' ng-show='vm.editing==1' class="btn btn-danger" ng-click='vm.currentModal="departmentModals"'>Delete</button>
                    <button ui-turn-off="{{vm.modalControl}}" class="btn btn-primary" ng-click='vm.saveState()'>Save</button>
                </div>
            </div>

        </div>
    </div>

    <div ui-yield-to="deleteModals"></div>
    <div ui-content-for="deleteModals">
        <div class="modal" ui-if="deleteModals" ui-state='deleteModals'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">


                        <h4 class="modal-title" style="color:red">Warning</h4>

                    </div>
                    <div class="modal-body">
                        You can undo this action.Are you sure to continute?

                    </div>
                    <div class="modal-footer">

                        <button ui-turn-off="deleteModals" ui-turn-on='{{vm.currentModal}}' class="btn btn-default">Cancel</button>
                        <button ui-turn-off="deleteModals" class="btn btn-danger" ng-click="vm.delState()">Continute</button>

                    </div>
                </div>
            </div>
        </div>
    </div>





    <div class="scrollable-header section" style="z-index:900">
        <div class="container section">
            <div class="row">
                <div class="col-md-12">
                    <h2>My Dashboard</h2>
                </div>
            </div>
        </div>
    </div>


    <div class="scrollable-content" style="position: relative;overflow: hidden;">


        <div ui-state="activeTab" default="1" style="height:inherit">

            <ul class="nav nav-tabs container">

                <li ui-class="{'active': activeTab == 1}" ng-click='vm.selectType="doctor";vm.changeType()'>
                    <a ui-set="{'activeTab': 1}">Employee</a>
                </li>
                <!--
                <li ui-class="{'active': activeTab == 2}" ng-click='vm.selectType="patient";vm.changeType()'>
                    <a ui-set="{'activeTab': 2}">Patient</a>
                </li>
-->
                <li ui-class="{'active': activeTab == 3}" ng-click='vm.selectType="drug";vm.changeType()'>
                    <a ui-set="{'activeTab': 3}">Drug</a>
                </li>
                <li ui-class="{'active': activeTab == 4}" ng-click='vm.selectType="disease";vm.changeType()'>
                    <a ui-set="{'activeTab': 4}">Disease</a>
                </li>
                <li ui-class="{'active': activeTab == 5}" ng-click='vm.selectType="department";vm.changeType()'>
                    <a ui-set="{'activeTab': 5}">Department</a>
                </li>
            </ul>
            <div style="position:relative;height:90%">
                <div class="section scrollable" ui-if="activeTab == 1">
                    <div class="scrollable-content">
                        <div class="container" style="height:inherit">

                            <div class="row" style="height:inherit">
                                <div class="col-md-12" style="height:inherit">
                                    <h3 class="page-header">Welcome <small>{{vm.aStaff.firstName}} {{vm.aStaff.lastName}}</small></h3>
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="input-group">
                                                <select class="form-control" ng-model='vm.selectType' ng-change='vm.changeType()'>
                                                    <option value='doctor'>Doctor</option>
                                                    <option value='nurse'>Nurse</option>
                                                    <!--                                                    <option value='Patient'>Patient</option>-->
                                                    <option value='pharmacist'>Pharmacist</option>
                                                    <option value='staff'>Staff</option>

                                                </select>
                                                <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>

                                                <input type="text" class="form-control" ng-model='vm.searchArg' placeholder="ex.First_Name Last_name " aria-describedby="sizing-addon1">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="btn btn-floating pull-right" ui-turn-on="employeeModals" ng-click="vm.editing=0;"><i class="fa fa-plus"></i></div>
                                        </div>
                                    </div>
                                    <div ng-show="true">
                                        <br>
                                        <h4 class="page-header" style="display:inline">Result List</h4>
                                        <h5 class='text-center' ng-show='vm.listIsEmpty'><strong >Search Not Found</strong></h5>
                                        <table class="table table-striped" ng-hide='vm.listIsEmpty'>
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>SSID</th>
                                                    <th ng-show='vm.selectType == "staff"'>Status</th>
                                                    <!--
                                                    <th>Doctor</th>
                                                    <th>Department</th>
                                                    <th>SSID</th>
                                                    <th>ID</th>
                                                    <th>Status</th>
-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="content-row" ui-turn-on='employeeModals' ng-click="vm.setState(person);vm.editing=1;" ng-repeat='person in vm.focusObject' ng-show='vm.employeeSearch(person)'>
                                                    <th scope="row">{{$index+1}}</th>
                                                    <td>{{person.firstName}}</td>
                                                    <td>{{person.lastName}}</td>
                                                    <td>{{person.ssid}}</td>
                                                    <td ng-show='vm.selectType == "staff"'>{{vm.tranAdminFlag(person.adminFlag)}}</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--
                <div class="section scrollable" ui-if="activeTab == 2">
                    <div class="scrollable-content">
                        <div class="container" style="height:inherit">

                            <div class="row" style="height:inherit">
                                <div class="col-md-12" style="height:inherit">
                                    <h3 class="page-header">Welcome <small>{{vm.aStaff.firstName}} {{vm.aStaff.lastName}}</small></h3>
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>
                                                <input type="text" class="form-control" ng-model='vm.searchArg' ng-change='vm.patientSearch()' placeholder="ex.First_Name Last_name " aria-describedby="sizing-addon1">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="btn btn-floating pull-right" ui-turn-on="petientModals" ng-click="vm.editing=0;"><i class="fa fa-plus"></i></div>
                                        </div>
                                    </div>
                                    <div ng-show="true">
                                        <br>
                                        <h4 class="page-header" style="display:inline">Result List</h4>
                                        <h5 class='text-center' ng-show='vm.listIsEmpty'><strong >Search Not Found</strong></h5>
                                        <table class="table table-striped" ng-hide='vm.listIsEmpty'>
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="content-row" ui-turn-on='patientModals' ng-click="vm.setState(object);vm.editing=1;" ng-repeat='person in vm.focusObject' ng-show=''>

                                                    <th scope="row">{{$index+1}}</th>
                                                    <td>{{person.firstName}}</td>
                                                    <td>{{person.lastName}}</td>
                                                    
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
-->

                <div class="section scrollable" ui-if="activeTab == 3">
                    <div class="scrollable-content">
                        <div class="container" style="height:inherit">

                            <div class="row" style="height:inherit">
                                <div class="col-md-12" style="height:inherit">



                                    <h3 class="page-header">Welcome <small>{{vm.aStaff.firstName}} {{vm.aStaff.lastName}}</small></h3>
                                    <!--
                            <h3 ng-show='vm.hasApp'>You have Appointment Today at {{vm.startTime}} - {{vm.finTime}}.</h3>
                            <h4 ng-hide='vm.hasApp'>You don't have Appointment Today. Your next Appointment is <span style='font-weight:bold;'>{{vm.Fdata.date}}</span> at {{vm.startTime}} - {{vm.finTime}}.</h4>
-->
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="input-group">

                                                <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>

                                                <input type="text" class="form-control" ng-model='vm.searchArg' placeholder="ex.Name Drug" aria-describedby="sizing-addon1">
                                            </div>

                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-md-8">
                                            <div class="btn btn-floating pull-right" ui-turn-on="drugModals" ng-click="vm.editing=0;"><i class="fa fa-plus"></i></div>
                                        </div>
                                    </div>
                                    <div ng-show="true">
                                        <br>
                                        <h4 class="page-header" style="display:inline">Result List</h4>
                                        <h5 class='text-center' ng-show='vm.listIsEmpty'><strong >Search Not Found</strong></h5>
                                        <table class="table table-striped" ng-hide='vm.listIsEmpty'>
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Drug Name</th>
                                                    <th>Drug ID</th>
                                                    <!--
                                                    <th>Doctor</th>
                                                    <th>Department</th>
                                                    <th>SSID</th>
                                                    <th>ID</th>
                                                    <th>Status</th>
-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="content-row" ui-turn-on='drugModals' ng-click="vm.setState(object);vm.editing=1;" ng-repeat='object in vm.focusObject' ng-show='vm.drugSearch(object)'>

                                                    <th scope="row">{{$index+1}}</th>
                                                    <td>{{object.drugName}}</td>
                                                    <td>{{object.drugId}}</td>
                                                    <!--
                                                    <td>{{person.doctor}}</td>
                                                    <td>{{person.department}}</td>
                                                    <td>{{person.ssid}}</td>
                                                    <td>{{person.id}}</td>
                                                    <td>
                                                        <i class='fa fa-check fa-lg' style='color:#00fa00;' ng-show='vm.checkStatus(person)'></i>
                                                        <i class='fa fa-check fa-lg' style='color:#ddd;' ng-hide='vm.checkStatus(person)'></i>

                                                    </td>
-->
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section scrollable" ui-if="activeTab == 4">
                    <div class="scrollable-content">
                        <div class="container" style="height:inherit">

                            <div class="row" style="height:inherit">
                                <div class="col-md-12" style="height:inherit">



                                    <h3 class="page-header">Welcome <small>{{vm.aStaff.firstName}} {{vm.aStaff.lastName}}</small></h3>
                                    <!--
                            <h3 ng-show='vm.hasApp'>You have Appointment Today at {{vm.startTime}} - {{vm.finTime}}.</h3>
                            <h4 ng-hide='vm.hasApp'>You don't have Appointment Today. Your next Appointment is <span style='font-weight:bold;'>{{vm.Fdata.date}}</span> at {{vm.startTime}} - {{vm.finTime}}.</h4>
-->
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="input-group">

                                                <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>

                                                <input type="text" class="form-control" ng-model='vm.searchArg' placeholder="ex.Name Disease" aria-describedby="sizing-addon1">
                                            </div>

                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-md-8">
                                            <div class="btn btn-floating pull-right" ui-turn-on="diseaseModals" ng-click="vm.editing=0;"><i class="fa fa-plus"></i></div>
                                        </div>
                                    </div>
                                    <div ng-show="true">
                                        <br>
                                        <h4 class="page-header" style="display:inline">Result List</h4>
                                        <h5 class='text-center' ng-show='vm.listIsEmpty'><strong >Search Not Found</strong></h5>
                                        <table class="table table-striped" ng-hide='vm.listIsEmpty'>
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Disease Name</th>
                                                    <th>Disease ID</th>
                                                    <!--
                                                    <th>Doctor</th>
                                                    <th>Department</th>
                                                    <th>SSID</th>
                                                    <th>ID</th>
                                                    <th>Status</th>
-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="content-row" ui-turn-on='diseaseModals' ng-click="vm.setState(object);vm.editing=1;" ng-repeat='object in vm.focusObject' ng-show='vm.diseaseSearch(object)'>

                                                    <th scope="row">{{$index+1}}</th>
                                                    <td>{{object.diseaseName}}</td>
                                                    <td>{{object.diseaseId}}</td>
                                                    <!--
                                                    <td>{{person.doctor}}</td>
                                                    <td>{{person.department}}</td>
                                                    <td>{{person.ssid}}</td>
                                                    <td>{{person.id}}</td>
                                                    <td>
                                                        <i class='fa fa-check fa-lg' style='color:#00fa00;' ng-show='vm.checkStatus(person)'></i>
                                                        <i class='fa fa-check fa-lg' style='color:#ddd;' ng-hide='vm.checkStatus(person)'></i>

                                                    </td>
-->
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section scrollable" ui-if="activeTab == 5">
                    <div class="scrollable-content">
                        <div class="container" style="height:inherit">

                            <div class="row" style="height:inherit">
                                <div class="col-md-12" style="height:inherit">



                                    <h3 class="page-header">Welcome <small>{{vm.aStaff.firstName}} {{vm.aStaff.lastName}}</small></h3>
                                    <!--
                            <h3 ng-show='vm.hasApp'>You have Appointment Today at {{vm.startTime}} - {{vm.finTime}}.</h3>
                            <h4 ng-hide='vm.hasApp'>You don't have Appointment Today. Your next Appointment is <span style='font-weight:bold;'>{{vm.Fdata.date}}</span> at {{vm.startTime}} - {{vm.finTime}}.</h4>
-->
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="input-group">
                                                <!--
                                                <select class="form-control" ng-model='vm.selectType' ng-change='vm.changeType()'>
                                                    <option value='doctor'>Doctor</option>
                                                    <option value='nurse'>Nurse</option>
                                                    <option value='Patient'>Patient</option>
                                                    <option value='pharmacist'>Pharmacist</option>
                                                    <option value='staff'>Staff</option>

                                                </select>
-->
                                                <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>

                                                <input type="text" class="form-control" ng-model='vm.searchArg' placeholder="ex.DepartmentName , DepartmentNumber" aria-describedby="sizing-addon1">
                                            </div>

                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-md-8">
                                            <div class="btn btn-floating pull-right" ui-turn-on="departmentModals" ng-click="vm.editing=0;"><i class="fa fa-plus"></i></div>
                                        </div>
                                    </div>
                                    <div ng-show="true">
                                        <br>
                                        <h4 class="page-header" style="display:inline">Result List</h4>
                                        <h5 class='text-center' ng-show='vm.listIsEmpty'><strong >Search Not Found</strong></h5>
                                        <table class="table table-striped" ng-hide='vm.listIsEmpty'>
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Department Name</th>
                                                    <th>Department Number</th>
                                                    <!--
                                                    <th>Doctor</th>
                                                    <th>Department</th>
                                                    <th>SSID</th>
                                                    <th>ID</th>
                                                    <th>Status</th>
-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="content-row" ui-turn-on='departmentModals' ng-click="vm.setState(object);vm.editing=1;" ng-repeat='object in vm.focusObject' ng-show='vm.departmentSearch(object)'>

                                                    <th scope="row">{{$index+1}}</th>
                                                    <td>{{object.departmentName}}</td>
                                                    <td>{{object.departmentNumber}}</td>
                                                    <!--
                                                    <td>{{person.doctor}}</td>
                                                    <td>{{person.department}}</td>
                                                    <td>{{person.ssid}}</td>
                                                    <td>{{person.id}}</td>
                                                    <td>
                                                        <i class='fa fa-check fa-lg' style='color:#00fa00;' ng-show='vm.checkStatus(person)'></i>
                                                        <i class='fa fa-check fa-lg' style='color:#ddd;' ng-hide='vm.checkStatus(person)'></i>

                                                    </td>
-->
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
