<?php
    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];

        //OK
        if($_POST['action']=="getAllTodayAppointment"){
            getAllTodayAppointment();
        }
        //OK
        else if($_POST['action']=="getPatientAppointById"){
            getPatientAppointById($_POST['patientId']);
        }
        else if($_POST['action']=="getBestAppointmentByDepartment"){
            getBestAppointmentByDepartment($_POST['departmentNumber']);
        }
        else if($_POST['action']=="getBestAppointmentByDoctor"){
            getBestAppointmentByDoctor($_POST['departmentNumber'],$_POST['doctorId']);
        }
        else if($_POST['action']=="deleteAppointmentById"){
            deleteAppointmentById($_POST['appointmentId']);
        }
        else if($_POST['action']=="updateAppointment"){
            updateAppointment($_POST['aAppointment']);

        }
//        else if($_POST['action']=="getBestAppointmentByDoctor"){
//            getBestAppointmentByDoctorForDepartment($_POST['departmentNumber'],$_POST['doctorId']);
//
//        }
        //OK
        else if($_POST['action']=="getDoctorAppointmentById"){
            getDoctorAppointmentById($_POST['doctorId'],$_POST['date']);
        }
        else if($_POST['action']=="getAvailableAppointment"){
           // echo "in";
            getAvailableAppointment($_POST['departmentId'], $_POST['doctorId'], $_POST['month'], $_POST['year']);
        }
        else if($_POST['action']=="addAvailableDate"){
           // echo "in";
            addAvailableDate($_POST['doctorId'], $_POST['date'], $_POST['period']);
        }
        else if($_POST['action']=="deleteAvailableDate"){
           // echo "in";
            deleteAvailableDate($_POST['doctorId'], $_POST['date'], $_POST['period']);
        }
        else if($_POST['action']=="addUnavailableDate"){
           // echo "in";
            addUnavailableDate($_POST['doctorId'], $_POST['date'], $_POST['period']);
        }
        else if($_POST['action']=="deleteUnavailableDate"){
           // echo "in";
            deleteUnavailableDate($_POST['doctorId'], $_POST['date'], $_POST['period']);
        }




    }






    function getAllTodayAppointment() {
		$conn = connectToDatabase();
    	//$dateToday = date("Y-m-d");
       // echo $dateToday;
		$sql = "SELECT
					hospital.appointment_form.appointment_id AS appointmentId,
					hospital.appointment_form.patient_id AS patientId,
					hospital.patient.firstname AS patientFirstName,
					hospital.patient.lastname AS patientLastName,
					hospital.patient.ssid AS patientSsid,
					hospital.appointment_form.cause AS cause,
					hospital.appointment_form.staff_id AS staffId,
					hospital.staff.firstname AS staffFirstName,
					hospital.staff.lastname AS staffLastname,
					hospital.appointment_form.status AS status,
					hospital.appointment_form.dno AS departmentNumber,
					hospital.department.department_name AS departmentName,
					hospital.appointment_form.doctor_id AS doctorId,
					hospital.doctor.firstname AS doctorFirstName,
					hospital.doctor.lastname AS doctorLastName,
					hospital.appointment_form.type AS type,
					hospital.appointment_form.date AS date,
					hospital.appointment_form.period AS period
				FROM hospital.appointment_form
					JOIN hospital.patient
						ON hospital.appointment_form.patient_id = hospital.patient.patient_id
					LEFT JOIN hospital.staff
						ON hospital.appointment_form.staff_id = hospital.staff.staff_id
					LEFT JOIN hospital.doctor
						ON hospital.appointment_form.doctor_id = hospital.doctor.doctor_id
					JOIN hospital.department
						ON hospital.appointment_form.dno = hospital.department.dno
				WHERE hospital.appointment_form.date = CURDATE()
				ORDER BY hospital.appointment_form.time";

		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}

function getPatientAppointById($patientId) {
		$dateToday = date("Y-m-d");
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.appointment_form.appointment_id AS appointmentId,
					hospital.appointment_form.patient_id AS patientId,
					hospital.patient.firstname AS patientFirstName,
					hospital.patient.lastname AS patientLastname,
					hospital.patient.ssid AS patientSsid,
					hospital.appointment_form.cause AS cause,
					hospital.appointment_form.staff_id AS staffId,
					hospital.staff.firstname AS staffFirstName,
					hospital.staff.lastname AS staffLastname,
					hospital.appointment_form.status AS status,
					hospital.appointment_form.dno AS departmentNumber,
					hospital.department.department_name AS departmentName,
					hospital.appointment_form.doctor_id AS doctorId,
					hospital.doctor.firstname AS doctorFirstName,
					hospital.doctor.lastname AS doctorLastName,
					hospital.appointment_form.type AS type,
					hospital.appointment_form.date AS date,
					hospital.appointment_form.period AS period
				FROM hospital.appointment_form
					JOIN hospital.patient
						ON hospital.appointment_form.patient_id = hospital.patient.patient_id
					LEFT JOIN hospital.staff
						ON hospital.appointment_form.staff_id = hospital.staff.staff_id
					LEFT JOIN hospital.doctor
						ON hospital.appointment_form.doctor_id = hospital.doctor.doctor_id
					JOIN hospital.department
						ON hospital.appointment_form.dno = hospital.department.dno
				WHERE hospital.appointment_form.patient_id = " . "'" . $patientId . "' " . "
				AND hospital.appointment_form.date >= " . "'" . $dateToday . "'" . "
				ORDER BY hospital.appointment_form.date";
		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}


	function getBestAppointmentByDepartment($departmentNumber) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.doctor
				WHERE hospital.doctor.dno = " . "'" . $departmentNumber . "'";
		$result = $conn->query($sql);
		$conn->close();
		$numberOfRow = $result->num_rows;
		$bestDate = "9999-99-99";
		$bestDepartment = array("departmentNumber" => $departmentNumber, "doctorId" => "", "date" => $bestDate, "period" => "99");
		for($i = 0;$i < $numberOfRow;$i++) {
			$bestDoctor = getBestAppointmentByDoctorForDepartment($departmentNumber, $result->fetch_assoc()["doctor_id"]);
			if(strcmp($bestDate, $bestDoctor["date"]) > 0) {
				$bestDate = $bestDoctor["date"];
				$bestDepartment["doctorId"] = $bestDoctor["doctorId"];
				$bestDepartment["date"] = $bestDoctor["date"];
				$bestDepartment["period"] = $bestDoctor["period"];
			}
		}
		echo json_encode($bestDepartment);
	}

    function getBestAppointmentByDoctor($departmentNumber, $doctorId) {
            $conn = connectToDatabase();

            //Get best from routine
            $sql = "SELECT
                        *
                    FROM hospital.doctor
                    WHERE hospital.doctor.doctor_id = " . "'" . $doctorId . "'";
            $result = $conn->query($sql);
            $output = $result->fetch_assoc();
            $task = array($output["su_m"], $output["su_a"],
                        $output["mo_m"], $output["mo_a"],
                        $output["tu_m"], $output["tu_a"],
                        $output["we_m"], $output["we_a"],
                        $output["th_m"], $output["th_a"],
                        $output["fr_m"], $output["fr_a"],
                        $output["sa_m"], $output["sa_a"]);

            $year = date("Y");
            $month = date("m");
            $date = date("d")+1;
            $bestRoutine = "9999-99-99";
            $bestPeriod = "9";
            $dayFormat = $year . "-" . $month . "-" . $date;
            for($i = 0;$i < 365;$i++) {
                $day = date('w', strtotime($dayFormat));
                if($task[$day * 2] == 1) {
                    if(!unavailable($dayFormat, "0", $doctorId) AND !isFull($dayFormat, "0", $doctorId)) {
                        $bestRoutine = $dayFormat;
                        $bestPeriod = "0";
                        break;
                    }
                }
                if($task[($day * 2) + 1] == 1) {
                    if(!unavailable($dayFormat, "1", $doctorId) AND !isFull($dayFormat, "1", $doctorId)) {
                        $bestRoutine = $dayFormat;
                        $bestPeriod = "1";
                        break;
                    }
                }
                $dayFormat = tomorrow($dayFormat);
            }
            //--------------------

            //Get best from available date
            $dayFormat = $year . "-" . $month . "-" . $date;
            $sql = "SELECT
                        *
                    FROM hospital.available_date
                    WHERE hospital.available_date.doctor_id = " . "'" . $doctorId . "'" . "
                    AND hospital.available_date.date >= " . "'" . $dayFormat . "'" . "
                    ORDER BY hospital.available_date.date";
            $result = $conn->query($sql);
            
            $bestAvailableDate = "9999-99-99";
            $bestAvailablePeriod = "9";
            if($result->num_rows >0){
                $numberOfAvailable = $result->num_rows;
                for($i = 0;$i < $numberOfAvailable;$i++) {
                    $sql = "SELECT
                                *
                            FROM hospital.available_date
                            WHERE hospital.available_date.doctor_id = " . "'" . $doctorId . "'" . "
                            AND hospital.available_date.date >= " . "'" . $dayFormat . "'" . "
                            ORDER BY hospital.available_date.date
                            LIMIT 1 OFFSET " . $i;
                    $result = $conn->query($sql);
                    $available_date = $result->fetch_assoc();
                    $availableDate = $available_date["date"];
                    $availablePeriod = $available_date["period"];
                    if(!isFull($availableDate, $availablePeriod, $doctorId)) {
                        $bestAvailableDate = $availableDate;
                        $bestAvailablePeriod = $availablePeriod;
                        break;
                    }
                }
            }
            //--------------------

            if(strcmp($bestRoutine, $bestAvailableDate) > 0) {
                //bestAvailable
                $bestAppointment = array("departmentNumber" => $departmentNumber, "doctorId" => $doctorId, "date" => $bestAvailableDate, "period" => $bestAvailablePeriod);
            } else {
                //bestRoutine
                $bestAppointment = array("departmentNumber" => $departmentNumber, "doctorId" => $doctorId, "date" => $bestRoutine, "period" => $bestPeriod);
            }

            $conn->close();
            echo json_encode($bestAppointment);
        }

    function getBestAppointmentByDoctorForDepartment($departmentNumber, $doctorId) {
		$conn = connectToDatabase();

		//Get best from routine
		$sql = "SELECT
					*
				FROM hospital.doctor
				WHERE hospital.doctor.doctor_id = " . "'" . $doctorId . "'";
		$result = $conn->query($sql);
		$output = $result->fetch_assoc();
		$task = array($output["su_m"], $output["su_a"],
					$output["mo_m"], $output["mo_a"],
					$output["tu_m"], $output["tu_a"],
					$output["we_m"], $output["we_a"],
					$output["th_m"], $output["th_a"],
					$output["fr_m"], $output["fr_a"],
					$output["sa_m"], $output["sa_a"]);

		$year = date("Y");
		$month = date("m");
		$date = date("d")+1;
		$bestRoutine = "9999-99-99";
		$bestPeriod = "9";
		$dayFormat = $year . "-" . $month . "-" . $date;
		for($i = 0;$i < 365;$i++) {
			$day = date('w', strtotime($dayFormat));
			if($task[$day * 2] == 1) {
				if(!unavailable($dayFormat, "0", $doctorId) AND !isFull($dayFormat, "0", $doctorId)) {
					$bestRoutine = $dayFormat;
					$bestPeriod = "0";
					break;
				}
			}
			if($task[($day * 2) + 1] == 1) {
				if(!unavailable($dayFormat, "1", $doctorId) AND !isFull($dayFormat, "1", $doctorId)) {
					$bestRoutine = $dayFormat;
					$bestPeriod = "1";
					break;
				}
			}
			$dayFormat = tomorrow($dayFormat);
		}
		//--------------------

		//Get best from available date
        $dayFormat = $year . "-" . $month . "-" . $date;
		$sql = "SELECT
					*
				FROM hospital.available_date
				WHERE hospital.available_date.doctor_id = " . "'" . $doctorId . "'" . "
                AND hospital.available_date.date >= " . "'" . $dayFormat . "'" . "
				ORDER BY hospital.available_date.date";
		$result = $conn->query($sql);
		
		$bestAvailableDate = "9999-99-99";
		$bestAvailablePeriod = "9";
        if($result->num_rows >0){
            $numberOfAvailable = $result->num_rows;
            for($i = 0;$i < $numberOfAvailable;$i++) {
                $sql = "SELECT
                            *
                        FROM hospital.available_date
                        WHERE hospital.available_date.doctor_id = " . "'" . $doctorId . "'" . "
                        AND hospital.available_date.date >= " . "'" . $dayFormat . "'" . "
                        ORDER BY hospital.available_date.date
                        LIMIT 1 OFFSET " . $i;
                $result = $conn->query($sql);
                $available_date = $result->fetch_assoc();
                $availableDate = $available_date["date"];
                $availablePeriod = $available_date["period"];
                if(!isFull($availableDate, $availablePeriod, $doctorId)) {
                    $bestAvailableDate = $availableDate;
                    $bestAvailablePeriod = $availablePeriod;
                    break;
                }
            }
        }
		//--------------------

		if(strcmp($bestRoutine, $bestAvailableDate) > 0) {
			//bestAvailable
			$bestAppointment = array("departmentNumber" => $departmentNumber, "doctorId" => $doctorId, "date" => $bestAvailableDate, "period" => $bestAvailablePeriod);
		} else {
			//bestRoutine
			$bestAppointment = array("departmentNumber" => $departmentNumber, "doctorId" => $doctorId, "date" => $bestRoutine, "period" => $bestPeriod);
		}

		$conn->close();
		return $bestAppointment;
	}


	function deleteAppointmentById($appointmentId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.appointment_form
				WHERE hospital.appointment_form.appointment_id = " . "'" . $appointmentId . "'";
		$result = $conn->query($sql);
		$conn->close();
	}


	function updateAppointment($appointment) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.appointment_form
				WHERE hospital.appointment_form.appointment_id = " . "'" . $appointment["appointmentId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
//			$sql = "UPDATE hospital.appointment_form
//					SET hospital.appointment_form.patient_id = " . "'" . $appointment["patientId"] . "'" . "," . "
//						hospital.appointment_form.patient_ssid = " . "'" . $appointment["patientSsid"] . "'" . "," . "
//						hospital.appointment_form.cause = " . "'" . $appointment["cause"] . "'" . "," . "
//						hospital.appointment_form.abnormality = " . "'" . $appointment["abnormality"] . "'" . "," . "
//						hospital.appointment_form.staff_id = " . "'" . $appointment["staffId"] . "'" . "," . "
//						hospital.appointment_form.status = " . "'" . $appointment["status"] . "'" . "," . "
//						hospital.appointment_form.dno = " . "'" . $appointment["departmentNumber"] . "'" . "," . "
//						hospital.appointment_form.doctor_id = " . "'" . $appointment["doctorId"] . "'" . "," . "
//						hospital.appointment_form.type = " . "'" . $appointment["type"] . "'" . "," . "
//						hospital.appointment_form.date = " . "'" . $appointment["date"] . "'" . "," . "
//						hospital.appointment_form.period = " . "'" . $appointment["period"] . "'" . "," . "
//						hospital.appointment_form.time = " . "'" . $appointment["time"] . "'" . "
//					WHERE hospital.appointment_form.appointment_id = " . "'" . $appointment["appointmentId"] . "'";
                $sql = "UPDATE hospital.appointment_form
					SET hospital.appointment_form.patient_id = " . "'" . $appointment["patientId"] . "'" . "," . "
						hospital.appointment_form.patient_ssid = " . "'" . $appointment["patientSsid"] . "'" . "," . "
						hospital.appointment_form.cause = " . "'" . $appointment["cause"] . "'" . "," . "

						hospital.appointment_form.staff_id = " . "'" . $appointment["staffId"] . "'" . "," . "
						hospital.appointment_form.status = " . "'" . $appointment["status"] . "'" . "," . "
						hospital.appointment_form.dno = " . "'" . $appointment["departmentNumber"] . "'" . "," . "
						hospital.appointment_form.doctor_id = " . "'" . $appointment["doctorId"] . "'" . "," . "
						hospital.appointment_form.type = " . "'" . $appointment["type"] . "'" . "," . "
						hospital.appointment_form.date = " . "'" . $appointment["date"] . "'" . "," . "
						hospital.appointment_form.period = " . "'" . $appointment["period"] . "'" . "," . "
						hospital.appointment_form.time = " . "'" . date("Y-m-d H:i:s") . "'" . "
					WHERE hospital.appointment_form.appointment_id = " . "'" . $appointment["appointmentId"] . "'";
			$conn->query($sql);
		} else {
			$sql = "SELECT
						hospital.appointment_form.appointment_id
					FROM hospital.appointment_form
					ORDER BY hospital.appointment_form.appointment_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["appointment_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.appointment_form (appointment_id, patient_id, patient_ssid, cause, abnormality, staff_id, status, dno, doctor_id, type, date, period, time)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $appointment["patientId"] . "'" . "," .
								"'" . $appointment["patientSsid"] . "'" . "," .
								"'" . $appointment["cause"] . "'" . "," .
								"'" . $appointment["abnormality"] . "'" . "," .
								"'" . $appointment["staffId"] . "'" . "," .
								"'" . $appointment["status"] . "'" . "," .
								"'" . $appointment["departmentNumber"] . "'" . "," .
								"'" . $appointment["doctorId"] . "'" . "," .
								"'" . $appointment["type"] . "'" . "," .
								"'" . $appointment["date"] . "'" . "," .
								"'" . $appointment["period"] . "'" . "," .
								"'" . $appointment["time"] . "'" . ")";
			$conn->query($sql);
		}

		$conn->close();
	}


	function getDoctorAppointmentById($doctorId, $date) {
		$conn = connectToDatabase();


        $date = date('Y-m-d',strtotime($date));
        //echo $date;
		$sql = "SELECT
					hospital.appointment_form.appointment_id AS appointmentId,
					hospital.appointment_form.patient_id AS patientId,
					hospital.patient.firstname AS patientFirstName,
					hospital.patient.lastname AS patientLastName,
					hospital.patient.ssid AS patientSsid,
					hospital.appointment_form.cause AS cause,
					hospital.appointment_form.staff_id AS staffId,
					hospital.staff.firstname AS staffFirstName,
					hospital.staff.lastname AS staffLastname,
					hospital.appointment_form.status AS status,
					hospital.appointment_form.dno AS departmentNumber,
					hospital.department.department_name AS departmentName,
					hospital.appointment_form.doctor_id AS doctorId,
					hospital.doctor.firstname AS doctorFirstName,
					hospital.doctor.lastname AS doctorLastName,
					hospital.appointment_form.type AS type,
					hospital.appointment_form.date AS date,
					hospital.appointment_form.period AS period
				FROM hospital.appointment_form
					JOIN hospital.patient
						ON hospital.appointment_form.patient_id = hospital.patient.patient_id
					LEFT JOIN hospital.staff
						ON hospital.appointment_form.staff_id = hospital.staff.staff_id
					LEFT JOIN hospital.doctor
						ON hospital.appointment_form.doctor_id = hospital.doctor.doctor_id
					JOIN hospital.department
						ON hospital.appointment_form.dno = hospital.department.dno
				WHERE hospital.appointment_form.doctor_id = " . "'" . $doctorId . "' " . "
					AND hospital.appointment_form.date = " . "'" .  $date . "'";
		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}



	

    function getAvailableAppointment($departmentId, $doctorId, $month, $year) {

            $dayOfMonth = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
            $status = array();
            $conn = connectToDatabase();
            $sql = "SELECT
                        *
                    FROM hospital.doctor
                    WHERE hospital.doctor.doctor_id = " . "'" . $doctorId . "'";
            $result = $conn->query($sql);
            $output = $result->fetch_assoc();
            $task = array($output["su_m"], $output["su_a"],
                        $output["mo_m"], $output["mo_a"],
                        $output["tu_m"], $output["tu_a"],
                        $output["we_m"], $output["we_a"],
                        $output["th_m"], $output["th_a"],
                        $output["fr_m"], $output["fr_a"],
                        $output["sa_m"], $output["sa_a"]);

            for($i = 0;$i < $dayOfMonth[$month - 1];$i++) {
                $dateFormat = $year . "-" . $month . "-" . ($i+1);
                $day = date("w",strtotime($dateFormat));

                //Period 0
                if((unavailable($dateFormat, "0", $doctorId) OR $task[$day*2] == 0) AND !available($dateFormat, "0", $doctorId)) {
                    $status[$i*2] = array("numberOfAppointment" => "0", "status" => "0");
                } else {
                    $sql = "SELECT
                                *
                            FROM hospital.appointment_form
                            WHERE hospital.appointment_form.date = " . "'" . $dateFormat . "'" . "
                            AND hospital.appointment_form.doctor_id = " . "'" . $doctorId . "'" . "
                            AND hospital.appointment_form.period = " . "'" . "0" . "'";
                    $result = $conn->query($sql);
                    $status[$i*2] = array("numberOfAppointment" => $result->num_rows, "status" => "1");
                }

                //Period 1
                if((unavailable($dateFormat, "1", $doctorId) OR $task[($day*2) + 1] == 0) AND !available($dateFormat, "1", $doctorId)) {
                    $status[($i*2) + 1] = array("numberOfAppointment" => "0", "status" => "0");
                } else {
                    $sql = "SELECT
                                *
                            FROM hospital.appointment_form
                            WHERE hospital.appointment_form.date = " . "'" . $dateFormat . "'" . "
                            AND hospital.appointment_form.doctor_id = " . "'" . $doctorId . "'" . "
                            AND hospital.appointment_form.period = " . "'" . "1" . "'";
                    $result = $conn->query($sql);
                    $status[($i*2) + 1] = array("numberOfAppointment" => $result->num_rows, "status" => "1");
                }
            }
            echo json_encode($status);
            $conn->close();
        }
    
    function available($date, $period, $doctorId) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.available_date
				WHERE hospital.available_date.doctor_id = " . "'" . $doctorId . "'" . "
				AND hospital.available_date.date = " . "'" . $date . "'" . "
				AND hospital.available_date.period = " . "'" . $period . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			return True;
		} else {
			return False;
		}
	}

	function unavailable($date, $period, $doctorId) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.unavailable_date
				WHERE hospital.unavailable_date.doctor_id = " . "'" . $doctorId . "'" . "
				AND hospital.unavailable_date.date = " . "'" . $date . "'" . "
				AND hospital.unavailable_date.period = " . "'" . $period . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			return True;
		} else {
			return False;
		}
	}

	function isFull($date, $period, $doctorId) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.appointment_form
				WHERE hospital.appointment_form.doctor_id = " . "'" . $doctorId . "'" . "
				AND hospital.appointment_form.date = " . "'" . $date . "'" . "
				AND hospital.appointment_form.period = " . "'" . $period . "'";
		$result = $conn->query($sql);
		if($result->num_rows >= 10) {
			return True;
		} else {
			return False;
		}
	}

	function tomorrow($date) {
		$year = date("Y",strtotime($date));
		$month = date("m",strtotime($date));
		$day = date("d",strtotime($date));
		$dayOfMonth = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		if($day >= $dayOfMonth[$month - 1]) {
			$day = 1;
			if($month >= 12) {
				$month = 1;
				$year++;
			} else {
				$month++;
			}
		} else {
			$day++;
		}
		$dateFormat = $year . "-" . $month . "-" . $day;
		return $dateFormat;
	}

    function addAvailableDate($doctorId, $date, $period) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.available_date
				WHERE hospital.available_date.doctor_id = " . "'" . $doctorId . "'" . "
				AND hospital.available_date.date = " . "'" . $date . "'" . "
				AND hospital.available_date.period = " . "'" . $period . "'";
		$result = $conn->query($sql);
		if($result->num_rows == 0) {
			$sql = "SELECT
						hospital.available_date.available_id
					FROM hospital.available_date
					ORDER BY hospital.available_date.available_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["available_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.available_date (available_id,
														doctor_id,
														date,
														period)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $doctorId . "'" . "," .
								"'" . $date . "'" . "," .
								"'" . $period . "'" . ")";
			$conn->query($sql);
		}
		$conn->close();
	}

	function deleteAvailableDate($doctorId, $date, $period) {
		$conn = connectToDatabase();

		$sql = "SELECT
					*
				FROM hospital.available_date
				WHERE hospital.available_date.doctor_id = " . "'" . $doctorId . "'" . "
				AND hospital.available_date.date = " . "'" . $date . "'" . "
				AND hospital.available_date.period = " . "'" . $period . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			//Delete appointment
			$sql = "SELECT
						*
					FROM hospital.appointment_form
					WHERE hospital.appointment_form.doctor_id = " . "'" . $doctorId . "'" . "
					AND hospital.appointment_form.date = " . "'" . $date . "'" . "
					AND hospital.appointment_form.period = " . "'" . $period . "'";
			$getAppointment = $conn->query($sql);
			$numberOfAppointment = $getAppointment->num_rows;
			for($i = 0;$i < $numberOfAppointment;$i++) {
				$appointment = $getAppointment->fetch_assoc();
				$sql = "DELETE FROM hospital.appointment_form
						WHERE hospital.appointment_form.appointment_id = " . "'" . $appointment["appointment_id"] . "'";
				$result = $conn->query($sql);
			}
		}

		//Delete available date
		$sql = "DELETE FROM hospital.available_date
				WHERE hospital.available_date.doctor_id = " . "'" . $doctorId . "'" . "
				AND hospital.available_date.date = " . "'" . $date . "'" . "
				AND hospital.available_date.period = " . "'" . $period . "'";
		$result = $conn->query($sql);

		$conn->close();
	}

	function addUnavailableDate($doctorId, $date, $period) {
		$conn = connectToDatabase();

		//Add Unavailable Date
		$sql = "SELECT
					*
				FROM hospital.unavailable_date
				WHERE hospital.unavailable_date.doctor_id = " . "'" . $doctorId . "'" . "
				AND hospital.unavailable_date.date = " . "'" . $date . "'" . "
				AND hospital.unavailable_date.period = " . "'" . $period . "'";
		$result = $conn->query($sql);
		if($result->num_rows == 0) {
			$sql = "SELECT
						hospital.unavailable_date.unavailable_id
					FROM hospital.unavailable_date
					ORDER BY hospital.unavailable_date.unavailable_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["unavailable_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.unavailable_date (unavailable_id,
														doctor_id,
														date,
														period)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $doctorId . "'" . "," .
								"'" . $date . "'" . "," .
								"'" . $period . "'" . ")";
			$conn->query($sql);
		}

		//Delete appoinrment from unavailable date
		$sql = "SELECT
					*
				FROM hospital.appointment_form
				WHERE hospital.appointment_form.doctor_id = " . "'" . $doctorId . "'" . "
				AND hospital.appointment_form.date = " . "'" . $date . "'" . "
				AND hospital.appointment_form.period = " . "'" . $period . "'";
		$getAppointment = $conn->query($sql);
		$numberOfAppointment = $getAppointment->num_rows;
		for($i = 0;$i < $numberOfAppointment;$i++) {
			$appointment = $getAppointment->fetch_assoc();
			$sql = "DELETE FROM hospital.appointment_form
					WHERE hospital.appointment_form.appointment_id = " . "'" . $appointment["appointment_id"] . "'";
			$result = $conn->query($sql);
		}

		$conn->close();
	}

	function deleteUnavailableDate($doctorId, $date, $period) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.unavailable_date
				WHERE hospital.unavailable_date.doctor_id = " . "'" . $doctorId . "'" . "
				AND hospital.unavailable_date.date = " . "'" . $date . "'" . "
				AND hospital.unavailable_date.period = " . "'" . $period . "'";
		$result = $conn->query($sql);
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}

//	function printJSON($result) {
//		$numberOfRow = $result->num_rows;
//		if($numberOfRow > 0) {
//			for($i = 0;$i < $numberOfRow;$i++) {
//				echo json_encode($result->fetch_assoc());
//			}
//		} else {
//			echo json_encode("No data");
//		}
//	}



?>
