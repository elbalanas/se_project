<?php
    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];
        //OK
        if($_POST['action']=="getAPatient"){
            getAPatient($_POST['patientId']);

        }
        else if($_POST['action']=="getPatientBySsid"){
            getPatientBySsid($_POST['ssid']);
        }
        else if($_POST['action']=="updatePatient"){
            updatePatient($_POST['aPatient']);
        }
        else if($_POST['action']=="deletePatientById"){
            deletePatientById($_POST['patientId']);
        }


    }

	function getAPatient($patientId) {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.patient.patient_id AS patientId,
					hospital.patient.ssid AS ssid,
					hospital.patient.firstname AS firstName,
					hospital.patient.lastname AS lastName,
					hospital.patient.address AS address,
					hospital.patient.sex AS sex,
                    hospital.patient.email AS email,
                    hospital.patient.telephone AS telephone
				FROM hospital.patient
				WHERE hospital.patient.patient_id = " . "'" . $patientId . "'";
		$result = $conn->query($sql);
		printAJSON($result);

		$conn->close();
	}

function getPatientBySsid($ssid) {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.patient.patient_id AS patientId,
					hospital.patient.ssid AS ssid,
					hospital.patient.firstname AS firstName,
					hospital.patient.lastname AS lastName,
					hospital.patient.address AS address,
					hospital.patient.telephone AS telephone,
					hospital.patient.email AS email,
					hospital.patient.password AS password
				FROM hospital.patient
				WHERE hospital.patient.ssid = " . "'" . $ssid . "'";
		$result = $conn->query($sql);
		printAJSON($result);
		$conn->close();
	}
//
//	function updatePatient($patient) {
//		$conn = connectToDatabase();
//		$sql = "SELECT
//					*
//				FROM hospital.patient
//				WHERE hospital.patient.patient_id = " . "'" . $patient["patientId"] . "'";
//		$result = $conn->query($sql);
//		if($result->num_rows > 0) {
//			$sql = "UPDATE hospital.patient
//					SET hospital.patient.ssid = " . "'" . $patient["ssid"] . "'" . "," . "
//						hospital.patient.firstname = " . "'" . $patient["firstName"] . "'" . "," . "
//						hospital.patient.lastname = " . "'" . $patient["lastName"] . "'" . "," . "
//						hospital.patient.sex = " . "'" . $patient["sex"] . "'" . "," . "
//						hospital.patient.address = " . "'" . $patient["address"] . "'" . "," . "
//						hospital.patient.telephone = " . "'" . $patient["telephone"] . "'" . "," . "
//						hospital.patient.email = " . "'" . $patient["email"] . "'" . "
//					WHERE hospital.patient.patient_id = " . "'" . $patient["patientId"] . "'";
//			$conn->query($sql);
//		} else {
//
//			//Add patient
//			$sql = "SELECT
//						hospital.patient.patient_id
//					FROM hospital.patient
//					ORDER BY hospital.patient.patient_id DESC
//					LIMIT 1";
//			$result = $conn->query($sql);
//			$key = (int) $result->fetch_assoc()["patient_id"];
//			$generatedKey = generateKey($key, 10);
//			$sql = "INSERT INTO hospital.patient (patient_id,
//												ssid,
//												firstname,
//												lastname,
//												sex,
//												address,
//												telephone,
//												email)
//					VALUES (" . "'" . $generatedKey . "'" . "," .
//								"'" . $patient["ssid"] . "'" . "," .
//								"'" . $patient["firstName"] . "'" . "," .
//								"'" . $patient["lastName"] . "'" . "," .
//								"'" . $patient["sex"] . "'" . "," .
//								"'" . $patient["address"] . "'" . "," .
//								"'" . $patient["telephone"] . "'" . "," .
//								"'" . $patient["email"] . "'" . ")";
//			$conn->query($sql);
//
//			//Add member
//			$sql = "INSERT INTO hospital.members (username,
//												password,
//												flag,
//												id)
//					VALUES (" . "'" . $patient["ssid"] . "'" . "," .
//								"'" . $patient["ssid"] . "'" . "," .
//								"'" . "patient" . "'" . "," .
//								"'" . $generatedKey . "'" . ")";
//			$conn->query($sql);
//		}
//
//		$conn->close();
//	}
function updatePatient($patient) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.patient
				WHERE hospital.patient.patient_id = " . "'" . $patient["patientId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.patient
					SET hospital.patient.ssid = " . "'" . $patient["ssid"] . "'" . "," . "
						hospital.patient.firstname = " . "'" . $patient["firstName"] . "'" . "," . "
						hospital.patient.lastname = " . "'" . $patient["lastName"] . "'" . "," . "
						hospital.patient.sex = " . "'" . $patient["sex"] . "'" . "," . "
						hospital.patient.address = " . "'" . $patient["address"] . "'" . "," . "
						hospital.patient.telephone = " . "'" . $patient["telephone"] . "'" . "," . "
						hospital.patient.email = " . "'" . $patient["email"] . "'" . "
					WHERE hospital.patient.patient_id = " . "'" . $patient["patientId"] . "'";
			$conn->query($sql);
		} else {

			//Add patient
			$sql = "SELECT
						hospital.patient.patient_id
					FROM hospital.patient
					ORDER BY hospital.patient.patient_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["patient_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.patient (patient_id,
												ssid,
												firstname,
												lastname,
												sex,
												address,
												telephone,
												email)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $patient["ssid"] . "'" . "," .
								"'" . $patient["firstName"] . "'" . "," .
								"'" . $patient["lastName"] . "'" . "," .
								"'" . $patient["sex"] . "'" . "," .
								"'" . $patient["address"] . "'" . "," .
								"'" . $patient["telephone"] . "'" . "," .
								"'" . $patient["email"] . "'" . ")";
			$conn->query($sql);

			//Add member
			$sql = "INSERT INTO hospital.members (username,
												password,
												flag,
												id)
					VALUES (" . "'" . $patient["ssid"] . "'" . "," .
								"'" . $patient["ssid"] . "'" . "," .
								"'" . "patient" . "'" . "," .
								"'" . $generatedKey . "'" . ")";
			$conn->query($sql);
		}

		$conn->close();
	}


	function deletePatientById($patientId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.patient
				WHERE hospital.patient.patient_id = " . "'" . $patientId . "'";
		$result = $conn->query($sql);
		$conn->close();
	}

function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}

?>
