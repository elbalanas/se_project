<?php

    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];


        if($_POST['action']=="getMedicalHistoryById"){
            getMedicalHistoryById($_POST['patientId'], $_POST['date']);
        }
        else if($_POST['action']=="getAllMedicalHistoryById"){
            getAllMedicalHistoryById($_POST['patientId'], $_POST['range']);
        }
        else if($_POST['action']=="updateMedicalHistory"){
            updateMedicalHistory($_POST['medicalHistory']);
        }
    }

	function getMedicalHistoryById($patientId, $date) {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.medical_history.medical_id AS medicalId,
					hospital.medical_history.disease_id AS diseaseId,
					hospital.disease.disease_name AS diseaseName,
					hospital.medical_history.doctor_id AS doctorId,
					hospital.doctor.firstname AS doctorFirstName,
					hospital.doctor.lastname AS doctorLastName,
					hospital.medical_history.patient_id AS patientId,
					hospital.medical_history.note AS doctorNote,
					hospital.medical_history.date AS date
				FROM hospital.medical_history
					JOIN hospital.disease
						ON hospital.medical_history.disease_id = hospital.disease.disease_id
					JOIN hospital.doctor
						ON hospital.medical_history.doctor_id = hospital.doctor.doctor_id
				WHERE hospital.medical_history.patient_id = " . "'" . $patientId . "' " . "
					AND hospital.medical_history.date = " . "'" . $date . "'";
		$result = $conn->query($sql);
		printAJSON($result);
		$conn->close();
	}

	function getAllMedicalHistoryById($patientId, $range) {
		$startAfter = ($range - 1) * 10;
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.medical_history.medical_id AS medicalId,
					hospital.medical_history.disease_id AS diseaseId,
					hospital.disease.disease_name AS diseaseName,
					hospital.medical_history.doctor_id AS doctorId,
					hospital.doctor.firstname AS doctorFirstName,
					hospital.doctor.lastname AS doctorLastName,
					hospital.medical_history.patient_id AS patientId,
					hospital.medical_history.note AS doctorNote,
					hospital.medical_history.date AS date
				FROM hospital.medical_history
					JOIN hospital.disease
						ON hospital.medical_history.disease_id = hospital.disease.disease_id
					JOIN hospital.doctor
						ON hospital.medical_history.doctor_id = hospital.doctor.doctor_id
				WHERE hospital.medical_history.patient_id = " . "'" . $patientId . "' " . "
				ORDER BY hospital.medical_history.date DESC
				LIMIT 10 OFFSET " . $startAfter;

		$result = $conn->query($sql);

		printJSON($result);
		$conn->close();
	}

function updateMedicalHistory($medicalHistory) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.medical_history
				WHERE hospital.medical_history.medical_id = " . "'" . $medicalHistory["medicalId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.medical_history
					SET hospital.medical_history.disease_id = " . "'" . $medicalHistory["diseaseId"] . "'" . "," . "
						hospital.medical_history.doctor_id = " . "'" . $medicalHistory["doctorId"] . "'" . "," . "
						hospital.medical_history.patient_id = " . "'" . $medicalHistory["patientId"] . "'" . "," . "
						hospital.medical_history.note = " . "'" . $medicalHistory["doctorNote"] . "'" . "," . "
						hospital.medical_history.date = " . "'" . $medicalHistory["date"] . "'" . "
					WHERE hospital.medical_history.medical_id = " . "'" . $medicalHistory["medicalId"] . "'";
			$conn->query($sql);
		} else {
			$sql = "SELECT
						hospital.medical_history.medical_id
					FROM hospital.medical_history
					ORDER BY hospital.medical_history.medical_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["medical_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.medical_history (medical_id, disease_id, doctor_id, patient_id, note, date)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $medicalHistory["diseaseId"] . "'" . "," .
								"'" . $medicalHistory["doctorId"] . "'" . "," .
								"'" . $medicalHistory["patientId"] . "'" . "," .
								"'" . $medicalHistory["doctorNote"] . "'" . "," .
								"'" . $medicalHistory["date"] . "'" . ")";
			$conn->query($sql);
		}
        echo $generatedKey;
		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}

	/*function updateMedicalHistory($medicalHistory) {
		$conn = connectToDatabase();
		$sql = "";
		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}*/



?>
