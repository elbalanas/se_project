<?php
    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];
        if($_POST['action']=="getAPharmacist"){
            getAPharmacist($_POST['pharmacistId']);
        }
        else if($_POST['action']=="getPharmacistList"){
            getPharmacistList();
        }
        else if($_POST['action']=="updatePharmacist"){
            updatePharmacist($_POST['aPharmacist']);
        }
        else if($_POST['action']=="deletePharmacistById"){
            deletePharmacistById($_POST['pharmacistId']);
        }

    }


	function getAPharmacist($pharmacistId) {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.pharmacist.pharmacist_id AS pharmacistId,
					hospital.pharmacist.ssid AS ssid,
					hospital.pharmacist.firstname AS firstName,
					hospital.pharmacist.lastname AS lastName,
					hospital.pharmacist.address AS address,
					hospital.pharmacist.telephone AS telephone,
					hospital.pharmacist.password AS password
				FROM hospital.pharmacist
				WHERE hospital.pharmacist.pharmacist_id = " . "'" . $pharmacistId . "'";
		$result = $conn->query($sql);
		printAJSON($result);
		$conn->close();
	}

	function getPharmacistList() {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.pharmacist.pharmacist_id AS pharmacistId,
					hospital.pharmacist.ssid AS ssid,
					hospital.pharmacist.firstname AS firstName,
					hospital.pharmacist.lastname AS lastName,
					hospital.pharmacist.address AS address,
					hospital.pharmacist.telephone AS telephone
				FROM hospital.pharmacist";
		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}

//	function updatePharmacist($pharmacist) {
//		$conn = connectToDatabase();
//		$sql = "SELECT
//					*
//				FROM hospital.pharmacist
//				WHERE hospital.pharmacist.pharmacist_id = " . "'" . $pharmacist["pharmacistId"] . "'";
//		$result = $conn->query($sql);
//		if($result->num_rows > 0) {
//			$sql = "UPDATE hospital.pharmacist
//					SET hospital.pharmacist.ssid = " . "'" . $pharmacist["ssid"] . "'" . "," . "
//						hospital.pharmacist.firstname = " . "'" . $pharmacist["firstName"] . "'" . "," . "
//						hospital.pharmacist.lastname = " . "'" . $pharmacist["lastName"] . "'" . "," . "
//						hospital.pharmacist.address = " . "'" . $pharmacist["address"] . "'" . "," . "
//						hospital.pharmacist.telephone = " . "'" . $pharmacist["telephone"] . "'" . "," . "
//						hospital.pharmacist.password = " . "'" . $pharmacist["password"] . "'" . "
//					WHERE hospital.pharmacist.pharmacist_id = " . "'" . $pharmacist["pharmacistId"] . "'";
//			$conn->query($sql);
//		} else {
//			$sql = "SELECT
//						hospital.pharmacist.pharmacist_id
//					FROM hospital.pharmacist
//					ORDER BY hospital.pharmacist.pharmacist_id DESC
//					LIMIT 1";
//			$result = $conn->query($sql);
//			$key = (int) $result->fetch_assoc()["pharmacist_id"];
//			$generatedKey = generateKey($key, 10);
//			$sql = "INSERT INTO hospital.pharmacist (pharmacist_id, ssid, firstname, lastname, address, telephone, password)
//					VALUES (" . "'" . $generatedKey . "'" . "," .
//								"'" . $pharmacist["ssid"] . "'" . "," .
//								"'" . $pharmacist["firstName"] . "'" . "," .
//								"'" . $pharmacist["lastName"] . "'" . "," .
//								"'" . $pharmacist["address"] . "'" . "," .
//								"'" . $pharmacist["telephone"] . "'" . "," .
//								"'" . $pharmacist["password"] . "'" . ")";
//			$conn->query($sql);
//		}
//
//		$conn->close();
//	}
function updatePharmacist($pharmacist) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.pharmacist
				WHERE hospital.pharmacist.pharmacist_id = " . "'" . $pharmacist["pharmacistId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.pharmacist
					SET hospital.pharmacist.ssid = " . "'" . $pharmacist["ssid"] . "'" . "," . "
						hospital.pharmacist.firstname = " . "'" . $pharmacist["firstName"] . "'" . "," . "
						hospital.pharmacist.lastname = " . "'" . $pharmacist["lastName"] . "'" . "," . "
						hospital.pharmacist.address = " . "'" . $pharmacist["address"] . "'" . "," . "
						hospital.pharmacist.telephone = " . "'" . $pharmacist["telephone"] . "'" . "
					WHERE hospital.pharmacist.pharmacist_id = " . "'" . $pharmacist["pharmacistId"] . "'";
			$conn->query($sql);
		} else {

			//Add pharmacist
			$sql = "SELECT
						hospital.pharmacist.pharmacist_id
					FROM hospital.pharmacist
					ORDER BY hospital.pharmacist.pharmacist_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["pharmacist_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.pharmacist (pharmacist_id,
													ssid,
													firstname,
													lastname,
													address,
													telephone)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $pharmacist["ssid"] . "'" . "," .
								"'" . $pharmacist["firstName"] . "'" . "," .
								"'" . $pharmacist["lastName"] . "'" . "," .
								"'" . $pharmacist["address"] . "'" . "," .
								"'" . $pharmacist["telephone"] . "'" . ")";
			$conn->query($sql);

			//Add member
			$sql = "INSERT INTO hospital.members (username,
												password,
												flag,
												id)
					VALUES (" . "'" . $pharmacist["ssid"] . "'" . "," .
								"'" . $pharmacist["ssid"] . "'" . "," .
								"'" . "pharmacist" . "'" . "," .
								"'" . $generatedKey . "'" . ")";
			$conn->query($sql);
		}

		$conn->close();
	}

	function deletePharmacistById($pharmacistId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.pharmacist
				WHERE hospital.pharmacist.pharmacist_id = " . "'" . $pharmacistId . "'";
		$result = $conn->query($sql);
        $type = 'pharmacist';
        $sql = "DELETE FROM hospital.members
                WHERE hospital.members.id = " . "'" . $pharmacistId . "'" . "
                AND hospital.members.flag = " . "'" . $type . "'";
        $result = $conn->query($sql);
		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}



?>
