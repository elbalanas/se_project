<?php

    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];


        if($_POST['action']=="getDiseaseList"){
            getDiseaseList();
        }
        else if($_POST['action']=="updateDisease"){
            updateDisease($_POST['aDisease']);
        }
        else if($_POST['action']=="deleteDiseaseById"){
            deleteDiseaseById($_POST['diseaseId']);
        }

    }


	function getDiseaseList() {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.disease.disease_id AS diseaseId,
					hospital.disease.disease_name AS diseaseName
				FROM hospital.disease";
		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}

	function updateDisease($disease) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.disease
				WHERE hospital.disease.disease_id = " . "'" . $disease["diseaseId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.disease
					SET hospital.disease.disease_name = " . "'" . $disease["diseaseName"] . "'" . "
					WHERE hospital.disease.disease_id = " . "'" . $disease["diseaseId"] . "'";
			$conn->query($sql);
		} else {
			$sql = "SELECT
						hospital.disease.disease_id
					FROM hospital.disease
					ORDER BY hospital.disease.disease_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["disease_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.disease (disease_id, disease_name)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $disease["diseaseName"] . "'" . ")";
			$conn->query($sql);
		}

		$conn->close();
	}

	function deleteDiseaseById($diseaseId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.disease
				WHERE hospital.disease.disease_id = " . "'" . $diseaseId . "'";
		$result = $conn->query($sql);
		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}



?>
