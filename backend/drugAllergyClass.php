<?php

    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];

        //OK
        if($_POST['action']=="getPatientAllergyById"){
            getPatientAllergyById($_POST['patientId']);
        }
        //OK ??????
        else if($_POST['action']=="addAllergyById"){
            addAllergyById($_POST['patientId'], $_POST['drugList']);
        }

    }





	function getPatientAllergyById($patientId) {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.drug_allergy.drug_allergy_id AS drugAllergyId,
					hospital.drug_allergy.drug_id AS drugId,
					hospital.drug_allergy.patient_id AS patientId,
					hospital.drug.drug_name AS drugName
				FROM hospital.drug_allergy
					JOIN hospital.drug
						ON hospital.drug_allergy.drug_id = hospital.drug.drug_id
				WHERE hospital.drug_allergy.patient_id = " . "'" . $patientId . "'";
		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}

	function addAllergyById($patientId, $drugList) {
		$numberOfRow = count($drugList);
		$conn = connectToDatabase();
		for($i = 0;$i < $numberOfRow;$i++) {
			$sql = "SELECT
						*
					FROM hospital.drug_allergy
					WHERE hospital.drug_allergy.patient_id = " . "'" . $patientId . "'" . "
					AND hospital.drug_allergy.drug_id = " . "'" . $drugList[$i]["drugId"] . "'";
			$result = $conn->query($sql);
			if($result->num_rows == 0) {

				$sql = "SELECT
							hospital.drug_allergy.drug_allergy_id
						FROM hospital.drug_allergy
						ORDER BY hospital.drug_allergy.drug_allergy_id DESC
						LIMIT 1";
				$result = $conn->query($sql);
				$key = (int) $result->fetch_assoc()["drug_allergy_id"];
				$generatedKey = generateKey($key, 13);
				$sql = "INSERT INTO hospital.drug_allergy(drug_allergy_id,patient_id,drug_id)
						 VALUES (" . "'" . $generatedKey . "'" . "," .
									"'" . $patientId . "'" . "," .
									"'" . $drugList[$i]["drugId"] . "'" . ")";
				$conn->query($sql);
                echo $generatedKey;


			}
		}
        echo "success";

		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}




?>
