<?php
    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
               // $value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];
        if($_POST['action']=="getAStaff"){
            getAStaff($_POST['staffId']);
        }
        else if($_POST['action']=="getStaffList"){
            getStaffList();
        }
        else if($_POST['action']=="updateStaff"){
            updateStaff($_POST['aStaff']);
        }
        else if($_POST['action']=="deleteStaffById"){
            deleteStaffById($_POST['staffId']);
        }
        else if($_POST['action']=="changeAuthor"){
            changeAuthor($_POST['ssid'],$_POST['staffId'],$_POST['adminFlag']);
        }

    }

	function getAStaff($staffId) {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.staff.staff_id AS staffId,
					hospital.staff.ssid AS ssid,
					hospital.staff.firstname AS firstName,
					hospital.staff.lastname AS lastName,
					hospital.staff.address AS address,
					hospital.staff.telephone AS telephone,
					hospital.staff.adminflag AS adminFlag,
					hospital.staff.password AS password
				FROM hospital.staff
				WHERE hospital.staff.staff_id = " . "'" . $staffId . "'";
		$result = $conn->query($sql);
		printAJSON($result);
		$conn->close();
	}

	function getStaffList() {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.staff.staff_id AS staffId,
					hospital.staff.ssid AS ssid,
					hospital.staff.firstname AS firstName,
					hospital.staff.lastname AS lastName,
					hospital.staff.address AS address,
					hospital.staff.telephone AS telephone,
                    hospital.staff.adminflag AS adminFlag
				FROM hospital.staff";
		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}

//	function updateStaff($staff) {
//		$conn = connectToDatabase();
//		$sql = "SELECT
//					*
//				FROM hospital.staff
//				WHERE hospital.staff.staff_id = " . "'" . $staff["staffId"] . "'";
//		$result = $conn->query($sql);
//		if($result->num_rows > 0) {
//			$sql = "UPDATE hospital.staff
//					SET hospital.staff.ssid = " . "'" . $staff["ssid"] . "'" . "," . "
//						hospital.staff.firstname = " . "'" . $staff["firstName"] . "'" . "," . "
//						hospital.staff.lastname = " . "'" . $staff["lastName"] . "'" . "," . "
//						hospital.staff.address = " . "'" . $staff["address"] . "'" . "," . "
//						hospital.staff.telephone = " . "'" . $staff["telephone"] . "'" . "," . "
//						hospital.staff.adminFlag = " . "'" . $staff["adminFlag"] . "'" . "," . "
//						hospital.staff.password = " . "'" . $staff["password"] . "'" . "
//					WHERE hospital.staff.staff_id = " . "'" . $staff["staffId"] . "'";
//			$conn->query($sql);
//		} else {
//			$sql = "SELECT
//						hospital.staff.staff_id
//					FROM hospital.staff
//					ORDER BY hospital.staff.staff_id DESC
//					LIMIT 1";
//			$result = $conn->query($sql);
//			$key = (int) $result->fetch_assoc()["staff_id"];
//			$generatedKey = generateKey($key, 10);
//			$sql = "INSERT INTO hospital.staff (staff_id, ssid, firstname, lastname, address, telephone, adminFlag, password)
//					VALUES (" . "'" . $generatedKey . "'" . "," .
//								"'" . $staff["ssid"] . "'" . "," .
//								"'" . $staff["firstName"] . "'" . "," .
//								"'" . $staff["lastName"] . "'" . "," .
//								"'" . $staff["address"] . "'" . "," .
//								"'" . $staff["telephone"] . "'" . "," .
//								"'" . $staff["adminFlag"] . "'" . "," .
//								"'" . $staff["password"] . "'" . ")";
//			$conn->query($sql);
//		}
//
//		$conn->close();
//	}
    
    function changeAuthor($ssid,$staffId,$adminFlag){
        $conn = connectToDatabase();
        $sql = "UPDATE hospital.staff
					SET hospital.staff.adminFlag = " . "'" . $adminFlag . "'" . "
					WHERE hospital.staff.staff_id = " . "'" . $staffId . "'";
        $conn->query($sql);
        $flag = "staff";
        if($adminFlag == "1" || $adminFlag == 1) $flag = "admin";
        $sql = "UPDATE hospital.members
                    SET hospital.members.flag = " . "'" . $flag . "'" . "
                    WHERE hospital.members.username = " . "'" . $ssid . "'";
        $conn->query($sql);
        $conn->close();
        echo $flag;
    }
    
    function updateStaff($staff) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.staff
				WHERE hospital.staff.staff_id = " . "'" . $staff["staffId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.staff
					SET hospital.staff.ssid = " . "'" . $staff["ssid"] . "'" . "," . "
						hospital.staff.firstname = " . "'" . $staff["firstName"] . "'" . "," . "
						hospital.staff.lastname = " . "'" . $staff["lastName"] . "'" . "," . "
						hospital.staff.address = " . "'" . $staff["address"] . "'" . "," . "
						hospital.staff.telephone = " . "'" . $staff["telephone"] . "'" . "," . "
						hospital.staff.adminFlag = " . "'" . $staff["adminFlag"] . "'" . "
					WHERE hospital.staff.staff_id = " . "'" . $staff["staffId"] . "'";
			$conn->query($sql);
		} else {

			//Add staff
			$sql = "SELECT
						hospital.staff.staff_id
					FROM hospital.staff
					ORDER BY hospital.staff.staff_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["staff_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.staff (staff_id,
												ssid,
												firstname,
												lastname,
												address,
												telephone,
												adminFlag)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $staff["ssid"] . "'" . "," .
								"'" . $staff["firstName"] . "'" . "," .
								"'" . $staff["lastName"] . "'" . "," .
								"'" . $staff["address"] . "'" . "," .
								"'" . $staff["telephone"] . "'" . "," .
								"'" . $staff["adminFlag"] . "'" . ")";
			$conn->query($sql);

			//Add member
			$sql = "INSERT INTO hospital.members (username,
												password,
												flag,
												id)
					VALUES (" . "'" . $staff["ssid"] . "'" . "," .
								"'" . $staff["ssid"] . "'" . "," .
								"'" . "staff" . "'" . "," .
								"'" . $generatedKey . "'" . ")";
			$conn->query($sql);
		}

		$conn->close();
	}

	function deleteStaffById($staffId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.staff
				WHERE hospital.staff.staff_id = " . "'" . $staffId . "'";
		$result = $conn->query($sql);
        $type = 'staff';
        $sql = "DELETE FROM hospital.members
                WHERE hospital.members.id = " . "'" . $staffId . "'" . "
                AND hospital.members.flag = " . "'" . $type . "'";
        $result = $conn->query($sql);
        $type = 'admin';
        $sql = "DELETE FROM hospital.members
                WHERE hospital.members.id = " . "'" . $staffId . "'" . "
                AND hospital.members.flag = " . "'" . $type . "'";
        $result = $conn->query($sql);
		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}



?>
