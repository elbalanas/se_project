<?php

    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];


        if($_POST['action']=="getDepartmentList"){
            getDepartmentList();
        }
        else if($_POST['action']=="updateDepartment"){
            updateDepartment($_POST['aDepartment']);
        }
        else if($_POST['action']=="deleteDepartmentById"){
            deleteDepartmentById($_POST['departmentId']);
        }



    }

	function getDepartmentList() {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.department.dno AS departmentNumber,
					hospital.department.department_name AS departmentName
				FROM hospital.department";
		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}

	function updateDepartment($department) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.department
				WHERE hospital.department.dno = " . "'" . $department["departmentNumber"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.department
					SET hospital.department.department_name = " . "'" . $department["departmentName"] . "'" . "
					WHERE hospital.department.dno = " . "'" . $department["departmentNumber"] . "'";
			$conn->query($sql);
		} else {
			$sql = "SELECT
						hospital.department.dno
					FROM hospital.department
					ORDER BY hospital.department.dno DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["dno"];
			$generatedKey = generateKey($key, 4);
			$sql = "INSERT INTO hospital.department (dno, department_name)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $department["departmentName"] . "'" . ")";
			$conn->query($sql);
		}

		$conn->close();
	}

	function deleteDepartmentById($departmentId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.department
				WHERE hospital.department.dno = " . "'" . $departmentId . "'";
		$result = $conn->query($sql);
		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}


?>
