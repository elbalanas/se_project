<?php
    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];

        if($_POST['action']=="getPatientInfoById"){
            getPatientInfoById($_POST['patientId'], $_POST['date']);

        }
        else if($_POST['action']=="updatePatientInfo"){
            updatePatientInfo($_POST['patientInfoForm']);
        }


    }

	function getPatientInfoById($patientId, $date) {
		$conn = connectToDatabase();
        //echo $patientId;
        //echo $date;
        //echo $date;
		$sql = "SELECT
					hospital.patient_info.info_id AS infoId,
					hospital.patient_info.nurse_id AS nurseId,
					hospital.nurse.firstname AS nurseFirstName,
					hospital.nurse.lastname AS nurseLastName,
					hospital.patient_info.patient_id AS patientId,
					hospital.patient.firstname AS patientFirstName,
					hospital.patient.lastname AS patientLastName,
					hospital.patient_info.weight AS weight,
					hospital.patient_info.height AS height,
					hospital.patient_info.temperature AS temperature,
					hospital.patient_info.heartrate AS heartRate,
					hospital.patient_info.systolic AS systolic,
					hospital.patient_info.diastolic AS diastolic,
					hospital.patient_info.date AS date
				FROM hospital.patient_info
					LEFT JOIN hospital.patient
						ON hospital.patient_info.patient_id = hospital.patient.patient_id
					LEFT JOIN hospital.nurse
						ON hospital.patient_info.nurse_id = hospital.nurse.nurse_id
				WHERE hospital.patient_info.patient_id = " . "'" . $patientId . "' " . "
					AND hospital.patient_info.date = " . "'" . $date . "'";
		$result = $conn->query($sql);
		printAJSON($result);
		$conn->close();
	}

	function updatePatientInfo($patientInfoForm) {
        echo $patientInfoForm['weight'];
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.patient_info
				WHERE hospital.patient_info.info_id = " . "'" . $patientInfoForm["infoId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.patient_info
					SET hospital.patient_info.patient_id = " . "'" . $patientInfoForm["patientId"] . "'" . "," . "
						hospital.patient_info.nurse_id = " . "'" . $patientInfoForm["nurseId"] . "'" . "," . "
						hospital.patient_info.weight = " . "'" . $patientInfoForm["weight"] . "'" . "," . "
						hospital.patient_info.height = " . "'" . $patientInfoForm["height"] . "'" . "," . "
						hospital.patient_info.temperature = " . "'" . $patientInfoForm["temperature"] . "'" . "," . "
						hospital.patient_info.heartrate = " . "'" . $patientInfoForm["heartRate"] . "'" . "," . "
						hospital.patient_info.systolic = " . "'" . $patientInfoForm["systolic"] . "'" . "," . "
						hospital.patient_info.diastolic = " . "'" . $patientInfoForm["diastolic"] . "'" . "," . "
						hospital.patient_info.date = " . "'" . $patientInfoForm["date"] . "'" . "
					WHERE hospital.patient_info.info_id = " . "'" . $patientInfoForm["infoId"] . "'";
			$conn->query($sql);
		} else {
			$sql = "SELECT
						hospital.patient_info.info_id
					FROM hospital.patient_info
					ORDER BY hospital.patient_info.info_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["info_id"];
			$generatedKey = generateKey($key, 14);
			$sql = "INSERT INTO hospital.patient_info (info_id, patient_id, nurse_id, weight, height, temperature, heartrate, systolic, diastolic, date)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $patientInfoForm["patientId"] . "'" . "," .
								"'" . $patientInfoForm["nurseId"] . "'" . "," .
								"'" . $patientInfoForm["weight"] . "'" . "," .
								"'" . $patientInfoForm["height"] . "'" . "," .
								"'" . $patientInfoForm["temperature"] . "'" . "," .
								"'" . $patientInfoForm["heartRate"] . "'" . "," .
								"'" . $patientInfoForm["systolic"] . "'" . "," .
								"'" . $patientInfoForm["diastolic"] . "'" . "," .
								"'" . $patientInfoForm["date"] . "'" . ")";
			$conn->query($sql);
		}

		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}



?>
