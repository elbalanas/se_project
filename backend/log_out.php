<?php
include("../inc/config.php");
    logOut();
    function logOut(){
        session_start();
            if(isset($_SESSION['username'])){
                unset($_SESSION['username']);
            }
            if(isset($_SESSION['flag'])){
                unset($_SESSION['flag']);
            }
            if(isset($_SESSION['id'])){
                unset($_SESSION['id']);
            }
            $go="Location: ".base_url;
                header($go);
                exit;
        }
?>
