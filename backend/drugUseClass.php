<?php

    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];


        if($_POST['action']=="getDrugUseById"){
            getDrugUseById($_POST['patientId'], $_POST['date']);
        }
        else if($_POST['action']=="getAllDrugUseById"){
            getAllDrugUseById($_POST['patientId'], $_POST['range']);
        }
        else if($_POST['action']=="updateDrugUse"){
             updateDrugUse($_POST['aDrugUse']);
        }
        else if($_POST['action']=="updateDrugUseOrder"){
            updateDrugUseOrder($_POST['drugUseOrderId'], $_POST['drugId'], $_POST['drugName'], $_POST['drugQuantity'], $_POST['drugUseId']);
        }

    }


	function getDrugUseById($patientId, $date) {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.drug_history.druguse_id,
					hospital.drug_history.prescription_id,
					hospital.drug_history.patient_id,
					hospital.prescription.doctor_id,
					hospital.doctor.firstname AS doctorFirstName,
					hospital.doctor.lastname AS doctorLastName,
					hospital.drug_history.pharmacist_id,
					hospital.pharmacist.firstname AS pharmacistFirstName,
					hospital.pharmacist.lastname AS pharmacistLastName,
					hospital.prescription.medical_id,
					hospital.prescription.date,
					hospital.druguseorder.druguse_order_id,
					hospital.druguseorder.drug_id,
					hospital.druguseorder.quantity,
					hospital.drug.drug_name
				FROM hospital.drug_history
					JOIN hospital.prescription
						ON hospital.drug_history.prescription_id = hospital.prescription.prescription_id
					JOIN hospital.doctor
						ON hospital.prescription.doctor_id = hospital.doctor.doctor_id
					JOIN hospital.pharmacist
						ON hospital.drug_history.pharmacist_id = hospital.pharmacist.pharmacist_id
					JOIN hospital.druguseorder
						ON hospital.drug_history.druguse_id = hospital.druguseorder.druguse_id
					JOIN hospital.drug
						ON hospital.druguseorder.drug_id = hospital.drug.drug_id
				WHERE hospital.drug_history.patient_id = " . "'" . $patientId . "'" . "
				AND hospital.prescription.date = " . "'" . $date . "'";
		$result = $conn->query($sql);
		$drugList = array();
		$output = array("druguse_id" => "", "prescription_id" => "", "patient_id" => "", "doctor_id" => "", "doctorFirstName" => "", "doctorLastName" => "", "pharmacist_id" => "", "pharmacistFirstName" => "", "pharmacistLastName" => "", "medical_id" => "", "date" => "", "drugList" => "");
		$numberOfRow = $result->num_rows;
		for($i = 0;$i < $numberOfRow;$i++) {
			$output = $result->fetch_assoc();
			$drugList[$i] = array("drugUseOrderId" => $output["druguse_order_id"], "drugId" => $output["drug_id"], "drugName" => $output["drug_name"], "drugQuantity" => $output["quantity"]);
		}
		$pres = array("drugUseId" => $output["druguse_id"], "prescriptionId" => $output["prescription_id"], "patientId" => $output["patient_id"], "doctorId" => $output["doctor_id"], "doctorFirstName" => $output["doctorFirstName"], "doctorLastName" => $output["doctorLastName"], "pharmacistId" => $output["pharmacist_id"], "pharmacistFirstName" => $output["pharmacistFirstName"], "pharmacistLastName" => $output["pharmacistLastName"], "medicalId" => $output["medical_id"], "date" => $output["date"], "drugList" => $drugList);
		echo json_encode($pres);
		$conn->close();
	}

	function getAllDrugUseById($patientId, $range) {
		$startAfter = ($range - 1) * 10;
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.drug_history.druguse_id,
					hospital.drug_history.prescription_id,
					hospital.drug_history.patient_id,
					hospital.prescription.doctor_id,
					hospital.doctor.firstname AS doctorFirstName,
					hospital.doctor.lastname AS doctorLastName,
					hospital.drug_history.pharmacist_id,
					hospital.pharmacist.firstname AS pharmacistFirstName,
					hospital.pharmacist.lastname AS pharmacistLastName,
					hospital.prescription.medical_id,
					hospital.prescription.date
				FROM hospital.drug_history
					JOIN hospital.prescription
						ON hospital.drug_history.prescription_id = hospital.prescription.prescription_id
					JOIN hospital.doctor
						ON hospital.prescription.doctor_id = hospital.doctor.doctor_id
					JOIN hospital.pharmacist
						ON hospital.drug_history.pharmacist_id = hospital.pharmacist.pharmacist_id
				WHERE hospital.drug_history.patient_id = " . "'" . $patientId . "'" . "
				 ORDER BY hospital.prescription.date DESC
				 LIMIT 10 OFFSET " . $startAfter;
//        $sql = "SELECT
//					*
//				FROM hospital.drug_history
//					JOIN hospital.prescription
//						ON hospital.drug_history.prescription_id = hospital.prescription.prescription_id
//					JOIN hospital.doctor
//						ON hospital.prescription.doctor_id = hospital.doctor.doctor_id
//
//				WHERE hospital.drug_history.patient_id = " . "'" . $patientId . "'" . "
//				 ORDER BY hospital.prescription.date DESC
//				 LIMIT 10 OFFSET " . $startAfter;
		$result = $conn->query($sql);
		$numberOfRow = $result->num_rows;

		$drugUseOutput = array();
        //echo "in";
		for($i = 0;$i < $numberOfRow;$i++) {
            //echo "in";
			$drugUse = $result->fetch_assoc();

			//Get drugList
			$drugList = array();
			$sql = "SELECT
						*
					FROM hospital.drug_history
						JOIN hospital.druguseorder
							ON hospital.drug_history.druguse_id = hospital.druguseorder.druguse_id
						JOIN hospital.drug
							ON hospital.druguseorder.drug_id = hospital.drug.drug_id
					WHERE hospital.drug_history.druguse_id = " . "'" . $drugUse["druguse_id"] . "'";
			$drugResult = $conn->query($sql);
			$numberOfDrugList = $drugResult->num_rows;
			for($j = 0;$j < $numberOfDrugList;$j++) {
				$eachDrugResult = $drugResult->fetch_assoc();
				$drugList[$j] = array("drugUseOrderId" => $eachDrugResult["druguse_order_id"],
									"drugId" => $eachDrugResult["drug_id"],
									"drugName" => $eachDrugResult["drug_name"],
									"drugQuantity" => $eachDrugResult["quantity"]);
			}
			//--------------------

			$output = array("drugUseId" => $drugUse["druguse_id"],
							"prescriptionId" => $drugUse["prescription_id"],
							"patientId" => $drugUse["patient_id"],
							"doctor_id" => $drugUse["doctor_id"],
							"doctorFirstName" => $drugUse["doctorFirstName"],
							"doctorLastName" => $drugUse["doctorLastName"],
							"pharmacist_id" => $drugUse["pharmacist_id"],
							"pharmacistFirstName" => $drugUse["pharmacistFirstName"],
							"pharmacistLastName" => $drugUse["pharmacistLastName"],
							"medical_id" => $drugUse["medical_id"],
							"date" => $drugUse["date"],
							"drugList" => $drugList);
			$drugUseOutput[$i] = $output;
		}
		echo json_encode($drugUseOutput);
		$conn->close();
	}
//
//	function updateDrugUse($drugUse) {
//		$conn = connectToDatabase();
//		$sql = "SELECT
//					*
//				FROM hospital.drug_history
//				WHERE hospital.drug_history.druguse_id = " . "'" . $drugUse["drugUseId"] . "'";
//		$result = $conn->query($sql);
//		if($result->num_rows > 0) {
//			$sql = "UPDATE hospital.drug_history
//					SET hospital.drug_history.prescription_id = " . "'" . $drugUse["prescriptionId"] . "'" . "," . "
//						hospital.drug_history.patient_id = " . "'" . $drugUse["patientId"] . "'" . "," . "
//						hospital.drug_history.pharmacist_id = " . "'" . $drugUse["pharmacistId"] . "'" . "
//					WHERE hospital.drug_history.druguse_id = " . "'" . $drugUse["drugUseId"] . "'";
//			$conn->query($sql);
//		} else {
//			$sql = "SELECT
//						*
//					FROM hospital.drug_history
//					ORDER BY hospital.drug_history.druguse_id DESC
//					LIMIT 1";
//			$result = $conn->query($sql);
//			$key = (int) $result->fetch_assoc()["druguse_id"];
//			$generatedKey = generateKey($key, 13);
//			$sql = "INSERT INTO hospital.drug_history (druguse_id, prescription_id, patient_id, pharmacist_id)
//					VALUES (" . "'" . $generatedKey . "'" . "," .
//								"'" . $drugUse["prescriptionId"] . "'" . "," .
//								"'" . $drugUse["patientId"] . "'" . "," .
//								"'" . $drugUse["pharmacist_id"] . "'" . ")";
//			$conn->query($sql);
//		}
//
//		$numberOfDrugList = count($drugUse["drugList"]);
//		for($i = 0;$i < $numberOfDrugList;$i++) {
//			updateDrugUseOrder($drugUse["drugList"][$i]["drugUseOrderId"],
//							$drugUse["drugList"][$i]["drugId"],
//							$drugUse["drugList"][$i]["drugName"],
//							$drugUse["drugList"][$i]["drugQuantity"],
//							$drugUse["drugUseId"]);
//		}
//		$conn->close();
//	}
function updateDrugUse($drugUse) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.drug_history
				WHERE hospital.drug_history.druguse_id = " . "'" . $drugUse["drugUseId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.drug_history
					SET hospital.drug_history.prescription_id = " . "'" . $drugUse["prescriptionId"] . "'" . "," . "
						hospital.drug_history.patient_id = " . "'" . $drugUse["patientId"] . "'" . "," . "
						hospital.drug_history.pharmacist_id = " . "'" . $drugUse["pharmacistId"] . "'" . "
					WHERE hospital.drug_history.druguse_id = " . "'" . $drugUse["drugUseId"] . "'";
			$conn->query($sql);
			deleteDrugUseOrder($drugUse["drugUseId"]);
            addDrugUseOrder($drugUse["drugUseId"], $drugUse["drugList"]);
		} else {
			$sql = "SELECT
						*
					FROM hospital.drug_history
					ORDER BY hospital.drug_history.druguse_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["druguse_id"];
			$generatedKey = generateKey($key, 13);
			$sql = "INSERT INTO hospital.drug_history (druguse_id, prescription_id, patient_id, pharmacist_id)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $drugUse["prescriptionId"] . "'" . "," .
								"'" . $drugUse["patientId"] . "'" . "," .
								"'" . $drugUse["pharmacistId"] . "'" . ")";
			$conn->query($sql);
            addDrugUseOrder($generatedKey, $drugUse["drugList"]);
		}

		$conn->close();
	}
function addDrugUseOrder($drugUseId, $drugList) {
		$conn = connectToDatabase();
		for($i = 0;$i < count($drugList);$i++) {
			$sql = "SELECT
						hospital.druguseorder.druguse_order_id
					FROM hospital.druguseorder
					ORDER BY hospital.druguseorder.druguse_order_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["druguse_order_id"];
			$generatedKey = generateKey($key, 13);
			$sql = "INSERT INTO hospital.druguseorder (druguse_order_id, druguse_id, drug_id, quantity)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $drugUseId . "'" . "," .
								"'" . $drugList[$i]["drugId"] . "'" . "," .
								"'" . $drugList[$i]["drugQuantity"] . "'" . ")";
			$conn->query($sql);
		}
		$conn->close();
	}
function deleteDrugUseOrder($drugUseId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.druguseorder
				WHERE hospital.druguseorder.druguse_id = " . "'" . $drugUseId . "'";
		$result = $conn->query($sql);
		$conn->close();
	}


	function updateDrugUseOrder($drugUseOrderId, $drugId, $drugName, $drugQuantity, $drugUseId) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.druguseorder
				WHERE hospital.druguseorder.druguse_order_id = " . "'" . $drugUseOrderId . "'";
		$result = $conn->query($sql);
		echo $sql;
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.druguseorder
					SET hospital.druguseorder.druguse_id = " . "'" . $drugUseId . "'" . "," . "
						hospital.druguseorder.drug_id = " . "'" . $drugId . "'" . "," . "
						hospital.druguseorder.quantity = " . "'" . $drugQuantity . "'" . "
					WHERE hospital.druguseorder.druguse_order_id = " . "'" . $drugUseOrderId . "'";
			$conn->query($sql);
		} else {
			$sql = "SELECT
						hospital.druguseorder.druguse_order_id
					FROM hospital.druguseorder
					ORDER BY hospital.druguseorder.druguse_order_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["druguse_order_id"];
			$generatedKey = generateKey($key, 13);
			$sql = "INSERT INTO hospital.druguseorder (druguse_order_id, druguse_id, drug_id, quantity)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $drugUseId . "'" . "," .
								"'" . $drugId . "'" . "," .
								"'" . $drugQuantity . "'" . ")";
			$conn->query($sql);
		}
		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}




	//getDrugUseById("0000000000", "2015-11-23");

?>
