<?php
include("../inc/config.php");
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$username=strip_tags(trim($_POST["username"]));
	$password=strip_tags(trim($_POST["password"]));
	include(root."inc/connect_db_boom.php");
	$results=$db->prepare("SELECT * FROM members WHERE username LIKE ?");
	$results->bindParam(1,$username);
	$results->execute();
	$true_password=$results->fetch(PDO::FETCH_ASSOC);

	echo
	'<input style="display:none" type="text" name="username" id="usernametmp" value='.htmlspecialchars($username).' ></input>
	<script type="text/javascript">
		var username=document.getElementById("usernametmp");
		if(window.localStorage!=null){
				localStorage["username"]=username.value;
			}
	</script>';

	if($true_password["password"]==$password&&$password!=""){
		$results=$db->prepare("UPDATE members SET recently=NOW() WHERE username=?");
		$results->bindParam(1,$username);
		$results->execute();
		$results=$db->prepare("SELECT recently FROM members WHERE username=?");
		$results->bindParam(1,$username);
		$results->execute();
		$true_recently=$results->fetch(PDO::FETCH_ASSOC);
		session_start();
		$_SESSION['username']=$username;
        $_SESSION['flag']=$true_password["flag"];
        $_SESSION['id']=$true_password["id"];
		$_SESSION['recently']=$true_recently;
		echo '<script>
		window.location.href ="'.base_url.$true_password["flag"].'"
	</script>';
	}
	else{
		$error_message="Username or Password is wrong";

		echo '<script>
		window.location.href = "'.base_url.'?error='.$error_message.'"
		</script>';

	}
}
