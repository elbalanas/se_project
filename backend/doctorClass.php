<?php
    include("../inc/config.php");
include(root.'inc/connect_database.php');
include(root.'inc/print_json.php');


if($_SERVER["REQUEST_METHOD"]=="POST"){

    $errors     = array();    // array to hold validation errors
	$data       = array();    // array to pass back data


    $_POST = json_decode(file_get_contents('php://input'), true);
    foreach ($_POST as $key => $value) {
            if(get_magic_quotes_gpc()){
                $value=stripslash($value);
            }
            //$value=trim(htmlspecialchars($value));

            /*if(empty($value)&&$key!="spam"){
                $error_message="no data";
                break;
            }*/


            $_POST[$key]=$value;
    }
    session_start();
    //$username=$_SESSION['username'];
    //OK
    if($_POST['action']=="getADoctor"){
        getADoctor($_POST['doctorId']);
    }
    //OK
    else if($_POST['action']=="getDoctorList"){
        getDoctorList();
    }
    //OK
    else if($_POST['action']=="getDoctorByDepartment"){
        getDoctorByDepartment($_POST['departmentNumber']);
    }
    else if($_POST['action']=="updateDoctor"){
        updateDoctor($_POST['aDoctor']);
    }
    else if($_POST['action']=="deleteDoctorById"){
        deleteDoctorById($_POST['doctorId']);
    }
}




    function getADoctor($doctorId) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.doctor
				WHERE hospital.doctor.doctor_id = " . "'" . $doctorId . "'";
		$result = $conn->query($sql);
		$getResult = $result->fetch_assoc();
		$output = array("doctorId" => $getResult["doctor_id"],
						"ssid" => $getResult["ssid"],
						"firstName" => $getResult["firstname"],
						"lastName" => $getResult["lastname"],
						"departNumber" => $getResult["dno"],
						"address" => $getResult["address"],
						"telephone" => $getResult["telephone"],
						"password" => $getResult["password"],
						"task" => array($getResult["mo_m"],
										$getResult["mo_a"],
										$getResult["tu_m"],
										$getResult["tu_a"],
										$getResult["we_m"],
										$getResult["we_a"],
										$getResult["th_m"],
										$getResult["th_a"],
										$getResult["fr_m"],
										$getResult["fr_a"],
										$getResult["sa_m"],
										$getResult["sa_a"],
										$getResult["su_m"],
										$getResult["su_a"]));
		echo json_encode($output);
		$conn->close();
	}

	function getDoctorList() {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.doctor";
		$result = $conn->query($sql);
		$numberOfRow = $result->num_rows;
        $output=array();
		if($numberOfRow > 0) {
			for($i = 0;$i < $numberOfRow;$i++) {
				$getResult = $result->fetch_assoc();
//				$output = array("doctorId" => $getResult["doctor_id"],
//								"ssid" => $getResult["ssid"],
//								"firstName" => $getResult["firstname"],
//								"lastName" => $getResult["lastname"],
//								"departNumber" => $getResult["dno"],
//								"address" => $getResult["address"],
//								"telephone" => $getResult["telephone"],
//								"task" => array($getResult["mo_m"],
//												$getResult["mo_a"],
//												$getResult["tu_m"],
//												$getResult["tu_a"],
//												$getResult["we_m"],
//												$getResult["we_a"],
//												$getResult["th_m"],
//												$getResult["th_a"],
//												$getResult["fr_m"],
//												$getResult["fr_a"],
//												$getResult["sa_m"],
//												$getResult["sa_a"],
//												$getResult["su_m"],
//												$getResult["su_a"]));

                array_push($output,array("doctorId" => $getResult["doctor_id"],
								"ssid" => $getResult["ssid"],
								"firstName" => $getResult["firstname"],
								"lastName" => $getResult["lastname"],
								"departmentNumber" => $getResult["dno"],
								"address" => $getResult["address"],
								"telephone" => $getResult["telephone"],
								"task" => array($getResult["mo_m"],
												$getResult["mo_a"],
												$getResult["tu_m"],
												$getResult["tu_a"],
												$getResult["we_m"],
												$getResult["we_a"],
												$getResult["th_m"],
												$getResult["th_a"],
												$getResult["fr_m"],
												$getResult["fr_a"],
												$getResult["sa_m"],
												$getResult["sa_a"],
												$getResult["su_m"],
												$getResult["su_a"])));

			}
            echo json_encode($output);
		} else {
			echo json_encode("No data");
		}
		$conn->close();
	}

	function getDoctorByDepartment($departmentNumber) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.doctor
				WHERE hospital.doctor.dno = " . "'" . $departmentNumber . "'";
		$result = $conn->query($sql);
        $output=array();
		$numberOfRow = $result->num_rows;
		if($numberOfRow > 0) {
			for($i = 0;$i < $numberOfRow;$i++) {
				$getResult = $result->fetch_assoc();
//				$output = array("doctorId" => $getResult["doctor_id"],
//								"ssid" => $getResult["ssid"],
//								"firstName" => $getResult["firstname"],
//								"lastName" => $getResult["lastname"],
//								"departNumber" => $getResult["dno"],
//								"address" => $getResult["address"],
//								"telephone" => $getResult["telephone"],
//								"task" => array($getResult["mo_m"],
//												$getResult["mo_a"],
//												$getResult["tu_m"],
//												$getResult["tu_a"],
//												$getResult["we_m"],
//												$getResult["we_a"],
//												$getResult["th_m"],
//												$getResult["th_a"],
//												$getResult["fr_m"],
//												$getResult["fr_a"],
//												$getResult["sa_m"],
//												$getResult["sa_a"],
//												$getResult["su_m"],
//												$getResult["su_a"]));
                array_push($output,array("doctorId" => $getResult["doctor_id"],
								"ssid" => $getResult["ssid"],
								"firstName" => $getResult["firstname"],
								"lastName" => $getResult["lastname"],
								"departNumber" => $getResult["dno"],
								"address" => $getResult["address"],
								"telephone" => $getResult["telephone"],
								"task" => array($getResult["mo_m"],
												$getResult["mo_a"],
												$getResult["tu_m"],
												$getResult["tu_a"],
												$getResult["we_m"],
												$getResult["we_a"],
												$getResult["th_m"],
												$getResult["th_a"],
												$getResult["fr_m"],
												$getResult["fr_a"],
												$getResult["sa_m"],
												$getResult["sa_a"],
												$getResult["su_m"],
												$getResult["su_a"])));

			}
		} else {
			echo json_encode("No data");
		}
        echo json_encode($output);
		$conn->close();
	}
//
//	function updateDoctor($doctor) {
//		$conn = connectToDatabase();
//		$sql = "SELECT
//					*
//				FROM hospital.doctor
//				WHERE hospital.doctor.doctor_id = " . "'" . $doctor["doctorId"] . "'";
//		$result = $conn->query($sql);
//		if($result->num_rows > 0) {
//			$sql = "UPDATE hospital.doctor
//					SET hospital.doctor.ssid = " . "'" . $doctor["ssid"] . "'" . "," . "
//						hospital.doctor.firstname = " . "'" . $doctor["firstName"] . "'" . "," . "
//						hospital.doctor.lastname = " . "'" . $doctor["lastName"] . "'" . "," . "
//						hospital.doctor.dno = " . "'" . $doctor["departmentNumber"] . "'" . "," . "
//						hospital.doctor.address = " . "'" . $doctor["address"] . "'" . "," . "
//						hospital.doctor.telephone = " . "'" . $doctor["telephone"] . "'" . "," . "
//						hospital.doctor.password = " . "'" . $doctor["password"] . "'" . "," . "
//						hospital.doctor.mo_m = " . "'" . $doctor["task"][0] . "'" . "," . "
//						hospital.doctor.mo_a = " . "'" . $doctor["task"][1] . "'" . "," . "
//						hospital.doctor.tu_m = " . "'" . $doctor["task"][2] . "'" . "," . "
//						hospital.doctor.tu_a = " . "'" . $doctor["task"][3] . "'" . "," . "
//						hospital.doctor.we_m = " . "'" . $doctor["task"][4] . "'" . "," . "
//						hospital.doctor.we_a = " . "'" . $doctor["task"][5] . "'" . "," . "
//						hospital.doctor.th_m = " . "'" . $doctor["task"][6] . "'" . "," . "
//						hospital.doctor.th_a = " . "'" . $doctor["task"][7] . "'" . "," . "
//						hospital.doctor.fr_m = " . "'" . $doctor["task"][8] . "'" . "," . "
//						hospital.doctor.fr_a = " . "'" . $doctor["task"][9] . "'" . "," . "
//						hospital.doctor.sa_m = " . "'" . $doctor["task"][10] . "'" . "," . "
//						hospital.doctor.sa_a = " . "'" . $doctor["task"][11] . "'" . "," . "
//						hospital.doctor.su_m = " . "'" . $doctor["task"][12] . "'" . "," . "
//						hospital.doctor.su_a = " . "'" . $doctor["task"][13] . "'" . "
//					WHERE hospital.doctor.doctor_id = " . "'" . $doctor["doctorId"] . "'";
//			$conn->query($sql);
//		} else {
//			$sql = "SELECT
//						hospital.doctor.doctor_id
//					FROM hospital.doctor
//					ORDER BY hospital.doctor.doctor_id DESC
//					LIMIT 1";
//			$result = $conn->query($sql);
//			$key = (int) $result->fetch_assoc()["doctor_id"];
//			$generatedKey = generateKey($key, 10);
//			$sql = "INSERT INTO hospital.doctor (doctor_id, ssid, firstname, lastname, dno, address, telephone, password, mo_m, mo_a, tu_m, tu_a, we_m, we_a, th_m, th_a, fr_m, fr_a, sa_m, sa_a, su_m, su_a)
//					VALUES (" . "'" . $generatedKey . "'" . "," .
//								"'" . $doctor["ssid"] . "'" . "," .
//								"'" . $doctor["firstName"] . "'" . "," .
//								"'" . $doctor["lastName"] . "'" . "," .
//								"'" . $doctor["departmentNumber"] . "'" . "," .
//								"'" . $doctor["address"] . "'" . "," .
//								"'" . $doctor["telephone"] . "'" . "," .
//								"'" . $doctor["password"] . "'" . "," .
//								"'" . $doctor["task"][0] . "'" . "," .
//								"'" . $doctor["task"][1] . "'" . "," .
//								"'" . $doctor["task"][2] . "'" . "," .
//								"'" . $doctor["task"][3] . "'" . "," .
//								"'" . $doctor["task"][4] . "'" . "," .
//								"'" . $doctor["task"][5] . "'" . "," .
//								"'" . $doctor["task"][6] . "'" . "," .
//								"'" . $doctor["task"][7] . "'" . "," .
//								"'" . $doctor["task"][8] . "'" . "," .
//								"'" . $doctor["task"][9] . "'" . "," .
//								"'" . $doctor["task"][10] . "'" . "," .
//								"'" . $doctor["task"][11] . "'" . "," .
//								"'" . $doctor["task"][12] . "'" . "," .
//								"'" . $doctor["task"][13] . "'" . ")";
//			$conn->query($sql);
//		}
//
//		$conn->close();
//	}

	function updateDoctor($doctor) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.doctor
				WHERE hospital.doctor.doctor_id = " . "'" . $doctor["doctorId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.doctor
					SET hospital.doctor.ssid = " . "'" . $doctor["ssid"] . "'" . "," . "
						hospital.doctor.firstname = " . "'" . $doctor["firstName"] . "'" . "," . "
						hospital.doctor.lastname = " . "'" . $doctor["lastName"] . "'" . "," . "
						hospital.doctor.dno = " . "'" . $doctor["departmentNumber"] . "'" . "," . "
						hospital.doctor.address = " . "'" . $doctor["address"] . "'" . "," . "
						hospital.doctor.telephone = " . "'" . $doctor["telephone"] . "'" . "," . "
						hospital.doctor.mo_m = " . "'" . $doctor["task"][0] . "'" . "," . "
						hospital.doctor.mo_a = " . "'" . $doctor["task"][1] . "'" . "," . "
						hospital.doctor.tu_m = " . "'" . $doctor["task"][2] . "'" . "," . "
						hospital.doctor.tu_a = " . "'" . $doctor["task"][3] . "'" . "," . "
						hospital.doctor.we_m = " . "'" . $doctor["task"][4] . "'" . "," . "
						hospital.doctor.we_a = " . "'" . $doctor["task"][5] . "'" . "," . "
						hospital.doctor.th_m = " . "'" . $doctor["task"][6] . "'" . "," . "
						hospital.doctor.th_a = " . "'" . $doctor["task"][7] . "'" . "," . "
						hospital.doctor.fr_m = " . "'" . $doctor["task"][8] . "'" . "," . "
						hospital.doctor.fr_a = " . "'" . $doctor["task"][9] . "'" . "," . "
						hospital.doctor.sa_m = " . "'" . $doctor["task"][10] . "'" . "," . "
						hospital.doctor.sa_a = " . "'" . $doctor["task"][11] . "'" . "," . "
						hospital.doctor.su_m = " . "'" . $doctor["task"][12] . "'" . "," . "
						hospital.doctor.su_a = " . "'" . $doctor["task"][13] . "'" . "
					WHERE hospital.doctor.doctor_id = " . "'" . $doctor["doctorId"] . "'";
			$conn->query($sql);
		} else {

			//Add doctor
			$sql = "SELECT
						hospital.doctor.doctor_id
					FROM hospital.doctor
					ORDER BY hospital.doctor.doctor_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["doctor_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.doctor (doctor_id,
												ssid,
												firstname,
												lastname,
												dno,
												address,
												telephone,
												mo_m, mo_a,
												tu_m, tu_a,
												we_m, we_a,
												th_m, th_a,
												fr_m, fr_a,
												sa_m, sa_a,
												su_m, su_a)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $doctor["ssid"] . "'" . "," .
								"'" . $doctor["firstName"] . "'" . "," .
								"'" . $doctor["lastName"] . "'" . "," .
								"'" . $doctor["departmentNumber"] . "'" . "," .
								"'" . $doctor["address"] . "'" . "," .
								"'" . $doctor["telephone"] . "'" . "," .
								"'" . $doctor["task"][0] . "'" . "," .
								"'" . $doctor["task"][1] . "'" . "," .
								"'" . $doctor["task"][2] . "'" . "," .
								"'" . $doctor["task"][3] . "'" . "," .
								"'" . $doctor["task"][4] . "'" . "," .
								"'" . $doctor["task"][5] . "'" . "," .
								"'" . $doctor["task"][6] . "'" . "," .
								"'" . $doctor["task"][7] . "'" . "," .
								"'" . $doctor["task"][8] . "'" . "," .
								"'" . $doctor["task"][9] . "'" . "," .
								"'" . $doctor["task"][10] . "'" . "," .
								"'" . $doctor["task"][11] . "'" . "," .
								"'" . $doctor["task"][12] . "'" . "," .
								"'" . $doctor["task"][13] . "'" . ")";
			$conn->query($sql);

			//Add member
			$sql = "INSERT INTO hospital.members (username,
												password,
												flag,
												id)
					VALUES (" . "'" . $doctor["ssid"] . "'" . "," .
								"'" . $doctor["ssid"] . "'" . "," .
								"'" . "doctor" . "'" . "," .
								"'" . $generatedKey . "'" . ")";
			$conn->query($sql);
		}

		$conn->close();
	}

	function deleteDoctorById($doctorId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.doctor
				WHERE hospital.doctor.doctor_id = " . "'" . $doctorId . "'";
		$result = $conn->query($sql);
        $type = 'doctor';
        $sql = "DELETE FROM hospital.members
                WHERE hospital.members.id = " . "'" . $doctorId . "'" . "
                AND hospital.members.flag = " . "'" . $type . "'";
        $result = $conn->query($sql);
		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}



?>
