<?php

    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];


        if($_POST['action']=="getDrugList"){
            getDrugList();
        }
        else if($_POST['action']=="updateDrug"){
            updateDrug($_POST['aDrug']);
        }
        else if($_POST['action']=="deleteDrugById"){
            deleteDrugById($_POST['drugId']);
        }
    }

	function getDrugList() {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.drug.drug_id AS drugId,
					hospital.drug.drug_name AS drugName
				FROM hospital.drug";
		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}

	function updateDrug($drug) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.drug
				WHERE hospital.drug.drug_id = " . "'" . $drug["drugId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.drug
					SET hospital.drug.drug_name = " . "'" . $drug["drugName"] . "'" . "
					WHERE hospital.drug.drug_id = " . "'" . $drug["drugId"] . "'";
			$conn->query($sql);
		} else {
			$sql = "SELECT
						hospital.drug.drug_id
					FROM hospital.drug
					ORDER BY hospital.drug.drug_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["drug_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.drug (drug_id, drug_name)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $drug["drugName"] . "'" . ")";
			$conn->query($sql);

		}

		$conn->close();
	}

	function deleteDrugById($drugId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.drug
				WHERE hospital.drug.drug_id = " . "'" . $drugId . "'";
		$result = $conn->query($sql);
		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}



?>
