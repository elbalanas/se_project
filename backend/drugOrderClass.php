<?php

    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];


        if($_POST['action']=="getDrugOrderById"){
            getDrugOrderById($_POST['patientId'], $_POST['date']);
        }
        else if($_POST['action']=="updateDrugOrder"){

            echo json_encode($_POST);
            updateDrugOrder($_POST['drugOrder']);
        }
    }


	function getDrugOrderById($patientId, $date) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.drugorder
					JOIN hospital.prescription
						ON hospital.drugorder.prescription_id = hospital.prescription.prescription_id
					JOIN hospital.doctor
						ON hospital.prescription.doctor_id = hospital.doctor.doctor_id
					JOIN hospital.drug
						ON hospital.drugorder.drug_id = hospital.drug.drug_id
				WHERE hospital.prescription.patient_id = " . "'" . $patientId . "'" . "
				AND hospital.prescription.date = " . "'" . $date . "'";
		$result = $conn->query($sql);
		$drugList = array();
		$output;
		$numberOfRow = $result->num_rows;
        if($numberOfRow>0){
            for($i = 0;$i < $numberOfRow;$i++) {
                $output = $result->fetch_assoc();
                $drugList[$i] = array("drugOrderId" => $output["order_id"], "drugId" => $output["drug_id"], "drugName" => $output["drug_name"], "drugQuantity" => $output["quantity"]);
            }
            $pres = array("prescriptionId" => $output["prescription_id"], "patientId" => $output["patient_id"], "doctorId" => $output["doctor_id"], "doctorFirstName" => $output["firstname"], "doctorLastName" => $output["lastname"], "medicalId" => $output["medical_id"], "date" => $output["date"], "doctorNote" => "note" , "drugList" => $drugList);
        }else{
            $sql = "SELECT
					*
				FROM hospital.prescription
					JOIN hospital.doctor
						ON hospital.prescription.doctor_id = hospital.doctor.doctor_id
				WHERE hospital.prescription.patient_id = " . "'" . $patientId . "'" . "
				AND hospital.prescription.date = " . "'" . $date . "'";  
            $result = $conn->query($sql);
            $output = $result->fetch_assoc();
            $pres = array("prescriptionId" => $output["prescription_id"], "patientId" => $output["patient_id"], "doctorId" => $output["doctor_id"], "doctorFirstName" => $output["firstname"], "doctorLastName" => $output["lastname"], "medicalId" => $output["medical_id"], "date" => $output["date"], "doctorNote" => "note" , "drugList" => array());
        }
        echo json_encode($pres);
		$conn->close();
	}

	function updateDrugOrder($drugOrder) {
		$conn = connectToDatabase();
		echo "in";
		//Update prescription
		$sql = "SELECT
					*
				FROM hospital.prescription
				WHERE hospital.prescription.prescription_id = " . "'" . $drugOrder["prescriptionId"] . "'";
		$result = $conn->query($sql);

		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.prescription
					SET hospital.prescription.patient_id = " . "'" . $drugOrder["patientId"] . "'" . "," . "
						hospital.prescription.doctor_id = " . "'" . $drugOrder["doctorId"] . "'" . "," . "
						hospital.prescription.medical_id = " . "'" . $drugOrder["medicalId"] . "'" . "," . "
						hospital.prescription.note = " . "'" . $drugOrder["doctorNote"] . "'" . "," . "
						hospital.prescription.date = " . "'" . $drugOrder["date"] . "'" . "
					WHERE hospital.prescription.prescription_id = " . "'" . $drugOrder["prescriptionId"] . "'";
			$conn->query($sql);
			deleteDrugOrder($drugOrder["prescriptionId"]);
            addDrugOrder($drugOrder["prescriptionId"], $drugOrder["drugList"]);
		} else {
			$sql = "SELECT
						hospital.prescription.prescription_id
					FROM hospital.prescription
					ORDER BY hospital.prescription.prescription_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["prescription_id"];
			$generatedKey = generateKey($key, 15);
			$sql = "INSERT INTO hospital.prescription (prescription_id, patient_id, doctor_id, medical_id, note, date)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $drugOrder["patientId"] . "'" . "," .
								"'" . $drugOrder["doctorId"] . "'" . "," .
								"'" . $drugOrder["medicalId"] . "'" . "," .
								"'" . $drugOrder["doctorNote"] . "'" . "," .
								"'" . $drugOrder["date"] . "'" . ")";
			$conn->query($sql);
            addDrugOrder($generatedKey, $drugOrder["drugList"]);
		}


		$conn->close();
	}

	function addDrugOrder($prescriptionId, $drugList) {
		$conn = connectToDatabase();

		for($i = 0;$i < count($drugList);$i++) {
			$sql = "SELECT
						hospital.drugorder.order_id
					FROM hospital.drugorder
					ORDER BY hospital.drugorder.order_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["order_id"];
			$generatedKey = generateKey($key, 13);
			$sql = "INSERT INTO hospital.drugorder (order_id, prescription_id, drug_id, quantity)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $prescriptionId . "'" . "," .
								"'" . $drugList[$i]["drugId"] . "'" . "," .
								"'" . $drugList[$i]["drugQuantity"] . "'" . ")";
             echo $drugList[$i]["drugId"];
			$conn->query($sql);
		}
		$conn->close();
	}

	function deleteDrugOrder($prescriptionId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.drugorder
				WHERE hospital.drugorder.prescription_id = " . "'" . $prescriptionId . "'";
		$result = $conn->query($sql);
		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}


	/*$drugList = array(
					array("drugOrderId" => "0000000000000", "drugId" => "00000000000000", "drugName" => "Betadine", "drugQuantity" => "250"),
					array("drugOrderId" => "0000000000001", "drugId" => "00000000000000", "drugName" => "Betadine", "drugQuantity" => "250"),
					array("drugOrderId" => "123", "drugId" => "00000000000000", "drugName" => "Betadine", "drugQuantity" => "250")
				);
	$drugorder = array("prescriptionId" => "000000000000001", "patientId" => "0000000000", "doctorId" => "0000000000", "doctorFirstName" => "Manda", "doctorLastName" => "Model", "medicalId" => "0000000000", "date" => "2015-11-23", "doctorNote" => "Nothing", "drugList" => $drugList);
	updateDrugOrderById($drugorder);*/

?>
