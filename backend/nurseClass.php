<?php
    include("../inc/config.php");
    include(root.'inc/connect_database.php');
    include(root.'inc/print_json.php');


    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $errors     = array();    // array to hold validation errors
        $data       = array();    // array to pass back data


        $_POST = json_decode(file_get_contents('php://input'), true);
        foreach ($_POST as $key => $value) {
                if(get_magic_quotes_gpc()){
                    $value=stripslash($value);
                }
                //$value=trim(htmlspecialchars($value));

                /*if(empty($value)&&$key!="spam"){
                    $error_message="no data";
                    break;
                }*/


                $_POST[$key]=$value;
        }
        session_start();
        //$username=$_SESSION['username'];
        //OK
        if($_POST['action']=="getANurse"){
            getANurse($_POST['nurseId']);

        }
        else if($_POST['action']=="getNurseList"){
            getNurseList();
        }
        else if($_POST['action']=="updateNurse"){
            updateNurse($_POST['aNurse']);
        }
        //OK
        else if($_POST['action']=="deleteNurseById"){
            deleteNurseById($_POST['nurseId']);
        }

    }


    function getANurse($nurseId) {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.nurse.nurse_id AS nurseId,
					hospital.nurse.ssid AS ssid,
					hospital.nurse.firstname AS firstName,
					hospital.nurse.lastname AS lastName,
					hospital.nurse.address AS address,
					hospital.nurse.telephone AS telephone,
					hospital.nurse.password AS password
				FROM hospital.nurse
				WHERE hospital.nurse.nurse_id = " . "'" . $nurseId . "'";
		$result = $conn->query($sql);
		printAJSON($result);
		$conn->close();
	}

	function getNurseList() {
		$conn = connectToDatabase();
		$sql = "SELECT
					hospital.nurse.nurse_id AS nurseId,
					hospital.nurse.ssid AS ssid,
					hospital.nurse.firstname AS firstName,
					hospital.nurse.lastname AS lastName,
					hospital.nurse.address AS address,
					hospital.nurse.telephone AS telephone
				FROM hospital.nurse";
		$result = $conn->query($sql);
		printJSON($result);
		$conn->close();
	}
//
//	function updateNurse($nurse) {
//		$conn = connectToDatabase();
//		$sql = "SELECT
//					*
//				FROM hospital.nurse
//				WHERE hospital.nurse.nurse_id = " . "'" . $nurse["nurseId"] . "'";
//		$result = $conn->query($sql);
//		if($result->num_rows > 0) {
//			$sql = "UPDATE hospital.nurse
//					SET hospital.nurse.ssid = " . "'" . $nurse["ssid"] . "'" . "," . "
//						hospital.nurse.firstname = " . "'" . $nurse["firstName"] . "'" . "," . "
//						hospital.nurse.lastname = " . "'" . $nurse["lastName"] . "'" . "," . "
//						hospital.nurse.address = " . "'" . $nurse["address"] . "'" . "," . "
//						hospital.nurse.telephone = " . "'" . $nurse["telephone"] . "'" . "," . "
//						hospital.nurse.password = " . "'" . $nurse["password"] . "'" . "
//					WHERE hospital.nurse.nurse_id = " . "'" . $nurse["nurseId"] . "'";
//			$conn->query($sql);
//		} else {
//			$sql = "SELECT
//						hospital.nurse.nurse_id
//					FROM hospital.nurse
//					ORDER BY hospital.nurse.nurse_id DESC
//					LIMIT 1";
//			$result = $conn->query($sql);
//			$key = (int) $result->fetch_assoc()["nurse_id"];
//			$generatedKey = generateKey($key, 10);
//			$sql = "INSERT INTO hospital.nurse (nurse_id, ssid, firstname, lastname, address, telephone, password)
//					VALUES (" . "'" . $generatedKey . "'" . "," .
//								"'" . $nurse["ssid"] . "'" . "," .
//								"'" . $nurse["firstName"] . "'" . "," .
//								"'" . $nurse["lastName"] . "'" . "," .
//								"'" . $nurse["address"] . "'" . "," .
//								"'" . $nurse["telephone"] . "'" . "," .
//								"'" . $nurse["password"] . "'" . ")";
//			$conn->query($sql);
//		}
//
//		$conn->close();
//	}

	function updateNurse($nurse) {
		$conn = connectToDatabase();
		$sql = "SELECT
					*
				FROM hospital.nurse
				WHERE hospital.nurse.nurse_id = " . "'" . $nurse["nurseId"] . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$sql = "UPDATE hospital.nurse
					SET hospital.nurse.ssid = " . "'" . $nurse["ssid"] . "'" . "," . "
						hospital.nurse.firstname = " . "'" . $nurse["firstName"] . "'" . "," . "
						hospital.nurse.lastname = " . "'" . $nurse["lastName"] . "'" . "," . "
						hospital.nurse.address = " . "'" . $nurse["address"] . "'" . "," . "
						hospital.nurse.telephone = " . "'" . $nurse["telephone"] . "'" . "
					WHERE hospital.nurse.nurse_id = " . "'" . $nurse["nurseId"] . "'";
			$conn->query($sql);
		} else {

			//Add nurse
			$sql = "SELECT
						hospital.nurse.nurse_id
					FROM hospital.nurse
					ORDER BY hospital.nurse.nurse_id DESC
					LIMIT 1";
			$result = $conn->query($sql);
			$key = (int) $result->fetch_assoc()["nurse_id"];
			$generatedKey = generateKey($key, 10);
			$sql = "INSERT INTO hospital.nurse (nurse_id,
												ssid,
												firstname,
												lastname,
												address,
												telephone)
					VALUES (" . "'" . $generatedKey . "'" . "," .
								"'" . $nurse["ssid"] . "'" . "," .
								"'" . $nurse["firstName"] . "'" . "," .
								"'" . $nurse["lastName"] . "'" . "," .
								"'" . $nurse["address"] . "'" . "," .
								"'" . $nurse["telephone"] . "'" . ")";
			$conn->query($sql);

			//Add member
			$sql = "INSERT INTO hospital.members (username,
												password,
												flag,
												id)
					VALUES (" . "'" . $nurse["ssid"] . "'" . "," .
								"'" . $nurse["ssid"] . "'" . "," .
								"'" . "nurse" . "'" . "," .
								"'" . $generatedKey . "'" . ")";
			$conn->query($sql);
		}

		$conn->close();
	}

	function deleteNurseById($nurseId) {
		$conn = connectToDatabase();
		$sql = "DELETE FROM hospital.nurse
				WHERE hospital.nurse.nurse_id = " . "'" . $nurseId . "'";
		$result = $conn->query($sql);
        $type = 'nurse';
        $sql = "DELETE FROM hospital.members
                WHERE hospital.members.id = " . "'" . $nurseId . "'" . "
                AND hospital.members.flag = " . "'" . $type . "'";
        $result = $conn->query($sql);
		$conn->close();
	}

	function generateKey($key, $length) {
		$newKey = ++$key . "";
		$loop = $length - strlen($newKey);
		for($i = 0;$i < $loop;$i++) {
			$newKey = "0" . $newKey;
		}
		return $newKey;
	}




?>
