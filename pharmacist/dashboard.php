<?php include("../inc/config.php"); ?>
    <div ui-yield-to="modals"></div>
    <div ui-content-for="modals">
        <div class="modal" ui-if="modals" ui-state='modals'>
            <div class="modal-backdrop in"></div>
            <div class="modal-dialog" style='height:100%;'>
                <div class="modal-content" style='height:90%;'>

                    <div class="modal-header">
                        <button class="close" ui-turn-off="modals">&times;</button>
                        <h4 class="modal-title">Patient Infomation</h4>
                    </div>

                    <div class="modal-body" style='height:85%; '>
                        <div class="container" style='height:100%; overflow:auto;'>

                            <div class="col-md-12">
                                <div ui-state="patientTab" default="2" class='row'>
                                    <ul class="nav nav-pills nav-tabs">

                                        <li ui-class="{'active': patientTab == 1}">
                                            <a ui-set="{'patientTab': 1}">Patient Information</a>
                                        </li>

                                        <li ui-class="{'active': patientTab == 2}">
                                            <a ui-set="{'patientTab': 2}">Measurement</a>
                                        </li>

                                        <li ui-class="{'active': patientTab == 3}">
                                            <a ui-set="{'patientTab': 3}">Dispensation History</a>
                                        </li>

                                        <li ui-class="{'active': patientTab == 4}">
                                            <a ui-set="{'patientTab': 4}">Drug Allergy</a>
                                        </li>

                                        <li ui-class="{'active': patientTab == 5}">
                                            <a ui-set="{'patientTab': 5}">Dispensation</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-12" style='height:80%; overflow:auto;'>
                                <div ui-if="patientTab == 1">
                                    <h3 class="page-header">Patient Information</h3>
                                    <br>
                                    <div>
                                        <div class='panel panel-info'>

                                            <div class='panel-heading'>General Information</div>

                                            <br>
                                            <div class='row section'>
                                                <div class='col-md-3 col-md-offset-1'>
                                                    <img src='../img/jack.jpg' class='img-rounded img-responsive' height="250" width="150">
                                                </div>
                                                <div class='col-md-8'>

                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>Name</label>
                                                            <p>{{vm.aPatient.firstName}} {{vm.aPatient.lastName}}</p>
                                                        </div>
                                                        <div class='col-md-3'>
                                                            <label>SSID</label>
                                                            <p>{{vm.aPatient.ssid}}</p>
                                                        </div>
                                                        <div class='col-md-2'>
                                                            <label>Sex</label>
                                                            <p>{{vm.tranSex(vm.aPatient.sex)}}</p>
                                                        </div>
                                                    </div>

                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>Email</label>
                                                            <p>{{vm.aPatient.email}}</p>
                                                        </div>
                                                        <div class='col-md-4'>
                                                            <label>Telephone</label>
                                                            <p>{{vm.aPatient.telephone}}</p>
                                                        </div>
                                                    </div>

                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-8'>
                                                            <label>Address</label>
                                                            <p>{{vm.aPatient.address}}</p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <br>
                                    <div>
                                        <div class='panel panel-info'>
                                            <div class='panel-heading'>Diagnosis</div>
                                            <div class='row section'>
                                                <br>
                                                <div class='col-md-12'>

                                                    <div class='row'>
                                                        <div class='col-md-5 col-md-offset-1'>
                                                            <label>Appointment with</label>
                                                            <p>Doctor {{vm.focusPerson.doctorFirstName}} {{vm.focusPerson.doctorLastName}}</p>
                                                        </div>
                                                        <div class='col-md-5'>
                                                            <label>Department</label>
                                                            <p>{{vm.focusPerson.departmentName}}</p>
                                                        </div>
                                                    </div>

                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-8 col-md-offset-1'>
                                                            <label>Cause</label>
                                                            <p>{{vm.checkCause(vm.focusPerson.cause)}}</p>
                                                        </div>
                                                    </div>

                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-3 col-md-offset-1'>
                                                            <label>Disease</label>
                                                            <p>{{vm.aMedicalHistory.diseaseName}}</p>
                                                        </div>
                                                        <div class='col-md-6'>
                                                            <label>Doctor Note</label>
                                                            <p>{{vm.checkCause(vm.aMedicalHistory.doctorNote)}}</p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div ui-if="patientTab == 2">
                                    <h3 class="page-header">Measurement</h3>
                                    <br>
                                    <div class='panel panel-info'>

                                        <div class='panel-heading'>Measured by <strong>Nurse {{vm.aPatientInfo.nurseFirstName}} {{vm.aPatientInfo.nurseLastName}}</strong></div>

                                        <br>
                                        <div class='section'>
                                            <div class='row'>
                                                <div class='col-md-10 col-md-offset-1'>

                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>Weight</label>
                                                            <p>{{vm.aPatientInfo.weight}} kg</p>
                                                        </div>
                                                        <div class='col-md-4'>
                                                            <label>Height</label>
                                                            <p>{{vm.aPatientInfo.height}} cm</p>
                                                        </div>
                                                        <div class='col-md-4'>
                                                            <label>Temperature</label>
                                                            <p>{{vm.aPatientInfo.temperature}} c</p>
                                                        </div>
                                                    </div>

                                                    <br>
                                                    <div class='row'>
                                                        <div class='col-md-4'>
                                                            <label>Heart Rate</label>
                                                            <p>{{vm.aPatientInfo.heartRate}} BPM</p>
                                                        </div>
                                                        <div class='col-md-4'>
                                                            <label>Blood Pressure</label>
                                                            <p>{{vm.aPatientInfo.systolic}} / {{vm.aPatientInfo.diastolic}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div ui-if="patientTab == 3">
                                    <h3 class='page-header'>Dispensation History</h3>
                                    <div align='center'>
                                        <ul class="pagination" style="margin: 0 0;">

                                            <li>
                                                <a aria-label="Previous" ng-click='vm.previousUse()'>
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>

                                            <li ng-repeat='drugUse in vm.drugUse' class="" ng-click='vm.setDrugUse(drugUse)'>
                                                <a>{{drugUse.date}}</a>
                                            </li>

                                            <li ng-click='vm.nextUse()'>
                                                <a aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <br>
                                    <br>
                                    <div class='panel panel-info'>
                                        <div class='panel-heading'>Dispensated at <strong>{{vm.focusDrugUse.date}}</strong></div>
                                        <br>
                                        <div class='row'>
                                            <div class='col-md-2 col-md-offset-1'>
                                                <label>Dispensed by</label>
                                                <p>{{vm.focusDrugUse.pharmacistFirstName}} {{vm.focusDrugUse.pharmacistLastName}}</p>
                                            </div>
                                            <div class='col-md-8'>
                                                <table class='table'>
                                                    <thead>
                                                        <th>#</th>
                                                        <th>Drug Name</th>
                                                        <th>Drug ID</th>
                                                        <th>Quantity</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat='drug in vm.focusDrugUse.drugList'>
                                                            <td>{{$index+1}}</td>
                                                            <td>{{drug.drugName}}</td>
                                                            <td>{{drug.drugId}}</td>
                                                            <td>{{drug.drugQuantity}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div ui-if="patientTab == 4">
                                    <h3 class='page-header'>Drug Allergy</h3>
                                    <br>
                                    <div class='panel panel-info'>

                                        <div class='panel-heading'>
                                            Drug Allergy List<a class='pull-right' ng-click='vm.addAllergySlot()'><i class="fa fa-plus" style="color:"></i></a>
                                        </div>

                                        <br>
                                        <div class='row'>
                                            <div class='col-md-10 col-md-offset-1'>
                                                <table class='table'>
                                                    <thead>
                                                        <th>#</th>
                                                        <th>Drug Name</th>
                                                        <th>Drug ID</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat="drug in vm.drugAllergy">
                                                            <td>{{$index + 1}}</td>
                                                            <td>{{drug.drugName}}</td>
                                                            <td>{{drug.drugId}}</td>
                                                        </tr>
                                                        <tr ng-repeat='newAllergy in vm.newDrugAllergy'>

                                                            <td ng-click='vm.delAllergySlot($index)'>
                                                                <a>Delete</a>
                                                            </td>

                                                            <td>
                                                                <select class='form-control' ng-model='newAllergy.drugId' ng-change='vm.findDrug(newAllergy,newAllergy.drugId)'>
                                                                    <option value='{{aDrug.drugId}}' ng-repeat='aDrug in vm.allDrug'>{{aDrug.drugName}}</option>
                                                                </select>
                                                            </td>

                                                            <td>
                                                                <p>{{newAllergy.drugId}}</p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                                <div class='btn btn-primary pull-right' ng-hide='vm.allergySlotCount==0' ng-click='vm.saveNewAllergy()'>Save</div>

                                            </div>
                                        </div>
                                        <br>

                                    </div>
                                </div>

                                <div ui-if="patientTab == 5">
                                    <h3 class='page-header'>Dispensation</h3>
                                    <br>
                                    <div class='panel panel-info'>

                                        <div class='panel-heading'>Prescription by <strong>Doctor {{vm.drugOrder.doctorFirstName}} {{vm.drugOrder.doctorLastName}}</strong></div>
                                        <br>

                                        <div class='row '>
                                            <div class='col-md-10 col-md-offset-1 panel'>
                                                <p><strong>Doctor Note : </strong>{{vm.checkCause(vm.drugOrder.doctorNote)}}</p>
                                            </div>
                                            <br>
                                        </div>

                                        <div class='row'>
                                            <div class='col-md-10 col-md-offset-1'>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Drug Name</th>
                                                            <th>Drug ID</th>
                                                            <th>Quantity</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ui-turn-on='modals'>
                                                            <tr ng-repeat='drug in vm.drugOrder.drugList'>
                                                                <td>{{$index+1}}</td>
                                                                <td>{{drug.drugName}}</td>
                                                                <td>{{drug.drugId}}</td>
                                                                <td>{{drug.drugQuantity}}</td>
                                                                <td>
                                                                    <a ng-show='vm.checkOrder(drug)' ng-click='vm.delToState(drug)'><i class='fa fa-check fa-lg' style='color:#00fa00;' ></i></a>
                                                                    <a ng-hide='vm.checkOrder(drug)' ng-click='vm.addToState(drug)'><i class='fa fa-check fa-lg' style='color:#ddd;' ></i></a>
                                                                </td>
                                                            </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <br>
                                    <div class='panel panel-info'>
                                        <div class='panel-heading'>Add Drug</div>

                                        <br>
                                        <div class='row'>
                                            <div class='col-md-10 col-md-offset-1'>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Drug Name</th>
                                                            <th>Drug ID</th>
                                                            <th>Quantity</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ui-turn-on='modals' ng-click="" ng-repeat='state in vm.pState.drugList'>

                                                            <td>
                                                                <div class='btn btn-danger pull-left' ng-click='vm.delState($index)'>Delete</div>
                                                            </td>

                                                            <td>
                                                                <select class='form-control' ng-model='state.drugId' ng-change='vm.findDrug(state,state.drugId)'>
                                                                    <option value='{{aDrug.drugId}}' ng-repeat='aDrug in vm.allDrug'>{{aDrug.drugName}}</option>
                                                                </select>
                                                            </td>

                                                            <td>
                                                                <p>{{state.drugId}}</p>
                                                            </td>

                                                            <td>
                                                                <input type="text" ng-model="state.drugQuantity" class="form-control" placeholder="Quantity">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="btn btn-primary pull-right" ng-click="vm.addState()"><i class="fa fa-plus"></i>Add Drug</div>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer lg">
                    <button ui-turn-off="modals" class="btn btn-default" ng-click='vm.clearState()'>Close</button>
                    <button ui-turn-off="{{vm.modalControl}}" class="btn btn-primary" ng-click='vm.saveState()'>Save changes</button>
                </div>
            </div>

        </div>
    </div>

    <div class="scrollable-header section" style="z-index:900">
        <div class="container section">
            <div class="row">
                <div class="col-md-12">
                    <h2>My Dashboard</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="scrollable-content" style="position: relative;overflow: hidden;">
        <div ui-state="activeTab" default="1" style="height:inherit">
            <ul class="nav nav-tabs container">
                <li ui-class="{'active': activeTab == 1}">
                    <a ui-set="{'activeTab': 1}">Activity</a>
                </li>
            </ul>

            <div style="position:relative;height:90%">
                <div class="section scrollable" ui-if="activeTab == 1">
                    <div class="scrollable-content">
                        <div class="container" style="height:inherit">
                            <div class="row" style="height:inherit">
                                <div class="col-md-12" style="height:inherit">
                                    <h3 class="page-header">Welcome <small>Pharmacist {{vm.aPharmacist.firstName}} {{vm.aPharmacist.lastName}}</small></h3>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>
                                                <input type="text" class="form-control" ng-model='vm.searchArg' placeholder="Search for..." aria-describedby="sizing-addon1">
                                            </div>
                                        </div>
                                    </div>

                                    <div ng-show="true">
                                        <br>
                                        <h4 class="page-header" style="display:inline" ng-show='vm.allAppointment.length!=0'>Patient List</h4>
                                        <h4 ng-show='vm.allAppointment.length==0'>Does not Have Appointment Today</h4>
                                        <table class="table table-striped" ng-show='vm.allAppointment.length!=0'>
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Patient Name</th>
                                                    <th>Doctor</th>
                                                    <th>Department</th>
                                                    <th>SSID</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="content-row" ui-turn-on='modals' ng-click="vm.setState(person)" ng-repeat='person in vm.allAppointment' ng-show='vm.pSearch(person)'>

                                                    <th scope="row">{{$index+1}}</th>
                                                    <td>{{person.patientFirstName}} {{person.patientLastName}}</td>
                                                    <td>{{person.doctorFirstName}} {{person.doctorLastName}}</td>
                                                    <td>{{person.departmentName}}</td>
                                                    <td>{{person.patientSsid}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
